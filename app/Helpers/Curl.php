<?php

namespace App\Helpers;

/**
 *
 */
class Curl
{
    public static function set($config = [])
    {
        $_postData = '';
        $_param    = (!isset($config['params'])) ? [] : $config['params'];
        $_url      = (!isset($config['url'])) ? '' : $config['url'];
        $_method   = (!isset($config['method'])) ? 'get' : $config['method'];
        $_headers  = (!isset($config['headers'])) ? [] : $config['headers'];
        $_postData = json_encode($_param);
        $ch        = curl_init();
        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $_headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($_method));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_postData);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
