<?php

namespace App\Helpers;

/**
 *
 */
class Tdb
{
	public static function filter($arg, $ops = null)
	{
		if ($ops != null && count($ops) > 0) {
			foreach ($ops as $origin => $alias) {
				$ops_view = (isset($_GET['view'])) ? explode(',', $_GET['view']) : null;
				if ($ops_view != null && count($ops_view) > 0) {
					if (in_array($alias, $ops_view)) {
						$arg = $arg->selectRaw($origin . ' as ' . $alias);
					}

				} else {
					$arg = $arg->selectRaw($origin . ' as ' . $alias);
				}
			}
		}
		return $arg;
	}

	
	public static function respon($data, $status = 200)
	{
		$res = [
			'aplikasi' => 'SURAK KELUAR SURAT MASUK ISLAM TANJUNG',
			'versi'    => '0.0.1',
			'tahun'    => '2018',
			'status'   => ($status == 200) ? true : false,
			'kode'     => $status,
		];
		if ($status == 200) {
			$res['result'] = $data;
		} else {
			$res = $data;
		}

		return response()->json($res, $status);
	}
}
