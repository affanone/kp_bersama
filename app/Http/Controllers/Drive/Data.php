<?php

namespace App\Http\Controllers\Drive;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Udb as F;
use Validator;

class Data extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function hak_akses(Request $request){
    	$data = F::filter(DB::table('hak_akses'),[
    		'id_hak_akses' => 'id',
    		'nama_hak_akses' => 'nama'
    	])
    		->get();

    	return F::respon($data);
    }

    public function format(Request $request){
    	$data = F::filter(DB::table('jenis_file'),[
    		'id_jenis_file' => 'id',
    		'nama_jenis_file' => 'nama'
    	])
    		->get();

    	return F::respon($data);
    }

    public function user(Request $request){
    	$guru = F::filter(DB::table('user'),[
    		'user.id_user' => 'id',
    		'profile_gukar.nama_gukar' => 'nama',
    		'profile_gukar.nik' => 'reg'
    	])
    		->where('profile_gukar.id_user','!=',$request->user)
    		->join('profile_gukar','profile_gukar.id_user','=','user.id_user')
    		->addSelect(DB::raw('"gukar" as jenis'))
    		->get();
    	$siswa = F::filter(DB::table('user'),[
    		'user.id_user' => 'id',
    		'profile_siswa.nama_siswa' => 'nama',
    		'profile_siswa.nis' => 'reg'
    	])
    		->where('profile_siswa.id_user','!=',$request->user)
    		->join('profile_siswa','profile_siswa.id_user','=','user.id_user')
    		->addSelect(DB::raw('"siswa" as jenis'))
    		->get();
    	$user = [];
    	foreach ($guru as $key => $value) {
    		array_push($user, $value);
    	}
    	foreach ($siswa as $key => $value) {
    		array_push($user, $value);
    	}
    	return F::respon($user);
    }

    public function filter(Request $request){
    	$data = F::filter(DB::table('jenis_file'),[
    		'id_jenis_file' => 'id',
    		'nama_jenis_file' => 'nama'
    	])
    		->get();
    	return F::respon([
    		'format' => $data,
    		'order' => [
    			[
    				'id' => 1,
    				'nama' => 'Name'
    			],
    			[
    				'id' => 2,
    				'nama' => 'Date Modified'
    			],
    			[
    				'id' => 3,
    				'nama' => 'Size'
    			],
    			[
    				'id' => 4,
    				'nama' => 'Type'
    			]
    		]
    	]);
    }
}
