<?php

namespace App\Http\Controllers\Drive;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Udb as F;
use Validator;
use Illuminate\Support\Facades\Storage;

const IMAGE_HANDLERS = [
    IMAGETYPE_JPEG => [
        'load' => 'imagecreatefromjpeg',
        'save' => 'imagejpeg',
        'quality' => 100
    ],
    IMAGETYPE_PNG => [
        'load' => 'imagecreatefrompng',
        'save' => 'imagepng',
        'quality' => 0
    ],
    IMAGETYPE_GIF => [
        'load' => 'imagecreatefromgif',
        'save' => 'imagegif'
    ]
];

class File extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function index(Request $request)
    {
        $lvl = DB::table('level_user')
        ->select('type_user.nama_type as level')
        ->where('level_user.id_user',$request->user)
        ->join('type_user','type_user.id_type','=','level_user.id_type')
        ->first();

        $guru = null;
        $siswa = null;

        if(strtolower($lvl->level)=='guru' || strtolower($lvl->level)=='guru bk')
            $guru = 2;
        else if(strtolower($lvl->level)=='siswa')
            $siswa = 3;

        $cari = $request->cari ?? null;
        $format = $request->format ?? null;
        $order = $request->order ?? null;
        $desc_asc = $request->order_by ?? null;

        $order_data = [];
        $order_data[1] = 'upload_file.nama_upload_file_berkas';
        $order_data[2] = 'upload_file.tanggal_upload_file_berkas';
        $order_data[3] = 'upload_file.ukuran_upload_file_berkas';
        $order_data[4] = 'upload_file.ekstensi_upload_file_berkas';

        $data = F::filter(DB::table('upload_file'),[
            'upload_file.id_upload_file_berkas' => 'id',
            'upload_file.ikon_upload_file_berkas' => 'ikon',
            'upload_file.nama_upload_file_berkas' => 'nama',
            'upload_file.ukuran_upload_file_berkas' => 'ukuran',
            'upload_file.lokasi_upload_file_berkas' => 'src',
            'upload_file.tanggal_upload_file_berkas' => 'tanggal',
            'upload_file.keterangan_upload_file_berkas' => 'keterangan',
            'upload_file.total_download_upload_file_berkas' => 'total_download',
            'upload_file.total_lihat_upload_file_berkas' => 'total_lihat',
            'upload_file.ekstensi_upload_file_berkas' => 'ekstensi',
            'upload_file.hash_upload_file_berkas' => 'hash',
            'upload_file.id_user' => 'user',
            'profile_gukar.nama_gukar' => 'guru',
            'profile_siswa.nama_siswa' => 'siswa',
            'profile_gukar.nik' => 'nik',
            'profile_siswa.nis' => 'nis',

        ])
        ->addSelect(DB::raw('(CASE 
            WHEN profile_gukar.nama_gukar IS NOT NULL THEN "Guru"
            WHEN profile_siswa.nama_siswa IS NOT NULL THEN "Siswa"
            ELSE "Admin" END
        ) as type_user'))
        ->where(function($query) use ($request,$guru,$siswa){
            return $query->whereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = 5 and type_hak_akses_group like ?) ',['%,'.$request->user.',%'])
            ->orWhereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = ?) ',[1])
            ->when($guru, function ($query){
                return $query->orWhereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = ?) ',[2]);
            })
            ->when($siswa, function ($query){
                return $query->orWhereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = ?) ',[3]);
            })
            ->whereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses != ?)',[6]);
        })
        ->where('upload_file.id_user','<>',$request->user)
        ->when($cari, function ($query) use ($cari){
            return $query->where(function($query) use ($cari) {
                $query->where('upload_file.nama_upload_file_berkas','like','%'.$cari.'%')
                ->orWhere('upload_file.keterangan_upload_file_berkas','like','%'.$cari.'%');
            })
            ->orWhere(function($query) use ($cari){
                return $query->whereRaw('upload_file.id_user in (select id_user from profile_siswa where nama_siswa like ? or nis like ? ) ',['%'.$cari.'%','%'.$cari.'%'])
                ->orWhereRaw('upload_file.id_user in (select id_user from profile_gukar where nama_gukar like ? or nik like ?) ',['%'.$cari.'%','%'.$cari.'%']);
            });
        })
        ->when($order, function ($query) use ($order,$order_data,$desc_asc){
            return $query->orderBy($order_data[$order],$desc_asc);
        })
        ->when($format, function ($query) use ($format){
            return $query->where('upload_file.ekstensi_upload_file_berkas','like','%'.$format.'%');
        })        
        ->leftJoin('profile_siswa','profile_siswa.id_user','=','upload_file.id_user')
        ->leftJoin('profile_gukar','profile_gukar.id_user','=','upload_file.id_user')
        ->paginate(10);
        
        return F::respon($data);
    }

    public function pribadi(Request $request)
    {
        $lvl = DB::table('level_user')
        ->select('type_user.nama_type as level')
        ->where('level_user.id_user',$request->user)
        ->join('type_user','type_user.id_type','=','level_user.id_type')
        ->first();

        $guru = null;
        $siswa = null;

        if(strtolower($lvl->level)=='guru' || strtolower($lvl->level)=='guru bk')
            $guru = 2;
        else if(strtolower($lvl->level)=='siswa')
            $siswa = 3;

        $cari = $request->cari ?? null;
        $format = $request->format ?? null;
        $order = $request->order ?? null;
        $desc_asc = $request->order_by ?? null;

        $order_data = [];
        $order_data[1] = 'upload_file.nama_upload_file_berkas';
        $order_data[2] = 'upload_file.tanggal_upload_file_berkas';
        $order_data[3] = 'upload_file.ukuran_upload_file_berkas';
        $order_data[4] = 'upload_file.ekstensi_upload_file_berkas';

        $data = F::filter(DB::table('upload_file'),[
            'upload_file.id_upload_file_berkas' => 'id',
            'upload_file.ikon_upload_file_berkas' => 'ikon',
            'upload_file.nama_upload_file_berkas' => 'nama',
            'upload_file.ukuran_upload_file_berkas' => 'ukuran',
            'upload_file.lokasi_upload_file_berkas' => 'src',
            'upload_file.tanggal_upload_file_berkas' => 'tanggal',
            'upload_file.keterangan_upload_file_berkas' => 'keterangan',
            'upload_file.total_download_upload_file_berkas' => 'total_download',
            'upload_file.total_lihat_upload_file_berkas' => 'total_lihat',
            'upload_file.ekstensi_upload_file_berkas' => 'ekstensi',
            'upload_file.hash_upload_file_berkas' => 'hash',
            'upload_file.id_user' => 'user',
            'profile_gukar.nama_gukar' => 'guru',
            'profile_siswa.nama_siswa' => 'siswa',
            'profile_gukar.nik' => 'nik',
            'profile_siswa.nis' => 'nis',

        ])
        ->addSelect(DB::raw('(CASE 
            WHEN profile_gukar.nama_gukar IS NOT NULL THEN "Guru"
            WHEN profile_siswa.nama_siswa IS NOT NULL THEN "Siswa"
            ELSE "Admin" END
        ) as type_user'))
        ->where(function($query) use ($request,$guru,$siswa){
            return $query->whereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = 6) ')
            ->where('upload_file.id_user',$request->user);
        })
        ->when($cari, function ($query) use ($cari){
            return $query->where(function($query) use ($cari) {
                $query->where('upload_file.nama_upload_file_berkas','like','%'.$cari.'%')
                ->orWhere('upload_file.keterangan_upload_file_berkas','like','%'.$cari.'%');
            })
            ->orWhere(function($query) use ($cari){
                return $query->whereRaw('upload_file.id_user in (select id_user from profile_siswa where nama_siswa like ? or nis like ? ) ',['%'.$cari.'%','%'.$cari.'%'])
                ->orWhereRaw('upload_file.id_user in (select id_user from profile_gukar where nama_gukar like ? or nik like ?) ',['%'.$cari.'%','%'.$cari.'%']);
            });
        })
        ->when($order, function ($query) use ($order,$order_data,$desc_asc){
            return $query->orderBy($order_data[$order],$desc_asc);
        })
        ->when($format, function ($query) use ($format){
            return $query->where('upload_file.ekstensi_upload_file_berkas','like','%'.$format.'%');
        })        
        ->leftJoin('profile_siswa','profile_siswa.id_user','=','upload_file.id_user')
        ->leftJoin('profile_gukar','profile_gukar.id_user','=','upload_file.id_user')
        ->paginate(10);
        
        return F::respon($data);
    }

    public function manajer(Request $request)
    {
        $lvl = DB::table('level_user')
        ->select('type_user.nama_type as level')
        ->where('level_user.id_user',$request->user)
        ->join('type_user','type_user.id_type','=','level_user.id_type')
        ->first();

        $cari = $request->cari ?? null;
        $format = $request->format ?? null;
        $order = $request->order ?? null;
        $desc_asc = $request->order_by ?? null;

        $order_data = [];
        $order_data[1] = 'upload_file.nama_upload_file_berkas';
        $order_data[2] = 'upload_file.tanggal_upload_file_berkas';
        $order_data[3] = 'upload_file.ukuran_upload_file_berkas';
        $order_data[4] = 'upload_file.ekstensi_upload_file_berkas';

        $data = F::filter(DB::table('upload_file'),[
            'upload_file.id_upload_file_berkas' => 'id',
            'upload_file.ikon_upload_file_berkas' => 'ikon',
            'upload_file.nama_upload_file_berkas' => 'nama',
            'upload_file.ukuran_upload_file_berkas' => 'ukuran',
            'upload_file.lokasi_upload_file_berkas' => 'src',
            'upload_file.tanggal_upload_file_berkas' => 'tanggal',
            'upload_file.keterangan_upload_file_berkas' => 'keterangan',
            'upload_file.total_download_upload_file_berkas' => 'total_download',
            'upload_file.total_lihat_upload_file_berkas' => 'total_lihat',
            'upload_file.ekstensi_upload_file_berkas' => 'ekstensi',
            'upload_file.hash_upload_file_berkas' => 'hash',
            'upload_file.id_user' => 'user',
            'profile_gukar.nama_gukar' => 'guru',
            'profile_siswa.nama_siswa' => 'siswa',
            'profile_gukar.nik' => 'nik',
            'profile_siswa.nis' => 'nis',

        ])
        ->addSelect(DB::raw('(CASE 
            WHEN profile_gukar.nama_gukar IS NOT NULL THEN "Guru"
            WHEN profile_siswa.nama_siswa IS NOT NULL THEN "Siswa"
            ELSE "Admin" END
        ) as type_user'))
        ->where(function($query) use ($request){
            return $query->where('upload_file.id_user',$request->user);
        })
        ->when($cari, function ($query) use ($cari){
            return $query->where(function($query) use ($cari) {
                $query->where('upload_file.nama_upload_file_berkas','like','%'.$cari.'%')
                ->orWhere('upload_file.keterangan_upload_file_berkas','like','%'.$cari.'%');
            })
            ->orWhere(function($query) use ($cari){
                return $query->whereRaw('upload_file.id_user in (select id_user from profile_siswa where nama_siswa like ? or nis like ? ) ',['%'.$cari.'%','%'.$cari.'%'])
                ->orWhereRaw('upload_file.id_user in (select id_user from profile_gukar where nama_gukar like ? or nik like ?) ',['%'.$cari.'%','%'.$cari.'%']);
            });
        })
        ->when($order, function ($query) use ($order,$order_data,$desc_asc){
            return $query->orderBy($order_data[$order],$desc_asc);
        })
        ->when($format, function ($query) use ($format){
            return $query->where('upload_file.ekstensi_upload_file_berkas','like','%'.$format.'%');
        })        
        ->leftJoin('profile_siswa','profile_siswa.id_user','=','upload_file.id_user')
        ->leftJoin('profile_gukar','profile_gukar.id_user','=','upload_file.id_user')
        ->paginate(10);
        
        return F::respon($data);
    }

    public function bersama(Request $request)
    {
        $lvl = DB::table('level_user')
        ->select('type_user.nama_type as level')
        ->where('level_user.id_user',$request->user)
        ->join('type_user','type_user.id_type','=','level_user.id_type')
        ->first();

        $guru = null;
        $siswa = null;

        if(strtolower($lvl->level)=='guru' || strtolower($lvl->level)=='guru bk')
            $guru = 2;
        else if(strtolower($lvl->level)=='siswa')
            $siswa = 3;
        
        $cari = $request->cari ?? null;
        $format = $request->format ?? null;
        $order = $request->order ?? null;
        $desc_asc = $request->order_by ?? null;

        $order_data = [];
        $order_data[1] = 'upload_file.nama_upload_file_berkas';
        $order_data[2] = 'upload_file.tanggal_upload_file_berkas';
        $order_data[3] = 'upload_file.ukuran_upload_file_berkas';
        $order_data[4] = 'upload_file.ekstensi_upload_file_berkas';

        $data = F::filter(DB::table('upload_file'),[
            'upload_file.id_upload_file_berkas' => 'id',
            'upload_file.ikon_upload_file_berkas' => 'ikon',
            'upload_file.nama_upload_file_berkas' => 'nama',
            'upload_file.ukuran_upload_file_berkas' => 'ukuran',
            'upload_file.lokasi_upload_file_berkas' => 'src',
            'upload_file.tanggal_upload_file_berkas' => 'tanggal',
            'upload_file.keterangan_upload_file_berkas' => 'keterangan',
            'upload_file.total_download_upload_file_berkas' => 'total_download',
            'upload_file.total_lihat_upload_file_berkas' => 'total_lihat',
            'upload_file.ekstensi_upload_file_berkas' => 'ekstensi',
            'upload_file.hash_upload_file_berkas' => 'hash',
            'upload_file.id_user' => 'user',
            'profile_gukar.nama_gukar' => 'guru',
            'profile_siswa.nama_siswa' => 'siswa',
            'profile_gukar.nik' => 'nik',
            'profile_siswa.nis' => 'nis',

        ])
        ->addSelect(DB::raw('(CASE 
            WHEN profile_gukar.nama_gukar IS NOT NULL THEN "Guru"
            WHEN profile_siswa.nama_siswa IS NOT NULL THEN "Siswa"
            ELSE "Admin" END
        ) as type_user'))
        ->where(function($query) use ($request,$guru,$siswa){
            return $query->whereRaw('upload_file.id_hak_akses_group in (select id_hak_akses_group from hak_akses_group where id_hak_akses = 5 and type_hak_akses_group like ?) ',['%,'.$request->user.',%'])
            ->where('upload_file.id_user','!=',$request->user);
        })
        ->when($cari, function ($query) use ($cari){
            return $query->where(function($query) use ($cari) {
                $query->where('upload_file.nama_upload_file_berkas','like','%'.$cari.'%')
                ->orWhere('upload_file.keterangan_upload_file_berkas','like','%'.$cari.'%');
            })
            ->orWhere(function($query) use ($cari){
                return $query->whereRaw('upload_file.id_user in (select id_user from profile_siswa where nama_siswa like ? or nis like ? ) ',['%'.$cari.'%','%'.$cari.'%'])
                ->orWhereRaw('upload_file.id_user in (select id_user from profile_gukar where nama_gukar like ? or nik like ?) ',['%'.$cari.'%','%'.$cari.'%']);
            });
        })
        ->when($order, function ($query) use ($order,$order_data,$desc_asc){
            return $query->orderBy($order_data[$order],$desc_asc);
        })
        ->when($format, function ($query) use ($format){
            return $query->where('upload_file.ekstensi_upload_file_berkas','like','%'.$format.'%');
        })        
        ->leftJoin('profile_siswa','profile_siswa.id_user','=','upload_file.id_user')
        ->leftJoin('profile_gukar','profile_gukar.id_user','=','upload_file.id_user')
        ->paginate(10);
        
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $berkas = F::filter(DB::table('upload_file'),[
            'upload_file.id_upload_file_berkas' => 'id',
            'upload_file.ikon_upload_file_berkas' => 'ikon',
            'upload_file.nama_upload_file_berkas' => 'nama',
            'upload_file.ukuran_upload_file_berkas' => 'ukuran',
            'upload_file.lokasi_upload_file_berkas' => 'src',
            'upload_file.tanggal_upload_file_berkas' => 'tanggal',
            'upload_file.keterangan_upload_file_berkas' => 'keterangan',
            'upload_file.total_download_upload_file_berkas' => 'total_download',
            'upload_file.total_lihat_upload_file_berkas' => 'total_lihat',
            'upload_file.ekstensi_upload_file_berkas' => 'ekstensi',
            'upload_file.hash_upload_file_berkas' => 'hash',
            'upload_file.id_user' => 'user',
            'hak_akses_group.id_hak_akses' => 'hak_akses',
            'hak_akses_group.type_hak_akses_group' => 'hak_akses_group'
        ])
            ->where('upload_file.id_upload_file_berkas',$request->id)
            ->join('hak_akses_group','hak_akses_group.id_hak_akses_group','=','upload_file.id_hak_akses_group')
            ->first();
        return F::respon($berkas);
    }

    public function download(Request $request)
    {
        $file = DB::table('upload_file')
        ->where('hash_upload_file_berkas',$request->file)
        //->where('id_user',$request->user)
        ->first();
        DB::table('upload_file')
        ->where('hash_upload_file_berkas',$request->file)
        //->where('id_user',$request->user)
        ->update([
            'total_download_upload_file_berkas' => DB::raw('total_download_upload_file_berkas+1')
        ]);
        return Storage::disk('files')->download(str_replace('files/', '', $file->lokasi_upload_file_berkas) , $file->nama_upload_file_berkas);
    }

    public function lihat(Request $request){
        DB::table('upload_file')
        ->where('hash_upload_file_berkas',$request->file)
        ->where('id_user',$request->user)
        ->update([
            'total_lihat_upload_file_berkas' => DB::raw('total_lihat_upload_file_berkas+1')
        ]);
        return F::respon(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) {

    // 1. Load the image from the given $src
    // - see if the file actually exists
    // - check if it's of a valid image type
    // - load the image resource

    // get the type of the image
    // we need the type to determine the correct loader
        $type = exif_imagetype($src);

    // if no valid type or no handler found -> exit
        if (!$type || !IMAGE_HANDLERS[$type]) {
            return null;
        }

    // load the image with the correct loader
        $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);

    // no image found at supplied location -> exit
        if (!$image) {
            return null;
        }


    // 2. Create a thumbnail and resize the loaded $image
    // - get the image dimensions
    // - define the output size appropriately
    // - create a thumbnail based on that size
    // - set alpha transparency for GIFs and PNGs
    // - draw the final thumbnail

    // get original image width and height
        $width = imagesx($image);
        $height = imagesy($image);

    // maintain aspect ratio when no height set
        if ($targetHeight == null) {

        // get width to height ratio
            $ratio = $width / $height;

        // if is portrait
        // use ratio to scale height to fit in square
            if ($width > $height) {
                $targetHeight = floor($targetWidth / $ratio);
            }
        // if is landscape
        // use ratio to scale width to fit in square
            else {
                $targetHeight = $targetWidth;
                $targetWidth = floor($targetWidth * $ratio);
            }
        }

    // create duplicate image based on calculated target size
        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

    // set transparency options for GIFs and PNGs
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {

        // make image transparent
            imagecolortransparent(
                $thumbnail,
                imagecolorallocate($thumbnail, 0, 0, 0)
            );

        // additional settings for PNGs
            if ($type == IMAGETYPE_PNG) {
                imagealphablending($thumbnail, false);
                imagesavealpha($thumbnail, true);
            }
        }

    // copy entire source image to duplicate image and resize
        imagecopyresampled(
            $thumbnail,
            $image,
            0, 0, 0, 0,
            $targetWidth, $targetHeight,
            $width, $height
        );


    // 3. Save the $thumbnail to disk
    // - call the correct save method
    // - set the correct quality level

    // save the duplicate version of the image to disk
        return call_user_func(
            IMAGE_HANDLERS[$type]['save'],
            $thumbnail,
            $dest,
            IMAGE_HANDLERS[$type]['quality']
        );
    }

    public function store(Request $request)
    {
        $data = F::filter(DB::table('jenis_file'),[
            'id_jenis_file' => 'id',
            'nama_jenis_file' => 'nama'
        ])
        ->get();
        $_format = [];
        foreach ($data as $key => $value) {
            array_push($_format, strtolower($value->nama));
        }

        $rules = [
            'bagikan' => 'required',
            'file' => 'required',
        ];

        if($request->bagikan==4 || $request->bagikan==5){ // kecuali
            $rules['orang'] = 'required|array|min:1';
        }

        $validator = Validator::make($request->all(),$rules,[
            'bagikan.required' => 'Siapa saja yang akan melihat ini?',
            'keterangan.required' => 'Keterangan file harus diisi!',
            'file.required' => 'Sertakan file!',
            'orang.required' => 'Pilihlah beberapa orang',
            'orang.array' => 'Format beberapa orang tidak diperbolehkan',
            'orang.min' => 'Jumlah ke beberapa orang minimal harus ada'
        ]);

        if($validator->fails())
            return F::respon($validator->errors(),411);

        $keterangan = $request->keterangan ?? '-';

        $has = md5_file($_FILES['file']['tmp_name']);
        $cek = DB::table('upload_file')
        ->where('id_user',$request->user)
        ->where('hash_upload_file_berkas',$has)
        ->first();


        if($cek!=null){
            $validator->errors()->add('file', 'File ini duplikat dengan file "'.$cek->nama_upload_file_berkas.'" di server');
            return F::respon($validator->errors(),411);
        }

        $type_share = '';
        switch ($request->bagikan) {
            case '1':
            $type_share = 'public';
            break;
            case '2':
            $type_share = 'teacher';
            break;
            case '3':
            $type_share = 'student';
            break;
            case '4':
            $type_share = 'except';
            break;
            case '5':
            $type_share = 'only';
            break;
            case '6':
            $type_share = 'private';
            break;
        }

        $idgroup = null;
        if($request->bagikan==4 || $request->bagikan==5){
            $orang = '';
            foreach (collect($request->orang)->sort() as $key => $value) {
                $orang .= ','.$value.',';
            }
            $type_share = $orang;

            $group_detail = DB::table('hak_akses_group')
            ->where('hak_akses_group.type_hak_akses_group',$type_share)
            ->where('hak_akses_group.id_hak_akses',$request->bagikan)
            ->first();

            if($group_detail == null){
                $idgroup = DB::table('hak_akses_group')
                ->insertGetId([
                    'id_hak_akses' => $request->bagikan,
                    'type_hak_akses_group' => $type_share
                ]);
            }else{
                $idgroup = $group_detail->id_hak_akses_group;
            }
        }else{
            $group_detail = DB::table('hak_akses_group')
            ->where('id_hak_akses',$request->bagikan)
            ->first();
            if($group_detail == null){
                $idgroup = DB::table('hak_akses_group')
                ->insertGetId([
                    'id_hak_akses' => $request->bagikan,
                    'type_hak_akses_group' => $type_share
                ]);
            }else{
                $idgroup = $group_detail->id_hak_akses_group;
            }            
        }
        
        $File      = $request->file('file');
        $filename = $File->getClientOriginalName();
        $format = $File->getClientOriginalExtension();
        $datetime = date('Y-m-d H:i:s');
        $newname = date('YmdHis').rand(111,999).'.'.$format;
        $destination = 'files/'.$request->user;
        $link = $destination . '/' . $newname;
        $images_format = ['jpg','jpeg','png','gif']; // auto thumbnail
        if(!in_array(strtolower($format), $_format)){
            $validator->errors()->add('file', 'Format file tidak didukung oleh sistem!');
            return F::respon($validator->errors(),411);
        }
        $size = $File->getClientSize();
        $File->move(public_path($destination),$newname);
        $thumbnail = 'thumbnail/'.md5('thumb_'.rand(111111,999999).$newname).'.'.$format;
        if(in_array(strtolower($format), $images_format)){
            self::createThumbnail(public_path($destination).'/'.$newname, public_path($thumbnail), 160);
            $icon = $thumbnail;
        }else{
            $icon = DB::table('jenis_file')
            ->where('nama_jenis_file',$File->getClientOriginalExtension())
            ->first();

            if($icon==null)
                $icon = 'images/unknow.png';
            else
                $icon = $icon->ikon_jenis_file;
        }

        DB::table('upload_file')
        ->insert([
            'nama_upload_file_berkas' => $filename,
            'ukuran_upload_file_berkas' => $size,
            'lokasi_upload_file_berkas' => $link,
            'tanggal_upload_file_berkas' => $datetime,
            'total_download_upload_file_berkas' => 0,
            'total_lihat_upload_file_berkas' => 0,
            'hash_upload_file_berkas' => $has,
            'ikon_upload_file_berkas' => $icon,
            'keterangan_upload_file_berkas' => $keterangan,
            'ekstensi_upload_file_berkas' => $format,
            'id_user' => $request->user,
            'id_hak_akses_group' => $idgroup
        ]);  
        return F::respon(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'bagikan' => 'required',
        ];

        if($request->bagikan==4 || $request->bagikan==5){ // kecuali
            $rules['orang'] = 'required|array|min:1';
        }

        $validator = Validator::make($request->all(),$rules,[
            'bagikan.required' => 'Siapa saja yang akan melihat ini?',
            'keterangan.required' => 'Keterangan file harus diisi!',
            'orang.required' => 'Pilihlah beberapa orang',
            'orang.array' => 'Format beberapa orang tidak diperbolehkan',
            'orang.min' => 'Jumlah ke beberapa orang minimal harus ada'
        ]);

        if($validator->fails())
            return F::respon($validator->errors(),411);

        $keterangan = $request->keterangan ?? '-';

        $type_share = '';
        switch ($request->bagikan) {
            case '1':
            $type_share = 'public';
            break;
            case '2':
            $type_share = 'teacher';
            break;
            case '3':
            $type_share = 'student';
            break;
            case '4':
            $type_share = 'except';
            break;
            case '5':
            $type_share = 'only';
            break;
            case '6':
            $type_share = 'private';
            break;
        }

        $idgroup = null;
        if($request->bagikan==4 || $request->bagikan==5){
            $orang = '';
            foreach (collect($request->orang)->sort() as $key => $value) {
                $orang .= ','.$value.',';
            }
            $type_share = $orang;

            $group_detail = DB::table('hak_akses_group')
            ->where('hak_akses_group.type_hak_akses_group',$type_share)
            ->where('hak_akses_group.id_hak_akses',$request->bagikan)
            ->first();

            if($group_detail == null){
                $idgroup = DB::table('hak_akses_group')
                ->insertGetId([
                    'id_hak_akses' => $request->bagikan,
                    'type_hak_akses_group' => $type_share
                ]);
            }else{
                $idgroup = $group_detail->id_hak_akses_group;
            }
        }else{
            $group_detail = DB::table('hak_akses_group')
            ->where('id_hak_akses',$request->bagikan)
            ->first();
            if($group_detail == null){
                $idgroup = DB::table('hak_akses_group')
                ->insertGetId([
                    'id_hak_akses' => $request->bagikan,
                    'type_hak_akses_group' => $type_share
                ]);
            }else{
                $idgroup = $group_detail->id_hak_akses_group;
            }            
        }
        DB::table('upload_file')
        ->where('id_upload_file_berkas',$request->id)
        ->where('id_user',$request->user)
        ->update([
            'keterangan_upload_file_berkas' => $keterangan,
            'id_hak_akses_group' => $idgroup
        ]);  
        return F::respon(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r)
    {
        $eks = DB::table('upload_file')
            ->where('id_upload_file_berkas',$r->id)
            ->first();
        $images_format = ['jpg','jpeg','png','gif'];
        if(in_array(strtolower($eks->ekstensi_upload_file_berkas), $images_format)){
            Storage::disk('thumbnail')->delete(str_replace('thumbnail/', '', $eks->ikon_upload_file_berkas));
        }
        Storage::disk('files')->delete(str_replace('files/', '', $eks->lokasi_upload_file_berkas));
        DB::table('upload_file')
            ->where('id_upload_file_berkas',$r->id)
            ->delete();
        return self::manajer($r);
    }
}
