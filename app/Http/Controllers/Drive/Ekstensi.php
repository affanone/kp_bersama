<?php

namespace App\Http\Controllers\Drive;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Udb as F;
use Validator;
use Illuminate\Support\Facades\Storage;

const IMAGE_HANDLERS = [
    IMAGETYPE_JPEG => [
        'load' => 'imagecreatefromjpeg',
        'save' => 'imagejpeg',
        'quality' => 100
    ],
    IMAGETYPE_PNG => [
        'load' => 'imagecreatefrompng',
        'save' => 'imagepng',
        'quality' => 0
    ],
    IMAGETYPE_GIF => [
        'load' => 'imagecreatefromgif',
        'save' => 'imagegif'
    ]
];

class Ekstensi extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('jenis_file'),[
            'id_jenis_file' => 'id',
            'nama_jenis_file' => 'nama',
            'ikon_jenis_file' => 'ikon'
        ])
        ->when($cari,function($q) use ($cari){
            return $q->where('nama_jenis_file','like','%'.$cari.'%');
        })
        ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $default = $r->default ?? null;
        $rules = [
            'nama' => ['required','alpha_num',function($attr,$v,$er){
                $cek = DB::table('jenis_file')
                    ->where('nama_jenis_file',$v)
                    ->count();
                if($cek>0)
                    return $er('Jenis ekstensi sudah ada!');
            }]
        ];
        if($default==null)
            $rules['ikon'] = 'required|mimes:jpg,png,gif,jpeg';
        $str_err = [
            'nama.required' => 'Jenis ekstensi harus diisi',
            'nama.alpha_num' => 'Jenis ekstensi tidak diperbolehkan',
            'ikon.required' => 'Ikon ekstensi harus diisi',
            'ikon.mimes' => 'Format ikon harus JPG,JPEG,PNG,GIF'
        ];
        $v = Validator::make($r->all(), $rules,$str_err);
        if($v->fails())
            return F::respon($v->errors(),411);
        
        $thumbnail = null;
        if($default==null){
            $File      = $r->file('ikon');
            $format = $File->getClientOriginalExtension();
            $newname = date('YmdHis').rand(111,999).'.'.$format;
            $destination = 'thumbnail';
            $original = $destination.'/'.$newname;
            $thumbnail = $destination.'/'.md5($newname).'.'.$format;
            $File->move(public_path($destination),$newname);
            self::createThumbnail(public_path($destination).'/'.$newname, public_path($thumbnail), 160);
            Storage::disk('thumbnail')->delete($newname);
        }else{
            $destination = 'thumbnail';
            $thumbnail = $destination.'/unknow.png';
        }

        if($thumbnail==null){
            $v->errors()->add('error', 'Kesaahan ketika proses upload ikon');
            return F::respon($v->errors(),411);
        }

        DB::table('jenis_file')
            ->insert([
                'nama_jenis_file' => $r->nama,
                'ikon_jenis_file' => $thumbnail
            ]);

        return self::index($r);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r)
    {
        $eks = DB::table('jenis_file')
            ->where('id_jenis_file',$r->id)
            ->first();
        if($eks->ikon_jenis_file!='thumbnail/unknow.png'){
            Storage::disk('thumbnail')->delete(str_replace('thumbnail/', '', $eks->ikon_jenis_file));
        }
        DB::table('jenis_file')
            ->where('id_jenis_file',$r->id)
            ->delete();
        return self::index($r);
    }
    
    public function createThumbnail($src, $dest, $targetWidth, $targetHeight = null) {

    // 1. Load the image from the given $src
    // - see if the file actually exists
    // - check if it's of a valid image type
    // - load the image resource

    // get the type of the image
    // we need the type to determine the correct loader
        $type = exif_imagetype($src);

    // if no valid type or no handler found -> exit
        if (!$type || !IMAGE_HANDLERS[$type]) {
            return null;
        }

    // load the image with the correct loader
        $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);

    // no image found at supplied location -> exit
        if (!$image) {
            return null;
        }


    // 2. Create a thumbnail and resize the loaded $image
    // - get the image dimensions
    // - define the output size appropriately
    // - create a thumbnail based on that size
    // - set alpha transparency for GIFs and PNGs
    // - draw the final thumbnail

    // get original image width and height
        $width = imagesx($image);
        $height = imagesy($image);

    // maintain aspect ratio when no height set
        if ($targetHeight == null) {

        // get width to height ratio
            $ratio = $width / $height;

        // if is portrait
        // use ratio to scale height to fit in square
            if ($width > $height) {
                $targetHeight = floor($targetWidth / $ratio);
            }
        // if is landscape
        // use ratio to scale width to fit in square
            else {
                $targetHeight = $targetWidth;
                $targetWidth = floor($targetWidth * $ratio);
            }
        }

    // create duplicate image based on calculated target size
        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

    // set transparency options for GIFs and PNGs
        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {

        // make image transparent
            imagecolortransparent(
                $thumbnail,
                imagecolorallocate($thumbnail, 0, 0, 0)
            );

        // additional settings for PNGs
            if ($type == IMAGETYPE_PNG) {
                imagealphablending($thumbnail, false);
                imagesavealpha($thumbnail, true);
            }
        }

    // copy entire source image to duplicate image and resize
        imagecopyresampled(
            $thumbnail,
            $image,
            0, 0, 0, 0,
            $targetWidth, $targetHeight,
            $width, $height
        );


    // 3. Save the $thumbnail to disk
    // - call the correct save method
    // - set the correct quality level

    // save the duplicate version of the image to disk
        return call_user_func(
            IMAGE_HANDLERS[$type]['save'],
            $thumbnail,
            $dest,
            IMAGE_HANDLERS[$type]['quality']
        );
    }
}
