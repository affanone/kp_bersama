<?php

namespace App\Http\Controllers\Raport;

use App\Helpers\Ina as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Login extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = null;
        if (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
            $u    = Auth::user();
            $data = F::filter(DB::table('user'),[
                'user.email' =>'username',
                'type_user.nama_type' => 'level',
                'user.id_user'   => 'id'
            ])
                ->where('user.id_user',$u->id_user)
                ->join('level_user','level_user.id_user','=','user.id_user')
                ->join('type_user',function($j){
                    $j->on('type_user.id_type','=','level_user.id_type')
                        ->where(function($q){
                            $q->where('type_user.nama_type','Guru');
                            $q->orWhere('type_user.nama_type','Siswa');
                            $q->orWhere('type_user.nama_type','Wali Kelas');
                        });                        
                })
                ->orderBy('type_user.id_type')
                ->get();
                if($data == null){
                    return F::respon('Username atau password salah',401);            
                }else{
                    $user = [];
                    foreach ($data as $key => $value) {
                        $value->level = ucwords(str_replace([' '], [''], $value->level));
                        array_push($user,$value);
                    }
                    return F::respon($user);
                }
        }
        
        return F::respon('Username atau password salah',401);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
