<?php

namespace App\Http\Controllers\Raport;

use App\Helpers\Ina as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Nilai extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function catatan_ortu(Request $request)
    {
        DB::table('raport')
            ->where('id_raport', $request->id)
            ->update([
                'catatan_orangtua_raport' => $request->catatan,
            ]);

        return F::respon('Catatan berhasil diperbarui');
    }

    public function catatan_wali(Request $request)
    {
        DB::table('raport')
            ->where('id_raport', $request->id)
            ->update([
                'catatan_walikelas_raport' => $request->catatan,
            ]);

        return F::respon('Catatan berhasil diperbarui');
    }

    public function ketidakhadiran(Request $request)
    {
        DB::table('raport')
            ->where('id_raport', $request->id)
            ->update([
                'sakit_raport'     => $request->sakit,
                'ijin_raport'      => $request->ijin,
                'alpa_raport'      => $request->alpa,
                'kelakuan_raport'  => $request->kelakuan,
                'kerajinan_raport' => $request->kerajinan,
                'kerapian_raport'  => $request->kerapian,
            ]);

        return F::respon('Ketidak hadiran berhasil diperbarui');
    }

    public function deskripsi(Request $request)
    {
        DB::table('raport')
            ->where('id_raport', $request->id)
            ->update([
                'deskripsi_raport' => $request->deskripsi,
            ]);

        return F::respon('Deskripsi berhasil diperbarui');
    }

    public function delete_prestasi(Request $request)
    {
        DB::table('raport_prestasi')
            ->where('id_raport_prestasi', $request->id)
            ->delete();
        return F::respon(self::data_prestasi($request->raport));
    }

    public function delete_ekstrakurikuler(Request $request)
    {
        DB::table('raport_ekstr')
            ->where('id_raport_ekstr', $request->id)
            ->delete();
        return F::respon(self::data_ekstrakurikuler($request->raport));
    }

    public function delete_pkl(Request $request)
    {
        DB::table('raport_kp')
            ->where('id_raport_kp', $request->id)
            ->delete();
        return F::respon(self::data_pkl($request->raport));
    }

    public function insert_prestasi(Request $request)
    {
        $data = json_decode($request->data);
        DB::table('raport_prestasi')
            ->insert([
                'id_raport'           => $request->raport,
                'jenis_prestasi'      => $data->jenis,
                'keterangan_prestasi' => $data->keterangan,
            ]);

        return F::respon(self::data_prestasi($request->raport));
    }

    public function insert_ekstrakurikuler(Request $request)
    {
        $data = json_decode($request->data);
        DB::table('raport_ekstr')
            ->insert([
                'id_raport'               => $request->raport,
                'kegiatan_raport_ekstr'   => $data->kegiatan,
                'keterangan_raport_ekstr' => $data->keterangan,
                'predikat_raport_ekstr'   => $data->predikat,
            ]);

        return F::respon(self::data_ekstrakurikuler($request->raport));
    }

    public function insert_pkl(Request $request)
    {
        $data = json_decode($request->data);
        DB::table('raport_kp')
            ->insert([
                'id_raport'            => $request->raport,
                'mitra_raport_kp'      => $data->mitra,
                'lokasi_raport_kp'     => $data->lokasi,
                'lama_raport_kp'       => $data->lama,
                'keterangan_raport_kp' => $data->keterangan,
            ]);

        return F::respon(self::data_pkl($request->raport));
    }

    private function data_prestasi($id)
    {
        $prestasi = F::filter(DB::table('raport_prestasi'), [
            'id_raport_prestasi'  => 'id',
            'jenis_prestasi'      => 'jenis',
            'keterangan_prestasi' => 'keterangan',
        ])
            ->where('id_raport', $id)
            ->get();

        return $prestasi;
    }

    private function data_ekstrakurikuler($id)
    {
        $ekstra = F::filter(DB::table('raport_ekstr'), [
            'id_raport_ekstr'         => 'id',
            'kegiatan_raport_ekstr'   => 'kegiatan',
            'keterangan_raport_ekstr' => 'keterangan',
            'predikat_raport_ekstr'   => 'predikat',
        ])
            ->where('id_raport', $id)
            ->get();

        return $ekstra;
    }

    private function data_pkl($id)
    {
        $pkl = F::filter(DB::table('raport_kp'), [
            'id_raport_kp'         => 'id',
            'mitra_raport_kp'      => 'mitra',
            'lokasi_raport_kp'     => 'lokasi',
            'lama_raport_kp'       => 'lama',
            'keterangan_raport_kp' => 'keterangan',
        ])
            ->where('id_raport', $id)
            ->get();

        return $pkl;
    }

    private function data_nilai($id)
    {
        $nilai = F::filter(DB::table('nilai_raport'), [
            'nilai_raport.id_nilai_raport'        => 'id',
            'nilai_raport.id_mapel'               => 'id_mapel',
            'mapel.nama_mapel'                    => 'mapel',
            'nilai_raport.kkm_nilai_raport'       => 'kkm',
            'nilai_raport.jenis_nilai_raport'     => 'jenis',
            'nilai_raport.nilai_nilai_raport'     => 'nilai',
            'nilai_raport.deskripsi_nilai_raport' => 'deskripsi',
        ])
            ->join('mapel', 'mapel.id_mapel', '=', 'nilai_raport.id_mapel')
            ->where('nilai_raport.id_raport', $id)
            ->orderBy('mapel.nama_mapel')
            ->orderBy('nilai_raport.jenis_nilai_raport', 'desc')
            ->get();

        $data = [];
        for ($i = 0; $i < count($nilai); $i++) {
            array_push($data, [
                'id_mapel'            => $nilai[$i]->id_mapel,
                'mapel'               => $nilai[$i]->mapel,
                $nilai[$i]->jenis     => [
                    'kkm'       => $nilai[$i]->kkm,
                    'nilai'     => $nilai[$i]->nilai,
                    'deskripsi' => $nilai[$i]->deskripsi,
                ],
                $nilai[$i + 1]->jenis => [
                    'kkm'       => $nilai[$i + 1]->kkm,
                    'nilai'     => $nilai[$i + 1]->nilai,
                    'deskripsi' => $nilai[$i + 1]->deskripsi,
                ],
            ]);
            $i += 1;
        }

        return $data;
    }

    public function siswa(Request $request)
    {
        $siswa = F::filter(DB::table('profile_siswa'), [
            'profile_siswa.nis'        => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'profile_siswa.foto'       => 'foto',
            'profile_siswa.id_user'    => 'id',
        ])
            ->where('id_rombel', $request->rombel)
            ->get();
        return F::respon($siswa);
    }

    // menambilkan siswa berdasarkan rombel
    public function nilai_siswa(Request $request)
    {
        $tahun_ajaran = DB::table('tahun_ajaran')
            ->where('status', 1)
            ->first();

        $raport = DB::table('raport')
            ->where('id_user', $request->siswa)
            ->where('id_tahun_ajaran', $tahun_ajaran->id_tahun_ajaran)
            ->count();

        if ($raport == 0) {
            $data = DB::table('raport')
                ->insert([
                    'id_rombel'                => $request->rombel,
                    'id_user'                  => $request->siswa,
                    'id_tahun_ajaran'          => $tahun_ajaran->id_tahun_ajaran,
                    'deskripsi_raport'         => '',
                    'catatan_walikelas_raport' => '',
                    'catatan_orangtua_raport'  => '',
                    'sakit_raport'             => 0,
                    'ijin_raport'              => 0,
                    'alpa_raport'              => 0,
                    'kelakuan_raport'          => '',
                    'kerajinan_raport'         => '',
                    'kerapian_raport'          => '',
                ]);
        }

        $raport = F::filter(DB::table('raport'), [
            'raport.id_raport'                => 'id',
            'raport.deskripsi_raport'         => 'deskripsi',
            'raport.catatan_walikelas_raport' => 'catatan_walikelas',
            'raport.catatan_orangtua_raport'  => 'catatan_orangtua',
            'raport.sakit_raport'             => 'sakit',
            'raport.ijin_raport'              => 'ijin',
            'raport.alpa_raport'              => 'alpa',
            'raport.kelakuan_raport'          => 'kelakuan',
            'raport.kerajinan_raport'         => 'kerajinan',
            'raport.kerapian_raport'          => 'kerapian',
            'tahun_ajaran.nama_tahun_ajaran'  => 'tahun_ajaran',
        ])
            ->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran')
            ->where('raport.id_user', $request->siswa)
            ->where('raport.id_tahun_ajaran', $tahun_ajaran->id_tahun_ajaran)
            ->first();

        $pkl = self::data_pkl($raport->id);

        $ekstra = self::data_ekstrakurikuler($raport->id);

        $prestasi = self::data_prestasi($raport->id);

        $nilai = self::data_nilai($raport->id);

        return F::respon([
            'raport'   => $raport,
            'pkl'      => $pkl,
            'ekstra'   => $ekstra,
            'prestasi' => $prestasi,
            'nilai'    => $nilai,
        ]);
    }

    // menampilkan mapel yang di ampu oleh wali kelas
    public function mapel(Request $request)
    {
        $data = F::filter(DB::table('mapel'), [
            'mapel.id_mapel'   => 'id',
            'mapel.nama_mapel' => 'nama',
        ])
            ->whereRaw('id_jurusan in (select id_jurusan from rombel where id_rombel = ?)', [$request->rombel])
            ->orderBy('mapel.nama_mapel')
            ->get();
        return F::respon($data);
    }

    // menampilkan kelas yang di ampu oleh wali kelas
    public function kelas(Request $request)
    {
        $data = F::filter(DB::table('rombel'), [
            'rombel.id_rombel'             => 'id',
            'rombel.nama_rombel'           => 'rombel',
            'jurusan.nama_jurusan'         => 'jurusan',
            'level_kelas.nama_level_kelas' => 'level',
        ])
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->where('rombel.id_user', $request->user)
            ->get();
        return F::respon($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nilai = json_decode($request->nilai);

        foreach ($nilai as $key => $value) {
            $cek = DB::table('nilai_raport')
                ->where('id_raport', $request->raport)
                ->where('id_mapel', $value->id_mapel)
                ->count();

            if ($cek == 0) {
                DB::table('nilai_raport')
                    ->insert([
                        [
                            'id_raport'              => $request->raport,
                            'id_mapel'               => $value->id_mapel,
                            'kkm_nilai_raport'       => $value->pengetahuan->kkm,
                            'jenis_nilai_raport'     => 'pengetahuan',
                            'nilai_nilai_raport'     => $value->pengetahuan->nilai,
                            'deskripsi_nilai_raport' => $value->pengetahuan->deskripsi,
                        ],
                        [
                            'id_raport'              => $request->raport,
                            'id_mapel'               => $value->id_mapel,
                            'kkm_nilai_raport'       => $value->keterampilan->kkm,
                            'jenis_nilai_raport'     => 'keterampilan',
                            'nilai_nilai_raport'     => $value->keterampilan->nilai,
                            'deskripsi_nilai_raport' => $value->keterampilan->deskripsi,
                        ],
                    ]);
            } else {
                DB::table('nilai_raport')
                    ->where('id_raport', $request->raport)
                    ->where('jenis_nilai_raport', 'pengetahuan')
                    ->where('id_mapel', $value->id_mapel)
                    ->update([
                        'kkm_nilai_raport'       => $value->pengetahuan->kkm,
                        'nilai_nilai_raport'     => $value->pengetahuan->nilai,
                        'deskripsi_nilai_raport' => $value->pengetahuan->deskripsi,
                    ]);

                DB::table('nilai_raport')
                    ->where('id_raport', $request->raport)
                    ->where('jenis_nilai_raport', 'keterampilan')
                    ->where('id_mapel', $value->id_mapel)
                    ->update([
                        'kkm_nilai_raport'       => $value->keterampilan->kkm,
                        'nilai_nilai_raport'     => $value->keterampilan->nilai,
                        'deskripsi_nilai_raport' => $value->keterampilan->deskripsi,
                    ]);
            }
        }

        //return F::respon(self::data_nilai($request->raport));
        return F::respon('Nilai berhasil diperbarui');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // menampilkan kelas berdasarkan mata pelajaran pada setiap guru
    public function guru_kelas(Request $request)
    {
        $data = F::filter(DB::table('rombel'), [
            'rombel.id_rombel'             => 'id',
            'rombel.nama_rombel'           => 'rombel',
            'jurusan.nama_jurusan'         => 'jurusan',
            'level_kelas.nama_level_kelas' => 'level',
        ])
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->join('plot_ajar', function ($j) use ($request) {
                $j->on('plot_ajar.id_rombel', '=', 'rombel.id_rombel');
                $j->where('plot_ajar.id_user', $request->user);
            })
            ->join('tahun_ajaran', function ($j) use ($request) {
                $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'plot_ajar.id_tahun_ajaran');
                $j->where('tahun_ajaran.status', 1);
            })
            ->groupBy('rombel.id_rombel')
            ->orderBy('rombel.nama_rombel')
            ->orderBy('level_kelas.nama_level_kelas')
            ->orderBy('jurusan.nama_jurusan')
            ->get();
        return F::respon($data);
    }

    public function guru_mapel(Request $request)
    {
        $data = F::filter(DB::table('mapel'), [
            'mapel.id_mapel'   => 'id',
            'mapel.nama_mapel' => 'nama',
        ])
            ->join('plot_ajar', function ($j) use ($request) {
                $j->on('plot_ajar.id_mapel', '=', 'mapel.id_mapel');
                $j->where('plot_ajar.id_user', $request->user);
                $j->where('plot_ajar.id_rombel', $request->rombel);
            })
            ->join('tahun_ajaran', function ($j) use ($request) {
                $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'plot_ajar.id_tahun_ajaran');
                $j->where('tahun_ajaran.status', 1);
            })
            ->orderBy('mapel.nama_mapel')
            ->get();
        return F::respon($data);
    }

    public function guru_nilai(Request $request)
    {
        $tahun_ajaran = DB::table('tahun_ajaran')
            ->where('status', 1)
            ->first();

        $siswa = DB::table('profile_siswa')
            ->where('id_rombel', $request->rombel)
            ->get();

        foreach ($siswa as $key => $value) {
            $cek = DB::table('nilai_raport')
                ->join('raport', function ($j) use ($value) {
                    $j->on('raport.id_raport', '=', 'nilai_raport.id_raport');
                    $j->where('raport.id_rombel', $value->id_rombel);
                    $j->where('raport.id_user', $value->id_user);
                })
                ->join('tahun_ajaran', function ($j) use ($request) {
                    $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran');
                    $j->where('tahun_ajaran.status', 1);
                })
                ->where('nilai_raport.id_mapel', $request->mapel)
                ->count();
            if ($cek == 0) {
                $raport = DB::table('raport')
                    ->insertGetId([
                        'id_rombel'                => $request->rombel,
                        'id_user'                  => $value->id_user,
                        'id_tahun_ajaran'          => $tahun_ajaran->id_tahun_ajaran,
                        'deskripsi_raport'         => '',
                        'catatan_walikelas_raport' => '',
                        'catatan_orangtua_raport'  => '',
                        'sakit_raport'             => 0,
                        'ijin_raport'              => 0,
                        'alpa_raport'              => 0,
                        'kelakuan_raport'          => '',
                        'kerajinan_raport'         => '',
                        'kerapian_raport'          => '',
                    ]);

                DB::table('nilai_raport')
                    ->insert([
                        [
                            'id_raport'              => $raport,
                            'id_mapel'               => $request->mapel,
                            'kkm_nilai_raport'       => 0,
                            'jenis_nilai_raport'     => 'pengetahuan',
                            'nilai_nilai_raport'     => 0,
                            'deskripsi_nilai_raport' => '',
                        ],
                        [
                            'id_raport'              => $raport,
                            'id_mapel'               => $request->mapel,
                            'kkm_nilai_raport'       => 0,
                            'jenis_nilai_raport'     => 'keterampilan',
                            'nilai_nilai_raport'     => 0,
                            'deskripsi_nilai_raport' => '',
                        ],
                    ]);
            }
        }
        $nilai = F::filter(DB::table('nilai_raport'), [
            'nilai_raport.id_nilai_raport'        => 'id',
            'nilai_raport.id_mapel'               => 'id_mapel',
            'mapel.nama_mapel'                    => 'mapel',
            'nilai_raport.kkm_nilai_raport'       => 'kkm',
            'nilai_raport.jenis_nilai_raport'     => 'jenis',
            'nilai_raport.nilai_nilai_raport'     => 'nilai',
            'nilai_raport.deskripsi_nilai_raport' => 'deskripsi',
            'profile_siswa.id_user'               => 'user',
            'profile_siswa.nis'                   => 'nis',
            'profile_siswa.nama_siswa'            => 'nama',
        ])
            ->join('raport', function ($j) use ($request) {
                $j->on('raport.id_raport', '=', 'nilai_raport.id_raport');
                $j->where('raport.id_rombel', $request->rombel);
            })
            ->join('profile_siswa', function ($j) use ($request) {
                $j->on('profile_siswa.id_user', '=', 'raport.id_user');
            })
            ->join('mapel', function ($j) use ($request) {
                $j->on('mapel.id_mapel', '=', 'nilai_raport.id_mapel');
                $j->where('mapel.id_mapel', $request->mapel);
            })
            ->join('tahun_ajaran', function ($j) use ($request) {
                $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran');
                $j->where('tahun_ajaran.status', 1);
            })
            ->orderBy('profile_siswa.nama_siswa')
            ->orderBy('nilai_raport.jenis_nilai_raport', 'desc')
            ->orderBy('mapel.nama_mapel')
            ->get();
        $data = [];
        for ($i = 0; $i < count($nilai); $i++) {
            array_push($data, [
                'id_user'             => $nilai[$i]->user,
                'nama'                => $nilai[$i]->nama,
                'nis'                 => $nilai[$i]->nis,
                'id_mapel'            => $nilai[$i]->id_mapel,
                'mapel'               => $nilai[$i]->mapel,
                $nilai[$i]->jenis     => [
                    'kkm'       => $nilai[$i]->kkm,
                    'nilai'     => $nilai[$i]->nilai,
                    'deskripsi' => $nilai[$i]->deskripsi,
                ],
                $nilai[$i + 1]->jenis => [
                    'kkm'       => $nilai[$i + 1]->kkm,
                    'nilai'     => $nilai[$i + 1]->nilai,
                    'deskripsi' => $nilai[$i + 1]->deskripsi,
                ],
            ]);
            $i += 1;
        }

        return F::respon($data);
    }

    public function guru_nilai_simpan(Request $request)
    {
        $nilai = json_decode($request->nilai);

        foreach ($nilai as $key => $value) {

            DB::table('nilai_raport')
                ->where('nilai_raport.jenis_nilai_raport', 'pengetahuan')
                ->where('nilai_raport.id_mapel', $value->id_mapel)
                ->join('raport', function ($j) use ($request, $value) {
                    $j->on('raport.id_raport', '=', 'nilai_raport.id_raport');
                    $j->where('raport.id_rombel', $request->rombel);
                    $j->where('raport.id_user', $value->id_user);
                })
                ->join('tahun_ajaran', function ($j) {
                    $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran');
                    $j->where('tahun_ajaran.status', 1);
                })
                ->update([
                    'nilai_nilai_raport'     => $value->pengetahuan->nilai,
                    'deskripsi_nilai_raport' => $value->pengetahuan->deskripsi,
                ]);

            DB::table('nilai_raport')
                ->where('nilai_raport.jenis_nilai_raport', 'keterampilan')
                ->where('nilai_raport.id_mapel', $value->id_mapel)
                ->join('raport', function ($j) use ($request, $value) {
                    $j->on('raport.id_raport', '=', 'nilai_raport.id_raport');
                    $j->where('raport.id_rombel', $request->rombel);
                    $j->where('raport.id_user', $value->id_user);
                })
                ->join('tahun_ajaran', function ($j) {
                    $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran');
                    $j->where('tahun_ajaran.status', 1);
                })
                ->update([
                    'nilai_nilai_raport'     => $value->keterampilan->nilai,
                    'deskripsi_nilai_raport' => $value->keterampilan->deskripsi,
                ]);
        }
        return F::respon('Nilai siswa berhasil diupdate');
    }

    public function siswa_tahun_ajaran(Request $request)
    {
        $data = F::filter(DB::table('tahun_ajaran'), [
            'tahun_ajaran.id_tahun_ajaran'   => 'id',
            'tahun_ajaran.nama_tahun_ajaran' => 'nama',
        ])
            ->join('raport', function ($j) use ($request) {
                $j->on('raport.id_tahun_ajaran', '=', 'tahun_ajaran.id_tahun_ajaran');
                $j->where('raport.id_user', $request->user);
            })
            ->groupBy('raport.id_tahun_ajaran')
            ->orderBy('tahun_ajaran.status', 'desc')
            ->get();
        return F::respon($data);
    }

    public function siswa_nilai(Request $request)
    {
        $raport = F::filter(DB::table('raport'), [
            'raport.id_raport'                => 'id',
            'raport.deskripsi_raport'         => 'deskripsi',
            'raport.catatan_walikelas_raport' => 'catatan_walikelas',
            'raport.catatan_orangtua_raport'  => 'catatan_orangtua',
            'raport.sakit_raport'             => 'sakit',
            'raport.ijin_raport'              => 'ijin',
            'raport.alpa_raport'              => 'alpa',
            'raport.kelakuan_raport'          => 'kelakuan',
            'raport.kerajinan_raport'         => 'kerajinan',
            'raport.kerapian_raport'          => 'kerapian',
            'tahun_ajaran.nama_tahun_ajaran'  => 'tahun_ajaran',
        ])
            ->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran')
            ->where('raport.id_user', $request->siswa)
            ->where('raport.id_tahun_ajaran', $request->tahun)
            ->first();

        $profil = F::filter(DB::table('raport'), [
            'profile_siswa.id_user'          => 'id',
            'profile_siswa.nis'              => 'nis',
            'profile_siswa.nama_siswa'       => 'nama',
            'profile_gukar.nama_gukar'       => 'nama_guru',
            'rombel.nama_rombel'             => 'rombel',
            'level_kelas.nama_level_kelas'   => 'kelas',
            'jurusan.nama_jurusan'           => 'jurusan',
            'tahun_ajaran.nama_tahun_ajaran' => 'tahun_ajaran',
            'profile_siswa.foto'             => 'foto'
        ])
            ->where('raport.id_raport', $raport->id)
            ->join('profile_siswa', function ($j) use ($request) {
                $j->on('profile_siswa.id_user', '=', 'raport.id_user');
            })
            ->join('rombel', function ($j) use ($request) {
                $j->on('rombel.id_rombel', '=', 'raport.id_rombel');
            })
            ->leftJoin('profile_gukar', function ($j) use ($request) {
                $j->on('profile_gukar.id_user', '=', 'rombel.id_user');
            })
            ->join('jurusan', function ($j) use ($request) {
                $j->on('jurusan.id_jurusan', '=', 'rombel.id_jurusan');
            })
            ->join('level_kelas', function ($j) use ($request) {
                $j->on('level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas');
            })
            ->join('tahun_ajaran', function ($j) use ($request) {
                $j->on('tahun_ajaran.id_tahun_ajaran', '=', 'raport.id_tahun_ajaran');
            })
            ->first();

        $pkl = self::data_pkl($raport->id);

        $ekstra = self::data_ekstrakurikuler($raport->id);

        $prestasi = self::data_prestasi($raport->id);

        $nilai = self::data_nilai($raport->id);

        return F::respon([
            'raport'   => $raport,
            'pkl'      => $pkl,
            'ekstra'   => $ekstra,
            'prestasi' => $prestasi,
            'nilai'    => $nilai,
            'profil'   => $profil,
        ]);
    }
}
