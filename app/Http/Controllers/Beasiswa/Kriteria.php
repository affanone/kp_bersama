<?php

namespace App\Http\Controllers\Beasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Validator;

class Kriteria extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $kategori = $request->kategori ?? null;
        $data = F::filter(DB::table('kriteria_beasiswa'),[
            'kriteria_beasiswa.id_kriteria_beasiswa' => 'id',
            'kriteria_beasiswa.nama_kriteria_beasiswa' => 'nama',
            'kategori_beasiswa.nama_kategori_beasiswa' => 'kategori',
            'kriteria_beasiswa.bobot_kriteria_beasiswa' => 'bobot'
        ])
            ->when($kategori,function($q) use($kategori){
                return $q->where('kriteria_beasiswa.id_kategori_beasiswa',$kategori);
            })
            ->join('kategori_beasiswa','kategori_beasiswa.id_kategori_beasiswa','=','kriteria_beasiswa.id_kategori_beasiswa')
            ->get();
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'nama' => 'required',
            'kategori' => 'required',
            'bobot' => ['required','numeric','max:100']],[
            'nama.required' => 'Nama tidak boleh kosong',
            'kategori.required' => 'Kategori tidak boleh kosong',
            'bobot.required' => 'Bobot tidak boleh kosong',
            'bobot.numeric' => 'Format bobot harus angka',
            'bobot.max' => 'Bobot tidak boleh lebih dari 100'
        ]);
        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        $cek = DB::table('kriteria_beasiswa')
            ->select(DB::raw('(SUM(bobot_kriteria_beasiswa)+'.$request->bobot.') AS total'))
            ->where('id_kategori_beasiswa',$request->kategori)
            ->first();
        if($cek->total>100){
            $validasi->errors()->add('bobot','Total kriteria maksimal 100');
            return F::respon($validasi->errors(),411);
        }

        DB::table('kriteria_beasiswa')
            ->insert([
                'nama_kriteria_beasiswa' => $request->nama,
                'id_kategori_beasiswa' => $request->kategori,
                'bobot_kriteria_beasiswa' => $request->bobot
            ]);
        $request->merge(['kategori'=>null]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'nama' => 'required',
            'kategori' => 'required',
            'bobot' => ['required','numeric','max:100'],
        ],[
            'nama.required' => 'Nama tidak boleh kosong',
            'kategori.required' => 'Kategori tidak boleh kosong',
            'bobot.required' => 'Bobot tidak boleh kosong',
            'bobot.numeric' => 'Format bobot harus angka',
            'bobot.max' => 'Bobot tidak boleh lebih dari 100'
        ]);

        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        $cek = DB::table('kriteria_beasiswa')
            ->select(DB::raw('(SUM(bobot_kriteria_beasiswa)+'.$request->bobot.') AS total'))
            ->where('id_kriteria_beasiswa','<>',$request->id)
            ->where('id_kategori_beasiswa',$request->kategori)
            ->first();
        if($cek->total>100){
            $validasi->errors()->add('bobot','Total kriteria maksimal 100');
            return F::respon($validasi->errors(),411);
        }

        DB::table('kriteria_beasiswa')
            ->where('id_kriteria_beasiswa',$request->id)
            ->update([
                'nama_kriteria_beasiswa' => $request->nama,
                'id_kategori_beasiswa' => $request->kategori,
                'bobot_kriteria_beasiswa' => $request->bobot
            ]);
        $request->merge(['kategori'=>null]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('kriteria_beasiswa')
            ->where('id_kriteria_beasiswa',$request->id)
            ->delete();
        return self::index($request);
    }
}
