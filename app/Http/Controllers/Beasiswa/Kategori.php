<?php

namespace App\Http\Controllers\Beasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Validator;

class Kategori extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('kategori_beasiswa'),[
            'kategori_beasiswa.id_kategori_beasiswa' => 'id',
            'kategori_beasiswa.nama_kategori_beasiswa' => 'nama',
            'tahun_ajaran.nama_tahun_ajaran' => 'tahun'
        ])  
            ->join('tahun_ajaran',function($j){
                $j->on('tahun_ajaran.id_tahun_ajaran','=','kategori_beasiswa.id_tahun_ajaran');
                $j->where('tahun_ajaran.status',1);
            })
            ->get();
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'nama' => 'required|min:2'
        ],[
            'nama.required' => 'Nama tidak boleh kosong',
            'nama.min' => 'Nama minimal 2 karakter'
        ]);
        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        $tahun = DB::table('tahun_ajaran')
            ->where('status',1)
            ->first()->id_tahun_ajaran;
        DB::table('kategori_beasiswa')
            ->insert([
                'nama_kategori_beasiswa' => $request->nama,
                'id_tahun_ajaran' => $tahun
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'nama' => 'required|min:2'
        ],[
            'nama.required' => 'Nama tidak boleh kosong',
            'nama.min' => 'Nama minimal 2 karakter'
        ]);
        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        DB::table('kategori_beasiswa')
            ->where('id_kategori_beasiswa',$request->id)
            ->update([
                'nama_kategori_beasiswa' => $request->nama
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('kategori_beasiswa')
            ->where('id_kategori_beasiswa',$request->id)
            ->delete();
        return self::index($request);
    }
}
