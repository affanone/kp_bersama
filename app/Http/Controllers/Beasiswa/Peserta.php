<?php

namespace App\Http\Controllers\Beasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Validator;

class Peserta extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('profile_siswa'),[
                'profile_siswa.id_user' => 'id',
                'profile_siswa.nis' => 'nis',
                'profile_siswa.nama_siswa' => 'nama',
                'jurusan.nama_jurusan' => 'jurusan',
                'level_kelas.nama_level_kelas' => 'kelas'
            ])
            ->join('rombel','rombel.id_rombel','=','profile_siswa.id_rombel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->whereRaw('id_user not in (select id_user from tbl_nilai_kriteria)')
            ->get();
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'siswa' => 'required'
        ],[
            'siswa.required' => 'Nama tidak boleh kosong'
        ]);
        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        DB::table('kriteria_beasiswa')
            ->insert([
                'id_user' => $request->siswa,
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function kelas(){
        $data = F::filter(DB::table('rombel'),[
            'rombel.id_rombel' => 'id',
            'level_kelas.nama_level_kelas' => 'level',
            'jurusan.nama_jurusan' => 'jurusan',
            'rombel.nama_rombel' => 'kelas'
        ])
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->orderBy('jurusan')
            ->orderBy('level')
            ->orderBy('kelas')
            ->get();
        return F::respon($data);
    }
}
