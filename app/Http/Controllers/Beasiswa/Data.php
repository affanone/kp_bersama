<?php

namespace App\Http\Controllers\Beasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Validator;

class Data extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    public function siswa(){
        $data = F::filter(DB::table('peserta_beasiswa'),[
                'peserta_beasiswa.id_peserta_beasiswa' => 'id',
                'profile_siswa.nis' => 'nis',
                'profile_siswa.nama_siswa' => 'nama',
                'jurusan.nama_jurusan' => 'jurusan',
                'level_kelas.nama_level_kelas' => 'kelas'
            ])
            ->join('profile_siswa','profile_siswa.id_user','=','peserta_beasiswa.id_user')
            ->join('rombel','rombel.id_rombel','=','profile_siswa.id_rombel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->get();
        return F::respon($data);
    }
    public function tahun(){
        $data = F::filter(DB::table('tahun_ajaran'),[
                'id_tahun_ajaran' => 'id',
                'nama_tahun_ajaran' => 'nama',
            ])
            ->get();
        return F::respon($data);
    }
}
