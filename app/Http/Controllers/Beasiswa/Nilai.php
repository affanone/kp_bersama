<?php

namespace App\Http\Controllers\Beasiswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Validator;

class Nilai extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function semua(Request $request){
        $status = $request->status ?? null;
        $cari = $request->cari ?? null;
        $kategori = $request->kategori ?? null;
        $kelas = $request->kelas ?? null;
        $data = F::filter(DB::table('tbl_nilai_kriteria'),[
                'tbl_nilai_kriteria.id_nilai_kriteria' => 'id',
                'tbl_nilai_kriteria.id_user' => 'user',
                'tbl_nilai_kriteria.status_beasiswa' => 'status',
                'profile_siswa.nis' => 'nis',
                'profile_siswa.nama_siswa' => 'nama',
                'jurusan.nama_jurusan' => 'jurusan',
                'level_kelas.nama_level_kelas' => 'kelas',
                'rombel.nama_rombel' => 'rombel',
                'kriteria_beasiswa.nama_kriteria_beasiswa' => 'kriteria',
                'kriteria_beasiswa.bobot_kriteria_beasiswa' => 'bobot',
                'tbl_nilai_kriteria.nilai_kriteria' => 'nilai_kriteria',
                'tbl_nilai_kriteria.nilai_normalisasi' => 'nilai_normalisasi',
                'kategori_beasiswa.nama_kategori_beasiswa' => 'kategori',
                'tahun_ajaran.nama_tahun_ajaran' => 'tahun_ajaran',
            ])
            ->addSelect(DB::raw('sum(tbl_nilai_kriteria.nilai_kriteria/kriteria_beasiswa.bobot_kriteria_beasiswa)/count(tbl_nilai_kriteria.id_user) as nilai'))
            ->groupBy('tbl_nilai_kriteria.id_user')
            ->join('kriteria_beasiswa','kriteria_beasiswa.id_kriteria_beasiswa','=','tbl_nilai_kriteria.id_kriteria_beasiswa')
            ->join('kategori_beasiswa',function($j) use ($kategori){
                $j->on('kategori_beasiswa.id_kategori_beasiswa','=','kriteria_beasiswa.id_kategori_beasiswa')
                    ->when($kategori,function($q) use ($kategori){
                        return $q->where(function($qu) use ($kategori) {
                            $qu->where('kategori_beasiswa.id_kategori_beasiswa',$kategori)
                            ->orWhere('kategori_beasiswa.nama_kategori_beasiswa',$kategori);
                        });
                    });
            })
            ->join('tahun_ajaran',function($j){
                $j->on('tahun_ajaran.id_tahun_ajaran','=','kategori_beasiswa.id_tahun_ajaran');
                $j->where('tahun_ajaran.status',1);
            })
            ->join('profile_siswa',function($j) use ($cari){
                $j->on('profile_siswa.id_user','=','tbl_nilai_kriteria.id_user')
                    ->when($cari,function($q) use ($cari){
                        return $q->where(function($q) use ($cari){
                            $q->where('profile_siswa.nis','like','%'.$cari.'%');
                            $q->orWhere('profile_siswa.nama_siswa','like','%'.$cari.'%');
                        });
                    });
            })
            ->join('rombel',function($j) use ($kelas){
                $j->on('rombel.id_rombel','=','profile_siswa.id_rombel')
                    ->when($kelas,function($q) use ($kelas){
                        return $q->where('rombel.id_rombel',$kelas);
                    });
            })
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            // ->when($status,function($q) use ($status){
            //     return $q->where('tbl_nilai_kriteria.status_beasiswa',$status);
            // })
            // ->orderBy('tahun_ajaran.status','desc')
            ->orderBy('status','desc')
            ->orderBy('nilai','desc')
            ->get();
        return F::respon($data);
    }

    public function status(Request $request){
        DB::table('tbl_nilai_kriteria')
            ->where('id_user',$request->id)
            ->update([
                'status_beasiswa' => ($request->status=='1') ? 0 : 1
            ]);
        return self::index($request);
    }

    public function detail(Request $request){
        $data = F::filter(DB::table('tbl_nilai_kriteria'),[
            'kriteria_beasiswa.nama_kriteria_beasiswa' => 'kriteria',
            'tbl_nilai_kriteria.nilai_kriteria' => 'nilai',
            'kriteria_beasiswa.bobot_kriteria_beasiswa' => 'bobot'
        ])
            ->where('tbl_nilai_kriteria.id_user',$request->user)
            ->join('kriteria_beasiswa','kriteria_beasiswa.id_kriteria_beasiswa','=','tbl_nilai_kriteria.id_kriteria_beasiswa')
            ->join('kategori_beasiswa','kategori_beasiswa.id_kategori_beasiswa','=','kriteria_beasiswa.id_kategori_beasiswa')
            ->join('tahun_ajaran',function($j){
                $j->on('tahun_ajaran.id_tahun_ajaran','=','kategori_beasiswa.id_tahun_ajaran');
                $j->where('tahun_ajaran.status',1);
            })
            ->get();
        return F::respon($data);
    }

    public function index(Request $request)
    {
        $status = $request->status ?? null;
        $cari = $request->cari ?? null;
        $kategori = $request->kategori ?? null;
        $kelas = $request->kelas ?? null;
        $data = F::filter(DB::table('tbl_nilai_kriteria'),[
                'tbl_nilai_kriteria.id_nilai_kriteria' => 'id',
                'tbl_nilai_kriteria.id_user' => 'user',
                'tbl_nilai_kriteria.status_beasiswa' => 'status',
                'profile_siswa.nis' => 'nis',
                'profile_siswa.nama_siswa' => 'nama',
                'jurusan.nama_jurusan' => 'jurusan',
                'level_kelas.nama_level_kelas' => 'kelas',
                'rombel.nama_rombel' => 'rombel',
                'kriteria_beasiswa.nama_kriteria_beasiswa' => 'kriteria',
                'kriteria_beasiswa.bobot_kriteria_beasiswa' => 'bobot',
                'tbl_nilai_kriteria.nilai_kriteria' => 'nilai_kriteria',
                'tbl_nilai_kriteria.nilai_normalisasi' => 'nilai_normalisasi',
                'kategori_beasiswa.nama_kategori_beasiswa' => 'kategori',
                'tahun_ajaran.nama_tahun_ajaran' => 'tahun_ajaran',
            ])
            ->addSelect(DB::raw('sum(tbl_nilai_kriteria.nilai_kriteria/kriteria_beasiswa.bobot_kriteria_beasiswa)/count(tbl_nilai_kriteria.id_user) as nilai'))
            ->groupBy('tbl_nilai_kriteria.id_user')
            ->join('kriteria_beasiswa','kriteria_beasiswa.id_kriteria_beasiswa','=','tbl_nilai_kriteria.id_kriteria_beasiswa')
            ->join('kategori_beasiswa',function($j) use ($kategori){
                $j->on('kategori_beasiswa.id_kategori_beasiswa','=','kriteria_beasiswa.id_kategori_beasiswa')
                    ->when($kategori,function($q) use ($kategori){
                        return $q->where(function($qu) use ($kategori) {
                            $qu->where('kategori_beasiswa.id_kategori_beasiswa',$kategori)
                            ->orWhere('kategori_beasiswa.nama_kategori_beasiswa',$kategori);
                        });
                    });
            })
            ->join('tahun_ajaran',function($j){
                $j->on('tahun_ajaran.id_tahun_ajaran','=','kategori_beasiswa.id_tahun_ajaran');
                $j->where('tahun_ajaran.status',1);
            })
            ->join('profile_siswa',function($j) use ($cari){
                $j->on('profile_siswa.id_user','=','tbl_nilai_kriteria.id_user')
                    ->when($cari,function($q) use ($cari){
                        return $q->where(function($q) use ($cari){
                            $q->where('profile_siswa.nis','like','%'.$cari.'%');
                            $q->orWhere('profile_siswa.nama_siswa','like','%'.$cari.'%');
                        });
                    });
            })
            ->join('rombel',function($j) use ($kelas){
                $j->on('rombel.id_rombel','=','profile_siswa.id_rombel')
                    ->when($kelas,function($q) use ($kelas){
                        return $q->where('rombel.id_rombel',$kelas);
                    });
            })
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            // ->when($status,function($q) use ($status){
            //     return $q->where('tbl_nilai_kriteria.status_beasiswa',$status);
            // })
            // ->orderBy('tahun_ajaran.status','desc')
            ->orderBy('status','desc')
            ->orderBy('nilai','desc')
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $d = json_decode($request->data);
        $data = [];
        foreach ($d->kriteria as $key => $value) {
            array_push($data, [
                'id_kriteria_beasiswa' => $value->id,
                'nilai_kriteria' => $value->value,
                'nilai_normalisasi' => 0,
                'id_user' => $d->siswa,
                'status_beasiswa' => 0,
            ]);
        }
        DB::table('tbl_nilai_kriteria')
            ->insert($data);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validasi = Validator::make($request->all(),[
            'kriteria' => 'required',
            'nilai' => 'required',
            'normalisasi' => 'required',
            'siswa' => 'required',
        ],[
            'kriteria.required' => 'Nama tidak boleh kosong',
            'nilai.required' => 'Nilai tidak boleh kosong',
            'normalisasi.required' => 'Normalisasi tidak boleh kosong',
            'siswa.required' => 'Siswa tidak boleh kosong'
        ]);
        if($validasi->fails())
            return F::respon($validasi->errors(),411);

        DB::table('tbl_nilai_kriteria')
            ->where('id_nilai_kriteria',$request->id)
            ->update([
                'id_kriteria_beasiswa' => $request->kriteria,
                'nilai_kriteria' => $request->nilai,
                'nilai_normalisasi' => $request->normalisasi,
                'id_user' => $request->siswa,
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('tbl_nilai_kriteria')
            ->where('id_user',$request->id)
            ->delete();
        return self::index($request);
    }
}
