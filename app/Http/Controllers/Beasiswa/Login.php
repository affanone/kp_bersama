<?php

namespace App\Http\Controllers\Beasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Hdb as F;
use Illuminate\Support\Facades\Auth;

class Login extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = null;
        if (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
            $u    = Auth::user();
            $user = F::filter(DB::table('user'),[
                'email' =>'username',
                'nama_type' => 'level',
                'user.id_user'   => 'id'
            ])
                ->where('user.id_user',$u->id_user)
                ->join('level_user','level_user.id_user','=','user.id_user')
                ->join('type_user',function($j){
                    $j->on('type_user.id_type','=','level_user.id_type')
                        ->where('type_user.nama_type','like','%admin%');
                })
                ->first();
        }
        
        return F::respon($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
