<?php

namespace App\Http\Controllers\Learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Komentar extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request){
        $limit = $request->limit ?? 10;
        $data = F::filter(DB::table('komentar'),[
            'komentar.id_komentar' => 'id',
            'komentar.isi_komentar' => 'komentar',
            'komentar.tanggal_komentar' => 'tanggal',
            'user.email' => 'user',
            'user.id_user' => 'id_user'
        ])
            ->join('user','user.id_user','=','komentar.id_user')
            ->where('komentar.id_materi',$request->materi)
            ->limit($limit)
            ->orderBy('komentar.tanggal_komentar','desc')
            ->get();

        if($request->mode=='siswa'){
            $jkomentar = DB::table('komentar')
                ->where('komentar.id_materi',$request->materi)
                ->count();
            return F::respon([
                'total' => $jkomentar,
                'data' => $data
            ]);
        }
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('komentar')
            ->insert([
                'id_materi' => $request->materi,
                'id_user' => $request->user,
                'tanggal_komentar' => date('Y-m-d H:i:s'),
                'isi_komentar' => $request->komentar
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('komentar')
            ->where('id_komentar',$request->id)
            ->delete();
        return self::index($request);
    }
}
