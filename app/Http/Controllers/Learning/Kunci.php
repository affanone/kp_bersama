<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Kunci extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('kunci_jawaban'), [
            'id_kunci_jawaban'   => 'id',
            'id_soal'            => 'soal',
            'id_pilihan_jawaban' => 'jawaban',
        ])
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('kunci_jawaban')
            ->insert([
                'id_soal'            => $request->soal,
                'id_pilihan_jawaban' => $request->jawaban,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('kunci_jawaban')
            ->where('id_kunci_jawaban', $request->id)
            ->update([
                'id_soal'            => $request->soal,
                'id_pilihan_jawaban' => $request->jawaban,
            ]);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('kunci_jawaban')
            ->where('id_kunci_jawaban', $request->id)
            ->delete();

        return self::index($request);
    }
}
