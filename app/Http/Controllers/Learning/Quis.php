<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth;

class Quis extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    // berhenti kuis
    public function berhenti(Request $request){
        DB::table('nilai_quis')
            ->where('id_user',$request->user)
            ->where('id_quis',$request->kuis)
            ->update([
                'status_nilai_quis' => 1
            ]);
        return F::respon(true);
    }

    // cek ketersediaan kuis aktif pada halaman siswa
    public function cek_quis(Request $request){
        $date_now = date('Y-m-d H:i:s');
        $data = F::filter(DB::table('quis'),[
            'quis.id_quis' => 'id',
            'mapel.nama_mapel' => 'mapel',
            'quis.waktu_quis' => 'waktu',
            'quis.nama_quis' => 'nama',
            'quis.lama_quis' => 'lama',
            'quis.durasi_quis' => 'durasi',
            'profile_gukar.nama_gukar' => 'guru',
            'a.soal'    => 'soal',
            'nilai_quis.status_nilai_quis' => 'status'
        ])
            ->join('mapel','quis.id_mapel','=','mapel.id_mapel')
            ->join('rombel',function($j){
                $j->on('rombel.id_jurusan','=','mapel.id_jurusan')
                    ->whereRaw('rombel.id_level_kelas = mapel.id_level_kelas');
            })
            ->join('profile_siswa',function($j)use($request){
                $j->on('profile_siswa.id_rombel','=','rombel.id_rombel')
                    ->where('profile_siswa.id_user',$request->user);
            })
            ->join('plot_ajar',function($j){
                $j->on('plot_ajar.id_rombel','=','profile_siswa.id_rombel')
                    ->whereRaw('plot_ajar.id_mapel = mapel.id_mapel');
            })
            ->join('profile_gukar',function($j)use($request){
                $j->on('profile_gukar.id_user','=','plot_ajar.id_user');
            })
            ->join('nilai_quis',function($j)use($request){
                $j->on('nilai_quis.id_quis','=','quis.id_quis')
                    ->where('nilai_quis.id_user',$request->user)
                    ->whereRaw('nilai_quis.tanggal_quis = quis.waktu_quis');
            })
            ->leftJoin(DB::raw('(select a.id_quis, count(a.id_quis) as soal from soal a group by a.id_quis)a'),'a.id_quis','=','quis.id_quis')
            ->groupBy('quis.id_quis')
            ->whereNotNull('quis.waktu_quis')
            ->first();

        return F::respon([
            'kuis'=>$data,
            'timestamp'=>$date_now
        ]);
    }

    public function nilai(Request $request){

        $data = F::filter(DB::table('nilai_quis'),[
            'nilai_quis.id_quis' => 'id',
            'nilai_quis.tanggal_quis' => 'tanggal',
            'nilai_quis.nilai_nilai_quis' => 'nilai',
            'quis.nama_quis' => 'nama',
            'mapel.nama_mapel' => 'mapel',
            'level_kelas.nama_level_kelas' => 'kelas',
            'jurusan.nama_jurusan' => 'jurusan',
            'nilai_quis.jumlah_soal_quis' => 'soal'
        ])
            ->where('nilai_quis.id_user',$request->user)
            ->join('quis','quis.id_quis','=','nilai_quis.id_quis')
            ->join('mapel','mapel.id_mapel','=','quis.id_mapel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','mapel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','mapel.id_jurusan')
            ->orderBy('nilai_quis.tanggal_quis','desc')
            ->paginate(10);

        return F::respon($data);
    }

    public function end(Request $request){
        $date = date('Y-m-d H:i:s');

        if($request->autostop=='on'){
            DB::table('quis')
                ->where('id_quis',$request->id)
                ->update([
                    'waktu_quis'=>null,
                    'durasi_quis'=>0
                ]);
            return F::respon($date);
        }

        if (Auth::attempt(['id_user' => $request->user, 'password' => $request->password])) {
            DB::table('quis')
                ->where('id_quis',$request->id)
                ->update([
                    'waktu_quis'=>null,
                    'durasi_quis'=>0
                ]);
            return F::respon($date);
        }else

        return F::respon('Password yang anda masukkan salah!',411);
    }

    public function start(Request $request){
        $jml_soal = DB::table('soal')
            ->where('id_quis',$request->id)
            ->count();
            
        $date = date('Y-m-d H:i:s');
        if (Auth::attempt(['id_user' => $request->user, 'password' => $request->password])) {
            DB::table('quis')
                ->where('id_quis',$request->id)
                ->update([
                    'waktu_quis'=>$date,
                    'durasi_quis' => isset($request->autostop) ? 1 : 0
                ]);

            $peserta = json_decode($request->peserta);
            $data = [];
            foreach ($peserta as $key => $value) {
                array_push($data, [
                    'id_user' => $value,
                    'nilai_nilai_quis' => 0,
                    'tanggal_quis' => $date,
                    'id_quis' => $request->id,
                    'status_nilai_quis' => 0,
                    'jumlah_soal_quis' => $jml_soal,
                ]);
            }
            DB::table('nilai_quis')
                ->insert($data);
            return F::respon($date);
        }

        return F::respon('Password yang anda masukkan salah!',411);
    }

    public function info(Request $request){
        $data = F::filter(DB::table('quis'), [
            'quis.id_quis'    => 'id',
            'quis.id_mapel'   => 'mapel',
            'quis.nama_quis'  => 'nama',
            'quis.waktu_quis' => 'waktu',
            'quis.durasi_quis' => 'autostop',
            'quis.lama_quis'  => 'lama',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jurusan.nama_jurusan'  => 'jurusan',
            'rombel.nama_rombel' => 'rombel'
        ])
            ->addSelect(DB::raw('a.soal'))
            ->join('mapel',function($j)use($request){
                $j->on('mapel.id_mapel','=','quis.id_mapel');
            })
            ->join('jurusan',function($j)use($request){
                $j->on('jurusan.id_jurusan','=','mapel.id_jurusan');
            })
            ->join('level_kelas',function($j)use($request){
                $j->on('level_kelas.id_level_kelas','=','mapel.id_level_kelas');
            })
            ->join('rombel',function($j)use($request){
                $j->on('rombel.id_jurusan','=','jurusan.id_jurusan');
                $j->whereRaw('rombel.id_level_kelas = level_kelas.id_level_kelas');
            })
            ->join(DB::raw(' (select a.id_quis, count(a.id_quis) as soal from soal a where a.id_quis = '.$request->id.')a'),function($j){
                $j->on('a.id_quis','=','quis.id_quis');
            })
            ->where('quis.id_quis',$request->id)
            ->first();
        return F::respon($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('quis'), [
            'quis.id_quis'    => 'id',
            'quis.id_mapel'   => 'mapel',
            'quis.nama_quis'  => 'nama',
            'quis.waktu_quis' => 'waktu',
            'quis.lama_quis'  => 'lama',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jurusan.nama_jurusan'  => 'jurusan',
            'rombel.nama_rombel'  => 'rombel',
        ])
            ->when($cari,function($q,$cari){
                return $q->where('nama_quis','like','%'.$cari.'%');
            })
            ->join('soal',function($j)use($request){
                $j->on('soal.id_quis','=','quis.id_quis')
                    ->where('soal.id_user',$request->guru);
            })
            ->join('mapel',function($j)use($request){
                $j->on('mapel.id_mapel','=','quis.id_mapel');
            })
            ->join('jurusan',function($j)use($request){
                $j->on('jurusan.id_jurusan','=','mapel.id_jurusan');
            })
            ->join('level_kelas',function($j)use($request){
                $j->on('level_kelas.id_level_kelas','=','mapel.id_level_kelas');
            })
            ->join('rombel',function($j)use($request){
                $j->on('rombel.id_jurusan','=','jurusan.id_jurusan');
                $j->whereRaw('rombel.id_level_kelas = level_kelas.id_level_kelas');
            })
            ->groupBy('quis.id_quis')
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'  => 'required',
            // 'waktu' => 'required|date_format:Y-m-d H:i:s',
            'mapel' => 'required',
            'lama'  => 'required|date_format:H.i.s',
        ], [
            'nama.required'     => 'Tidak boleh kosong!',
            'waktu.required'    => 'Tidak boleh kosong!',
            'waktu.date_format' => 'Format tidak diperbolehkan!',
            'lama.required'     => 'Tidak boleh kosong!',
            'lama.date_format'  => 'Format harus jam!',
            'mapel.required'    => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        $id = DB::table('quis')
            ->insertGetId([
                'id_mapel'    => $request->mapel,
                'waktu_quis'  => null ,
                'durasi_quis' => 0,
                'nama_quis'   => $request->nama,
                'lama_quis'   => str_replace('.', ':', $request->lama),
            ]);

        DB::table('soal')
            ->insert([
                'id_user' => $request->guru,
                'id_quis' => $id,
                'soal' => 'HALO...! INI ADALAH CONTOH SOAL YANG TELAH KAMI BUAT!'
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'  => 'required',
            //'waktu' => 'required|date_format:Y-m-d H:i:s',
            'mapel' => 'required',
            'lama'  => 'required|date_format:H.i.s',
        ], [
            'nama.required'     => 'Tidak boleh kosong!',
            'waktu.required'    => 'Tidak boleh kosong!',
            'waktu.date_format' => 'Format tidak diperbolehkan!',
            'lama.required'     => 'Tidak boleh kosong!',
            'lama.date_format'  => 'Format harus jam!',
            'mapel.required'    => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('quis')
            ->where('id_quis', $request->id)
            ->update([
                'id_mapel'   => $request->mapel,
                'nama_quis'  => $request->nama,
                'lama_quis'  => str_replace('.', ':', $request->lama),
            ]);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        DB::table('soal')
            ->where('id_quis', $request->id)
            ->delete();

        DB::table('quis')
            ->where('id_quis', $request->id)
            ->delete();

        return self::index($request);
    }
}
