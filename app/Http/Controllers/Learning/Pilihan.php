<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Pilihan extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('pilihan_jawaban'), [
            'pilihan_jawaban.id_pilihan_jawaban'   => 'id',
            'pilihan_jawaban.id_soal' => 'soal',
            'text_pilihan_jawaban' => 'jawaban',
            'kunci_jawaban.id_pilihan_jawaban'      => 'benar'
        ])
            ->join('soal','soal.id_soal','=','pilihan_jawaban.id_soal')
            ->join('quis',function($j)use($request){
                $j->on('quis.id_quis','=','soal.id_quis');
                $j->where('quis.id_quis',$request->kuis);
            })
            ->groupBy('pilihan_jawaban.id_pilihan_jawaban')
            ->leftJoin('kunci_jawaban',function($j){
                $j->on('kunci_jawaban.id_pilihan_jawaban','=','pilihan_jawaban.id_pilihan_jawaban');
                $j->whereRaw('soal.id_soal = kunci_jawaban.id_soal');
            })
            ->get();
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('pilihan_jawaban')
            ->insert([
                'id_soal'              => $request->soal,
                'text_pilihan_jawaban' => $request->jawaban,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('pilihan_jawaban')
            ->where('id_pilihan_jawaban',$request->id)
            ->update([
                'id_soal'              => $request->soal,
                'text_pilihan_jawaban' => $request->jawaban,
            ]);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('pilihan_jawaban')
            ->where('id_pilihan_jawaban',$request->id)
            ->delete();

        return self::index($request);
    }
}
