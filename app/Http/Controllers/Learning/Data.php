<?php

namespace App\Http\Controllers\Learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;

class Data extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->data ?? null;

        $my_data = [
            'mapel',
            'jurusan',
            'level_kelas',
            'kelas',
            'kelas_guru',
            'mapel_guru',
            'format_lampiran'
        ];

        $v = Validator::make($request->all(), [
            'data' => 'required|string',
        ], [
            'data.required' => 'Data apa saja yang dibutuhkan?',
            'data.string'   => 'Format data harus string, ex: data1,data2,data3...',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        } else {
            $res = [];
            $ex  = explode(',', $request->data);

            foreach ($ex as $key => $value) {
                if (!in_array($value, $my_data)) {
                    $v->errors()->add('data', 'data [' . $value . '] tidak tersedia, gunakan data ini : ' . substr(join($my_data, ', '), 0, strlen(join($my_data, ', '))));
                    return F::respon($v->errors(), 411);
                }

                switch ($value) {
                    case 'mapel_guru':
                        $res[$value] = DB::table('mapel')
                            ->join('plot_ajar',function($j)use($request){
                                $kelas = $request->kelas ?? null;
                                $j->on('plot_ajar.id_mapel','=','mapel.id_mapel')
                                    ->when($kelas,function($q)use($kelas){
                                        $q->where('plot_ajar.id_rombel',$kelas);
                                    })
                                    ->where('plot_ajar.id_user',$request->guru)
                                    ->whereRaw('plot_ajar.id_tahun_ajaran in (select id_tahun_ajaran from tahun_ajaran where status=  ?)',[1]);
                            })
                            ->groupBy('mapel.id_mapel')
                            ->get();
                        break;
                    case 'mapel':
                        $res[$value] = DB::table('mapel')
                            ->get();
                        break;
                    case 'jurusan':
                        $res[$value] = DB::table('jurusan')
                            ->get();
                        break;
                    case 'level_kelas':
                        $res[$value] = DB::table('level_kelas')
                            ->get();
                        break;
                    case 'kelas':
                        $quis = $request->quis ?? null;
                        $res[$value] = $data = F::filter(DB::table('rombel'),[
                            'rombel.id_rombel' => 'id',
                            'level_kelas.nama_level_kelas' => 'level',
                            'jurusan.nama_jurusan' => 'jurusan',
                            'rombel.nama_rombel' => 'kelas'
                        ])
                            ->when($quis,function($j,$quis){
                                $j->join('mapel',function($j1){
                                    $j1->on('mapel.id_jurusan','=','rombel.id_jurusan');
                                    $j1->whereRaw('mapel.id_level_kelas = rombel.id_level_kelas');
                                });
                                $j->join('quis',function($j2)use($quis){
                                    $j2->on('quis.id_mapel','=','mapel.id_mapel');
                                    $j2->where('quis.id_quis',$quis);
                                });
                            })
                            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
                            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
                            ->orderBy('jurusan')
                            ->orderBy('level')
                            ->orderBy('kelas')
                            ->get();
                        break;
                    case 'kelas_guru':
                        $quis = $request->quis ?? null;
                        $res[$value] = $data = F::filter(DB::table('rombel'),[
                            'rombel.id_rombel' => 'id',
                            'level_kelas.nama_level_kelas' => 'level',
                            'jurusan.nama_jurusan' => 'jurusan',
                            'rombel.nama_rombel' => 'kelas'
                        ])
                            ->join('plot_ajar',function($j)use($request){
                                $j->on('plot_ajar.id_rombel','=','rombel.id_rombel')
                                    ->where('plot_ajar.id_user',$request->guru);
                            })
                            ->join('tahun_ajaran',function($j){
                                $j->on('tahun_ajaran.id_tahun_ajaran','=','plot_ajar.id_tahun_ajaran')
                                    ->where('tahun_ajaran.status',1);
                            })
                            ->groupBy('plot_ajar.id_rombel')
                            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
                            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
                            ->orderBy('jurusan')
                            ->orderBy('level')
                            ->orderBy('kelas')
                            ->get();
                        break;
                    case 'format_lampiran':
                        $res[$value] = 'pdf/doc/docx/zip';
                        break;
                }
            }
            return F::respon($res);
        }
    }
}
