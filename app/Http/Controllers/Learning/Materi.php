<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class Materi extends Controller
{

    public function siswa(Request $request){
        $siswa = DB::table('profile_siswa')
            ->where('id_user',$request->user)
            ->first();

        $rombel = $siswa->id_rombel;

        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('materi'), [
            'materi.id_materi'   => 'id',
            'materi.id_rombel'   => 'id_rombel',
            'rombel.nama_rombel' => 'rombel',
            'level_kelas.nama_level_kelas' => 'kelas',
            'jurusan.nama_jurusan' => 'jurusan',
            'materi.id_user'     => 'guru',
            'materi.id_mapel'    => 'id_mapel',
            'mapel.nama_mapel'   => 'mapel',
            'materi.nama_materi' => 'nama',
            'materi.isi_materi'  => 'isi',
            'materi.file_materi' => 'file',
            'materi.tanggal_materi' => 'tanggal',
            'materi.status_materi' => 'status',
        ])
            ->addSelect(DB::raw('IFNULL(a.jumlah,0) as komentar'))
            ->where('materi.id_rombel',$rombel)
            ->when($cari,function($q)use($cari){
                $q->where('materi.nama_materi','like','%'.$cari.'%');
            })
            ->leftJoin(DB::raw(' (select a.id_materi, count(a.id_materi) as jumlah from komentar a group by a.id_materi)a') ,'materi.id_materi','=','a.id_materi')
            ->join('mapel','mapel.id_mapel','=','materi.id_mapel')
            ->join('rombel','rombel.id_rombel','=','materi.id_rombel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->orderBy('materi.tanggal_materi','desc')
            ->paginate(10);
        return F::respon($data);
    }

    public function download(Request $request){
        $file = $request->file;
        $exists = Storage::disk('materi')->exists($file);
        if(!$exists)
            return F::respon('FILE NOT FOUND',403);
        else
            return Storage::disk('materi')->download($request->file);
    }

    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function subject(Request $request){
        $data = F::filter(DB::table('materi'),[
            'rombel.nama_rombel' => 'rombel',
            'level_kelas.nama_level_kelas' => 'kelas',
            'jurusan.nama_jurusan' => 'jurusan',
            'mapel.nama_mapel' => 'mapel',
            'materi.tanggal_materi' => 'tanggal',
            'materi.nama_materi' => 'subjek',
            'materi.isi_materi' => 'isi',
            'status_materi' => 'komentar',
            'file_materi' => 'file'
        ])
            ->join('mapel','mapel.id_mapel','=','materi.id_mapel')
            ->join('rombel','rombel.id_rombel','=','materi.id_rombel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->where('materi.id_materi',$request->id)
            ->first();

        return F::respon($data);
    }

    public function status(Request $request){
        DB::table('materi')
            ->where('id_materi',$request->id)
            ->update([
                'status_materi' => ($request->status) ? 0 : 1
            ]);

        return self::index($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('materi'), [
            'materi.id_materi'   => 'id',
            'materi.id_rombel'   => 'id_rombel',
            'rombel.nama_rombel' => 'rombel',
            'level_kelas.nama_level_kelas' => 'kelas',
            'jurusan.nama_jurusan' => 'jurusan',
            'materi.id_user'     => 'guru',
            'materi.id_mapel'    => 'id_mapel',
            'mapel.nama_mapel'   => 'mapel',
            'materi.nama_materi' => 'nama',
            'materi.isi_materi'  => 'isi',
            'materi.file_materi' => 'file',
            'materi.tanggal_materi' => 'tanggal',
            'materi.status_materi' => 'status',
        ])
            ->when($cari,function($q)use($cari){
                $q->where('materi.nama_materi','like','%'.$cari.'%');
            })
            ->join('mapel','mapel.id_mapel','=','materi.id_mapel')
            ->join('rombel','rombel.id_rombel','=','materi.id_rombel')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->orderBy('materi.tanggal_materi','desc')
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $validator = Validator::make($request->all(), [
            'kelas' => 'required',
            'guru'   => 'required',
            'mapel'  => 'required',
            'nama'   => 'required',
            'isi'    => 'required',
        ], [
            'kelas.required' => 'Tidak boleh kosong!',
            'guru.required'   => 'Tidak boleh kosong!',
            'mapel.required'  => 'Tidak boleh kosong!',
            'nama.required'   => 'Tidak boleh kosong!',
            'isi.required'    => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            if($request->hasFile('lampiran')){
                $file = Validator::make($request->all(),[
                    'lampiran' => 'mimes:pdf,doc,docx,zip'
                ],[
                    'lampiran.mimes' => 'Jenis file tidak didukung pdf/doc/docx/zip'
                ]);

                if($file->fails()){
                    $validator->errors()->add('lampiran','Jenis file tidak didukung!');
                    return F::respon($validator->errors(), 411);
                }
            }       
            return F::respon($validator->errors(), 411);
        }

        $newname = null;

        if ($request->hasFile('lampiran')) {
            $File          = $request->file('lampiran');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $request->file('lampiran')->storeAs('', $newname, 'materi');
        }

        $materi = DB::table('materi')
            ->insertGetId([
                'id_rombel'   => $request->kelas,
                'id_user'     => $request->guru,
                'id_mapel'    => $request->mapel,
                'nama_materi' => $request->nama,
                'isi_materi'  => $request->isi,
                'file_materi' => $newname,
                'tanggal_materi' => date('Y-m-d H:i:s')
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kelas' => 'required',
            'guru'   => 'required',
            'mapel'  => 'required',
            'nama'   => 'required',
            'isi'    => 'required',
        ], [
            'kelas.required' => 'Tidak boleh kosong!',
            'guru.required'   => 'Tidak boleh kosong!',
            'mapel.required'  => 'Tidak boleh kosong!',
            'nama.required'   => 'Tidak boleh kosong!',
            'isi.required'    => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            if($request->hasFile('lampiran')){
                $file = Validator::make($request->all(),[
                    'lampiran' => 'mimes:pdf,doc,docx,zip'
                ],[
                    'lampiran.mimes' => 'Jenis file tidak didukung pdf/doc/docx/zip'
                ]);

                if($file->fails()){
                    $validator->errors()->add('lampiran','Jenis file tidak didukung!');
                    return F::respon($validator->errors(), 411);
                }
            }       
            return F::respon($validator->errors(), 411);
        }

        $newname = null;
        $update = [
                'id_rombel'   => $request->kelas,
                'id_user'     => $request->guru,
                'id_mapel'    => $request->mapel,
                'nama_materi' => $request->nama,
                'isi_materi'  => $request->isi,
            ];
        if ($request->hasFile('lampiran')) {
            $materi = DB::table('materi')
                ->where('id_materi',$request->id)
                ->first();

            if($materi->file_materi!=null)
                Storage::disk('materi')->delete($materi->file_materi);
            
            $File          = $request->file('lampiran');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $request->file('lampiran')->storeAs('', $newname, 'materi');
            $update['file_materi'] = $newname;
        }

        DB::table('materi')
            ->where('id_materi',$request->id)
            ->update($update);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $materi = DB::table('materi')
            ->where('id_materi',$request->id)
            ->first();

        if($materi->file_materi!=null)
            Storage::disk('materi')->delete($materi->file_materi);

        DB::table('materi')
            ->where('id_materi', $request->id)
            ->delete();

        return self::index($request);
    }
}
