<?php

namespace App\Http\Controllers\Learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Room extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function aktif(Request $request){
        $quis = DB::table('quis')
            ->select('quis.waktu_quis')
            ->where('id_quis',$request->kuis)
            ->first();

        $siswa = DB::table('profile_siswa')
            ->select('profile_siswa.id_rombel')
            ->join('nilai_quis',function($j)use($request,$quis){
                $j->on('nilai_quis.id_user','=','profile_siswa.id_user');
                $j->where('nilai_quis.id_quis',$request->kuis);
                $j->where('nilai_quis.tanggal_quis',$quis->waktu_quis);
            })
            ->first();

        $data = F::filter(DB::table('profile_siswa'),[
            'profile_siswa.id_user' => 'id',
            'profile_siswa.nis' => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'b.id_quis' => 'kuis',
            'b.tanggal_quis' => 'tanggal',
            'b.nilai_nilai_quis' => 'nilai',
        ])
            ->addSelect(DB::raw('IFNULL(a.id_quis,NULL) AS aktif '))
            ->where('id_rombel',$siswa->id_rombel)
            ->leftJoin(DB::raw('(select a.id_user, a.id_quis, a.tanggal_quis from nilai_quis a)a'),function($j)use($request,$quis){
                $j->on('a.id_user','=','profile_siswa.id_user');
                $j->where('a.id_quis',$request->kuis);
                $j->where('a.tanggal_quis',$quis->waktu_quis);
            })

            ->leftJoin(DB::raw('(select b.id_user, b.id_quis, b.tanggal_quis,b.nilai_nilai_quis from nilai_quis b)b'),function($j)use($request,$quis){
                $j->on('b.id_user','=','profile_siswa.id_user');
                $j->where('b.id_quis',$request->kuis);
                $j->where('b.tanggal_quis','<>',$quis->waktu_quis);
            })
            ->orderBy('a.id_quis','desc')
            ->orderBy('b.id_quis','asc')
            ->orderBy('profile_siswa.nama_siswa')
            ->get();

        return F::respon([
            'peserta' => $data,
            'kelas' => $siswa->id_rombel
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('profile_siswa'),[
            'profile_siswa.id_user' => 'id',
            'profile_siswa.nis' => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'nilai_quis.id_quis' => 'kuis',
            'nilai_quis.tanggal_quis' => 'tanggal',
            'nilai_quis.nilai_nilai_quis' => 'nilai',
        ])
            ->addSelect(DB::raw('NULL AS aktif '))
            ->where('id_rombel',$request->rombel)
            ->leftJoin('nilai_quis',function($j)use($request){
                $j->on('nilai_quis.id_user','=','profile_siswa.id_user');
                $j->where('nilai_quis.id_quis',$request->kuis);
            })
            ->orderBy('nilai_quis.id_quis','asc')
            ->orderBy('profile_siswa.nama_siswa')
            ->get();
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
