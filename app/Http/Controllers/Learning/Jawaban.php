<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Jawaban extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = F::filter(DB::table('jawaban_siswa'), [
            'id_jawaban_siswa'     => 'id',
            'id_soal'              => 'soal',
            'id_pilihan_jawaban'   => 'jawaban',
            'id_user'              => 'siswa',
            'status_jawaban_siswa' => 'status',
        ])
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // jika diberhentikan secara manual oleh guru
        $cek_quis = DB::table('quis')
            ->join('nilai_quis',function($j)use($request){
                $j->on('nilai_quis.id_quis','=','quis.id_quis')
                    ->where('nilai_quis.id_user',$request->siswa)
                    ->whereRaw('nilai_quis.tanggal_quis = quis.waktu_quis');
            })
            ->whereNotNull('quis.waktu_quis')
            ->count();

        if($cek_quis==0)
            return F::respon('expired',411);

        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
            'siswa'   => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
            'siswa.required'   => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        $cek = DB::table('kunci_jawaban')
            ->where('id_pilihan_jawaban', $request->jawaban)
            ->where('id_soal', $request->soal)
            ->count();

        $cek_J = DB::table('jawaban_siswa')
            ->where('id_user', $request->siswa)
            ->where('id_soal', $request->soal)
            ->count();

        $benar = ($cek > 0) ? 1 : 0;
        if ($cek_J > 0) {
            DB::table('jawaban_siswa')
                ->where('id_user', $request->siswa)
                ->where('id_soal', $request->soal)
                ->update([
                    'status_jawaban_siswa' => $benar,
                ]);
        }else{
            DB::table('jawaban_siswa')
                ->insert([
                    'id_soal'              => $request->soal,
                    'id_pilihan_jawaban'   => $request->jawaban,
                    'id_user'              => $request->siswa,
                    'status_jawaban_siswa' => $benar,
                ]);
        }

        $total_benar = DB::table('jawaban_siswa')
            ->where('jawaban_siswa.id_user',$request->siswa)
            ->where('jawaban_siswa.status_jawaban_siswa',1)
            ->join('soal',function($j)use($request){
                $j->on('soal.id_soal','=','jawaban_siswa.id_soal')
                    ->where('soal.id_quis',$request->kuis);
            })
            ->count();

        $jml_soal = DB::table('soal')
            ->where('id_quis',$request->kuis)
            ->count();

        $nilai = (100/$jml_soal)*$total_benar;

        DB::table('nilai_quis')
            ->where('id_user',$request->siswa)
            ->where('id_quis',$request->kuis)
            ->update([
                'nilai_nilai_quis' => $nilai
            ]);

        $ddd = DB::table('jawaban_siswa')
            ->where('jawaban_siswa.id_user',$request->siswa)
            ->where('jawaban_siswa.status_jawaban_siswa',1)
            ->join('soal',function($j)use($request){
                $j->on('soal.id_soal','=','jawaban_siswa.id_soal')
                    ->where('soal.id_quis',$request->kuis);
            })
            ->get();
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'soal'    => 'required',
            'jawaban' => 'required',
            'siswa'   => 'required',
        ], [
            'soal.required'    => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
            'siswa.required'   => 'Tidak boleh kosong!',
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        $cek = DB::table('kunci_jawaban')
            ->where('id_pilihan_jawaban', $request->jawaban)
            ->where('id_soal', $request->soal)
            ->count();

        DB::table('jawaban_siswa')
            ->where('id_jawaban_siswa', $request->id)
            ->update([
                'id_soal'              => $request->soal,
                'id_pilihan_jawaban'   => $request->jawaban,
                'id_user'              => $request->siswa,
                'status_jawaban_siswa' => ($cek > 0) ? 1 : 0,
            ]);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('jawaban_siswa')
            ->where('id_jawaban_siswa', $request->id)
            ->delete();

        return self::index($request);
    }
}
