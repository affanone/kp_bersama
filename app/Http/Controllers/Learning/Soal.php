<?php

namespace App\Http\Controllers\learning;

use App\Helpers\Handi as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Soal extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function siswa(Request $request){

        $soal = F::filter(DB::table('soal'),[
            'soal.id_soal' => 'id',
            'soal.soal' => 'soal'
        ])
            ->where('id_quis',$request->kuis)
            ->get();
        $jawab = [];
        foreach ($soal as $key => $value) {
            $jawaban = F::filter(DB::table('pilihan_jawaban'),[
                'pilihan_jawaban.id_pilihan_jawaban' => 'id',
                'pilihan_jawaban.text_pilihan_jawaban' => 'jawaban',
                'jawaban_siswa.id_pilihan_jawaban' => 'dijawab'
            ])
                ->leftJoin('jawaban_siswa',function($j)use($value,$request) {
                    $j->on('jawaban_siswa.id_pilihan_jawaban','=','pilihan_jawaban.id_pilihan_jawaban');
                    $j->where('jawaban_siswa.id_soal',$value->id);
                    $j->where('jawaban_siswa.id_user',$request->user);
                })
                ->where('pilihan_jawaban.id_soal',$value->id)
                ->get();
            array_push($jawab,[
                'soal' => $value,
                'jawab' => $jawaban
            ]);
        }
        return F::respon($jawab);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('soal'), [
            'id_soal' => 'id',
            'id_user' => 'guru',
            'soal.id_quis' => 'quis',
            'soal'    => 'soal',
        ])
            ->when($cari,function($q,$cari){
                return $q->where('soal.soal','like','%'.$cari.'%');
            })
            ->join('quis',function($j)use($request){
                $j->on('quis.id_quis','=','soal.id_quis');
                $j->where('quis.id_quis',$request->kuis);
            })
            ->paginate(5);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'guru' => 'required',
            'quis' => 'required',
            'soal' => 'required',
            'jawaban' => ['required','array','min:4',function($attr,$val,$err){
                $abc = ['A','B','C','D'];
                foreach ($val as $key => $value) {
                    if(empty($value))
                        return $err('Jawaban '.$abc[$key].' tidak boleh kosong!');
                }
            }],
            'benar' => 'required'
        ], [
            'guru.required' => 'Tidak boleh kosong!',
            'quis.required' => 'Tidak boleh kosong!',
            'soal.required' => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
            'jawaban.min' => 'Jawaban minimal 4!',
            'jawaban.array' => 'Jawaban harus array!',
            'benar.required' => 'Yang mana yang benar?' 
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        $soal = DB::table('soal')
            ->insertGetId([
                'id_user' => $request->guru,
                'id_quis' => $request->quis,
                'soal'    => $request->soal,
            ]);

        $j = null;
        foreach ($request->jawaban as $key => $value) {
            $jawaban = DB::table('pilihan_jawaban')
            ->insertGetId([
                'id_soal' => $soal,
                'text_pilihan_jawaban' => $value
            ]);
            if($request->benar==$key)
                $j = $jawaban;
        }
        
        $cek = DB::table('kunci_jawaban')   
            ->where('id_soal',$soal)
            ->count();
        if($cek>0){
            DB::table('kunci_jawaban')   
                ->where('id_soal',$soal)
                ->update([
                    'id_pilihan_jawaban' => $j
                ]);
        }else{
            DB::table('kunci_jawaban')
                ->insert([
                    'id_soal' => $soal,
                    'id_pilihan_jawaban' => $j
                ]);
        }

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'guru' => 'required',
            'quis' => 'required',
            'soal' => 'required',
            'jawaban' => ['required','array','min:4',function($attr,$val,$err){
                $abc = ['A','B','C','D'];
                foreach ($val as $key => $value) {
                    if(empty($value))
                        return $err('Jawaban '.$abc[$key].' tidak boleh kosong!');
                }
            }],
            'benar' => 'required'
        ], [
            'guru.required' => 'Tidak boleh kosong!',
            'quis.required' => 'Tidak boleh kosong!',
            'soal.required' => 'Tidak boleh kosong!',
            'jawaban.required' => 'Tidak boleh kosong!',
            'jawaban.min' => 'Jawaban minimal 4!',
            'jawaban.array' => 'Jawaban harus array!',
            'benar.required' => 'Yang mana yang benar?' 
        ]);

        if ($validator->fails()) {
            return F::respon($validator->errors(), 411);
        }

        DB::table('soal')
            ->where('id_soal',$request->id)
            ->update([
                'soal'    => $request->soal,
            ]);

        $j = null;
        
        $jawaban = DB::table('pilihan_jawaban')
            ->where('id_soal',$request->id)
            ->get();

        foreach ($request->jawaban as $key => $value) {
            if(isset($jawaban[$key])){
                $id = $jawaban[$key]->id_pilihan_jawaban;
                DB::table('pilihan_jawaban')
                ->where('id_pilihan_jawaban',$id)
                ->update([
                    'text_pilihan_jawaban' => $value
                ]);
                if($request->benar==$key)
                    $j = $id;
            }else{
                $id = DB::table('pilihan_jawaban')
                ->insertGetId([
                    'id_soal' => $request->id,
                    'text_pilihan_jawaban' => $value
                ]);
                if($request->benar==$key)
                    $j = $id;
            }
        }
        
        $cek = DB::table('kunci_jawaban')   
            ->where('id_soal',$request->id)
            ->count();
        if($cek>0){
            DB::table('kunci_jawaban')   
                ->where('id_soal',$request->id)
                ->update([
                    'id_pilihan_jawaban' => $j
                ]);
        }else{
            DB::table('kunci_jawaban')
                ->insert([
                    'id_soal' => $request->id,
                    'id_pilihan_jawaban' => $j
                ]);
        }

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('kunci_jawaban')
            ->where('id_soal', $request->id)
            ->delete();
        DB::table('pilihan_jawaban')
            ->where('id_soal', $request->id)
            ->delete();
        DB::table('soal')
            ->where('id_soal', $request->id)
            ->delete();

        return self::index($request);
    }
}
