<?php

namespace App\Http\Controllers\Bk;

use App\Helpers\Fdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class SetpelanggarC extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function cari(Request $request){
        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'    => 'id',
            'nis'        => 'nis',
            'nama_siswa' => 'nama',
        ])
            ->where('nis','like','%'.$request->cari.'%')
            ->orWhere('nama_siswa','like','%'.$request->cari.'%')
            ->limit(30)
            ->get();
        if(count($profile)>1){
            //lebih
            return F::respon([
                'mode' => 'multi',
                'data' => $profile
            ]);
        }else if(count($profile)==1){
            //satu
            return F::respon([
                'mode' => 'one',
                'nis' => $profile[0]->nis
            ]);
        }else{
            //tak ditemukan
            return F::respon('Siswa tidak ditemukan', 411);
        }
    }

    public function index(Request $request)
    {
        $pelanggaran = F::filter(DB::table('pelanggaran_siswa'), [
            'id_pelanggaran_siswa'          => 'id',
            'view_pelanggaran_siswa'        => 'pelanggaran',
            'view_sanksi_pelanggaran_siswa' => 'sanksi',
            'view_surat_pelanggaran_siswa'  => 'surat',
            'apoint_pelanggaran_siswa'      => 'point',
            'tgl_pelanggaran_siswa'         => 'tanggal',
            'log_pelanggaran_siswa'         => 'log',
        ])
            ->join('user', 'user.id_user', '=', 'pelanggaran_siswa.id_user')
            ->join('profile_siswa', function ($j) use ($request) {
                $j->on('profile_siswa.id_user', '=', 'user.id_user')
                    ->where('nis', $request->nis);
            })
            ->get();

        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'    => 'id',
            'nis'        => 'nis',
            'nama_siswa' => 'nama',
        ])
            ->where('nis', $request->nis)
            ->first();
        $point = 0;
        foreach ($pelanggaran as $key => $value) {
            $point += $value->point;
        }

        if ($profile == null) {
            return F::respon('Siswa tidak ditemukan', 411);
        }

        $profile->point_terkumpul = $point;
        return F::respon([
            'pelanggaran' => $pelanggaran,
            'siswa'       => $profile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'siswa'       => 'required',
            'pelanggaran' => 'required',
            'sanksi'      => 'required',
            'point'       => 'required',
            'surat'       => 'required',
        ], [
            'siswa.required'       => 'Siswa harus ditentukan terlebih dahulu',
            'pelanggaran.required' => 'Pelanggaran harus dipilih',
            'sanksi.required'      => 'Sanki tidak ditemukan',
            'point.required'       => 'Point tidak ditemukan',
            'surat.required'       => 'Jenis surat harus dipilih',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        if ($request->surat == 'none') {
            $request->merge(['surat' => null]);
        }

        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'          => 'id',
            'nis'              => 'nis',
            'nama_siswa'       => 'nama',
            'nama_jurusan'     => 'jurusan',
            'nama_rombel'      => 'kelas',
            'nama_level_kelas' => 'level_kelas',
        ])
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'profile_siswa.id_jurusan')
            ->join('rombel', 'rombel.id_rombel', '=', 'profile_siswa.id_rombel')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->where('profile_siswa.id_user', $request->siswa)
            ->first();

        DB::table('pelanggaran_siswa')
            ->insert([
                'id_user'                       => $request->siswa,
                'view_pelanggaran_siswa'        => $request->pelanggaran,
                'view_sanksi_pelanggaran_siswa' => $request->sanksi,
                'view_surat_pelanggaran_siswa'  => $request->surat,
                'apoint_pelanggaran_siswa'      => $request->point,
                'tgl_pelanggaran_siswa'         => $request->tanggal,
                'log_pelanggaran_siswa'         => json_encode($profile),
            ]);

        if ($request->juz != 'false') {
            if (intval($request->juz) > 30) {
                $request->juz = 1;
            }

            DB::table('pengaturan')
                ->where('label_pengaturan', 'juz_quran_pelanggaran')
                ->update([
                    'value_pengaturan' => $request->juz,
                ]);

        }

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('pelanggaran_siswa')
            ->where('id_pelanggaran_siswa', $request->id)
            ->delete();
        return self::index($request);
    }
}
