<?php

namespace App\Http\Controllers\Bk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Fdb as F;
use Validator;

class SuratC extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $data = F::filter(DB::table('surat_bk'), [
            'id_surat_bk'    => 'id',
            'surat_surat_bk' => 'nama',
            'isi_surat_bk'   => 'isi',
            'tipe_surat_bk'  => 'jenis',
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('surat_surat_bk', 'like', '%' . $cari . '%')
                    ->orWhere('isi_surat_bk', 'like', '%' . $cari . '%')
                    ->orWhere('tipe_surat_bk', 'like', '%' . $cari . '%');
            });
            if($request->paging == 'off')
                $data = $data->get();
            else
                $data = $data->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'jenis' => 'required',
            'isi' => 'required',
        ], [
            'nama.required' => 'Nama susat harus diisi',
            'jenis.required' => 'Jenis surat belum ditentukan',
            'isi.required'  => 'Isi surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('surat_bk')
            ->insert([
                'surat_surat_bk' => $request->nama,
                'tipe_surat_bk'        => $request->jenis,
                'isi_surat_bk'        => $request->isi,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = F::filter(DB::table('surat_bk'), [
            'id_surat_bk'    => 'id',
            'surat_surat_bk' => 'nama',
            'isi_surat_bk'   => 'isi',
            'tipe_surat_bk'  => 'jenis',
        ])
            ->where('id_surat_bk', $id)
            ->first();
        return F::respon($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'jenis' => 'required',
            'isi' => 'required',
        ], [
            'nama.required' => 'Nama susat harus diisi',
            'jenis.required' => 'Jenis surat belum ditentukan',
            'isi.required'  => 'Isi surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('surat_bk')
            ->where('id_surat_bk',$request->id)
            ->update([
                'surat_surat_bk' => $request->nama,
                'tipe_surat_bk'        => $request->jenis,
                'isi_surat_bk'        => $request->isi,
            ]);

        return self::show($request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r)
    {
        DB::table('surat_bk')
            ->where('id_surat_bk', $r->id)
            ->delete();
        return self::index($r);
    }
}
