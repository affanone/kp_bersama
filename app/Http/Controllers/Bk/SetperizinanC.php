<?php

namespace App\Http\Controllers\Bk;

use App\Helpers\Fdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class SetperizinanC extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cari(Request $request){
        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'    => 'id',
            'nis'        => 'nis',
            'nama_siswa' => 'nama',
        ])
            ->where('nis','like','%'.$request->cari.'%')
            ->orWhere('nama_siswa','like','%'.$request->cari.'%')
            ->limit(30)
            ->get();
        if(count($profile)>1){
            //lebih
            return F::respon([
                'mode' => 'multi',
                'data' => $profile
            ]);
        }else if(count($profile)==1){
            //satu
            return F::respon([
                'mode' => 'one',
                'nis' => $profile[0]->nis
            ]);
        }else{
            //tak ditemukan
            return F::respon('Siswa tidak ditemukan', 411);
        }
    }
    public function index(Request $request)
    {
        $perizinan = F::filter(DB::table('perizinan_siswa'), [
            'id_perizinan_siswa'             => 'id',
            'keterangan_perizinan_siswa'     => 'keterangan',
            'view_surat_perizinan_siswa'     => 'surat',
            'kembali_perizinan_siswa'        => 'janji',
            'status_kembali_perizinan_siswa' => 'kembali',
            'tgl_perizinan_siswa'            => 'tanggal',
        ])
            ->join('user', 'user.id_user', '=', 'perizinan_siswa.id_user')
            ->join('profile_siswa', function ($j) use ($request) {
                $j->on('profile_siswa.id_user', '=', 'user.id_user')
                    ->where('nis', $request->nis);
            })
            ->get();

        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'    => 'id',
            'nis'        => 'nis',
            'nama_siswa' => 'nama',
        ])
            ->where('nis', $request->nis)
            ->first();

        if ($profile == null) {
            return F::respon('Siswa tidak ditemukan', 411);
        }

        return F::respon([
            'perizinan' => $perizinan,
            'siswa'     => $profile,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'siswa'          => 'required',
            'surat'          => 'required',
            'keterangan'     => 'required',
            'janji'          => 'required',
        ];
        if ($request->keterangan == 'true') {
            $rule['keterangan_isi'] = 'required';
        }else{
            $request->merge(['keterangan_isi' => null]);
        }


        $v = Validator::make($request->all(), $rule, [
            'siswa.required'          => 'Siswa harus ditentukan terlebih dahulu',
            'keterangan.required'     => 'Set keterangan harus dipilih',
            'keterangan_isi.required' => 'Isi keterangan tidak boleh kosong',
            'janji.required'          => 'Tentukan perjanjian akan kembali ke sekolah',
            'surat.required'          => 'Jenis surat harus dipilih',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        if ($request->surat == 'none') {
            $request->merge(['surat' => null]);
        }

        $profile = F::filter(DB::table('profile_siswa'), [
            'id_user'          => 'id',
            'nis'              => 'nis',
            'nama_siswa'       => 'nama',
            'nama_jurusan'     => 'jurusan',
            'nama_rombel'      => 'kelas',
            'nama_level_kelas' => 'level_kelas',
        ])
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'profile_siswa.id_jurusan')
            ->join('rombel', 'rombel.id_rombel', '=', 'profile_siswa.id_rombel')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->where('profile_siswa.id_user', $request->siswa)
            ->first();

        DB::table('perizinan_siswa')
            ->insert([
                'id_user'                        => $request->siswa,
                'keterangan_perizinan_siswa'     => $request->keterangan_isi,
                'view_surat_perizinan_siswa'     => $request->surat,
                'kembali_perizinan_siswa'        => $request->janji,
                'tgl_perizinan_siswa'            => date('Y-m-d H:i:s'),
                'status_kembali_perizinan_siswa' => 0,
                'log_perizinan_siswa'            => json_encode($profile),
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('perizinan_siswa')
            ->where('id_perizinan_siswa',$request->id)
            ->where('id_user',$request->siswa)
            ->update([
                'status_kembali_perizinan_siswa' => $request->kembali
            ]);
        return F::respon(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('perizinan_siswa')
            ->where('id_perizinan_siswa',$request->id)
            ->where('id_user',$request->siswa)
            ->delete();
        return F::respon(true);
    }
}
