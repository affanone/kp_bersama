<?php

namespace App\Http\Controllers\Bk;

use App\Helpers\Fdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class PelanggaranC extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function rekap(Request $request){

        $tanggal_awal = $request->tanggal_awal ?? null;
        $tanggal_akhir = $request->tanggal_akhir ?? null;
        $paging = $request->paging ?? null;
        $jenis = $request->jenis;

        $data = DB::table('pelanggaran_siswa')
            ->when($jenis,function($q)use($request){
                if($request->jenis=='siswa'){
                    return $q->join('profile_siswa',function($j)use($request){
                                $j->on('profile_siswa.id_user','=','pelanggaran_siswa.id_user');
                                $j->where('profile_siswa.nis',$request->siswa);
                            });
                }else{
                    return $q->where('pelanggaran_siswa.view_pelanggaran_siswa',$request->pelanggaran);
                }
            })
            ->whereBetween(DB::raw('DATE(pelanggaran_siswa.tgl_pelanggaran_siswa)') , [date($tanggal_awal) , date($tanggal_akhir)])
            ->orderBy('pelanggaran_siswa.tgl_pelanggaran_siswa','desc');

        if($paging=='off')
            $data = $data->get();
        else
            $data = $data->paginate(10);

        $data_log = [];
        foreach ($data as $key => $value) {
            $row = json_decode($value->log_pelanggaran_siswa);
            $row->pelanggaran = $value->view_pelanggaran_siswa;
            $row->sanksi = $value->view_sanksi_pelanggaran_siswa;
            $row->tanggal = $value->tgl_pelanggaran_siswa;
            $row->point = $value->apoint_pelanggaran_siswa;
            array_push($data_log,$row);
        }
        
        if($paging=='off')
            return F::respon($data_log);

        $replace = json_encode($data);
        $replace = json_decode($replace);
        $replace->data = $data_log;
        return F::respon($replace);
    }

    public function view(){
        $data = DB::table('pelanggaran_siswa')
            ->select(DB::raw('view_pelanggaran_siswa as pelanggaran'))
            ->groupBy('view_pelanggaran_siswa')
            ->get();
        return F::respon($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $data = F::filter(DB::table('pelanggaran'), [
            'id_pelanggaran'          => 'id',
            'pelanggaran_pelanggaran' => 'nama',
            'poin_pelanggaran'        => 'poin',
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('pelanggaran_pelanggaran', 'like', '%' . $cari . '%');
            });
            if($request->paging == 'off')
                $data = $data->get();
            else
                $data = $data->paginate(10);

        return F::respon($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'poin' => 'required|numeric|min:1',
        ], [
            'nama.required' => 'Nama pelanggaran harus diisi',
            'poin.required' => 'Nilai poin pada pelanggaran harus ditentukan',
            'poin.numeric'  => 'Nilai poin harus angka',
            'poin.min'      => 'Nilai poin minimal bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('pelanggaran')
            ->insert([
                'pelanggaran_pelanggaran' => $request->nama,
                'poin_pelanggaran'        => $request->poin,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = F::filter(DB::table('pelanggaran'), [
            'id_pelanggaran'          => 'id',
            'pelanggaran_pelanggaran' => 'nama',
            'poin_pelanggaran'        => 'poin',
        ])
            ->where('id_pelanggaran', $id)
            ->first();
        return F::respon($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'poin' => 'required|numeric|min:1',
        ], [
            'nama.required' => 'Nama pelanggaran harus diisi',
            'poin.required' => 'Nilai poin pada pelanggaran harus ditentukan',
            'poin.numeric'  => 'Nilai poin harus angka',
            'poin.min'      => 'Nilai poin minimal bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('pelanggaran')
            ->where('id_pelanggaran', $request->id)
            ->update([
                'pelanggaran_pelanggaran' => $request->nama,
                'poin_pelanggaran'        => $request->poin,
            ]);

        return self::show($request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r)
    {
        DB::table('pelanggaran')
            ->where('id_pelanggaran', $r->id)
            ->delete();
        return self::index($r);
    }
}
