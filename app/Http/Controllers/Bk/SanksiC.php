<?php

namespace App\Http\Controllers\Bk;

use App\Helpers\Fdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class SanksiC extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $data = F::filter(DB::table('sanksi'), [
            'id_sanksi'        => 'id',
            'sanksi_sanksi'    => 'nama',
            'max_point_sanksi' => 'max',
            'min_point_sanksi' => 'min',
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('sanksi_sanksi', 'like', '%' . $cari . '%');
            })
            ->orderBy('min_point_sanksi');
            if($request->paging == 'off')
                $data = $data->get();
            else
                $data = $data->paginate(10);
        return F::respon($data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'max'  => 'required|numeric|min:1',
            'min'  => 'required|numeric|min:1',
        ], [
            'nama.required' => 'Nama pelanggaran harus diisi',
            'max.required'  => 'Nilai maksimal poin pada pelanggaran harus ditentukan',
            'max.numeric'   => 'Nilai maksimal poin harus angka',
            'max.min'       => 'Nilai maksimal poin setidaknya bernilai 1',
            'min.required'  => 'Nilai minimal poin pada pelanggaran harus ditentukan',
            'min.numeric'   => 'Nilai minimal poin harus angka',
            'min.min'       => 'Nilai minimal poin setidaknya bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        if ($request->max <= $request->min) {
            $v->errors()->add('umum', 'Maksimal point tidak boleh lebih kecil atau sama dengan nilai minimal point');
            return F::respon($v->errors(), 411);
        }

        $tumpukan = DB::table('sanksi')
            ->where(function ($q) use ($request) {
                $q->where('max_point_sanksi', '>=', $request->min)
                    ->where('min_point_sanksi', '<=', $request->min);
            })

            ->orWhere(function ($q) use ($request) {
                $q->where('max_point_sanksi', '>=', $request->max)
                    ->where('min_point_sanksi', '<=', $request->max);
            })

            ->orWhere(function ($q) use ($request) {
                $q->where('max_point_sanksi', $request->max)
                    ->where('min_point_sanksi', $request->min);
            })
            ->count();

        if ($tumpukan > 0) {
            $v->errors()->add('umum', 'Range point ' . $request->min . ' - ' . $request->max . ' bertumbukan, silahkan periksa terlebih dahulu');
            return F::respon($v->errors(), 411);
        }

        DB::table('sanksi')
            ->insert([
                'sanksi_sanksi'    => $request->nama,
                'max_point_sanksi' => $request->max,
                'min_point_sanksi' => $request->min,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = F::filter(DB::table('sanksi'), [
            'id_sanksi'        => 'id',
            'sanksi_sanksi'    => 'nama',
            'max_point_sanksi' => 'max',
            'min_point_sanksi' => 'min',
        ])
            ->where('id_sanksi', $id)
            ->orderBy('min_point_sanksi')
            ->first();
        return F::respon($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
            'max'  => 'required|numeric|min:1',
            'min'  => 'required|numeric|min:1',
        ], [
            'nama.required' => 'Nama pelanggaran harus diisi',
            'max.required'  => 'Nilai maksimal poin pada pelanggaran harus ditentukan',
            'max.numeric'   => 'Nilai maksimal poin harus angka',
            'max.min'       => 'Nilai maksimal poin setidaknya bernilai 1',
            'min.required'  => 'Nilai minimal poin pada pelanggaran harus ditentukan',
            'min.numeric'   => 'Nilai minimal poin harus angka',
            'min.min'       => 'Nilai minimal poin setidaknya bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        if ($request->max <= $request->min) {
            $v->errors()->add('umum', 'Maksimal point tidak boleh lebih kecil atau sama dengan nilai minimal point');
            return F::respon($v->errors(), 411);
        }

        $tumpukan = DB::table('sanksi')
            ->where(function ($q) use ($request) {
                $q->where(function ($q) use ($request) {
                    $q->where('max_point_sanksi', '>=', $request->min)
                        ->where('min_point_sanksi', '<=', $request->min);
                })

                    ->orWhere(function ($q) use ($request) {
                        $q->where('max_point_sanksi', '>=', $request->max)
                            ->where('min_point_sanksi', '<=', $request->max);
                    })

                    ->orWhere(function ($q) use ($request) {
                        $q->where('max_point_sanksi', $request->max)
                            ->where('min_point_sanksi', $request->min);
                    });
            })

            ->where('id_sanksi', '!=', $request->id)
            ->count();

        if ($tumpukan > 0) {
            $v->errors()->add('umum', 'Range point ' . $request->min . ' - ' . $request->max . ' bertumbukan, silahkan periksa terlebih dahulu');
            return F::respon($v->errors(), 411);
        }

        DB::table('sanksi')
            ->where('id_sanksi', $request->id)
            ->update([
                'sanksi_sanksi'    => $request->nama,
                'max_point_sanksi' => $request->max,
                'min_point_sanksi' => $request->min,
            ]);

        return self::show($request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $r)
    {
        DB::table('sanksi')
            ->where('id_sanksi', $r->id)
            ->delete();
        return self::index($r);
    }
}
