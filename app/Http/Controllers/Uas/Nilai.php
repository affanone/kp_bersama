<?php

namespace App\Http\Controllers\Uas;

use App\Helpers\Adb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Nilai extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function log_ujian(Request $request){
        $data = F::filter(DB::table('nilai_uas'),[
            'jadwal_uas.nama_jadwal_uas' => 'ujian',
            'nilai_uas.total_soal_nilai_uas' => 'total_soal',
            'nilai_uas.total_dijawab_nilai_uas' => 'total_dijawab',
            'nilai_uas.total_benar_nilai_uas' => 'total_benar',
            'nilai_uas.tanggal_mulai_ujian' => 'mulai',
            'nilai_uas.tanggal_selesai_ujian' => 'selesai',
            'nilai_uas.nilai_huruf_nilai_uas' => 'nilai_huruf',
            'nilai_uas.nilai_angka_nilai_uas'   => 'nilai_angka',
            'jadwal_uas.id_jadwal_uas' => 'id',
            'mapel.nama_mapel' => 'mapel',
            'profile_gukar.nama_gukar' => 'pengawas',
            'jurusan.nama_jurusan' => 'jurusan',
            'level_kelas.nama_level_kelas' =>'kelas',
            'paket.nama_paket' => 'paket',
            'rombel.nama_rombel' => 'rombel',
            'jadwal_uas.durasi_jadwal_uas' => 'durasi',
            'jadwal_uas.status_jadwal_uas' => 'status',
            'a.tanggal_jadwal_susulan_uas' => 'tanggal_susulan',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'tahun_ajaran.nama_tahun_ajaran' => 'tahun_ajaran'
        ])
            ->join('jadwal_uas',function($j)use($request){
                $cari = $request->cari ?? null;
                $j->on('jadwal_uas.id_jadwal_uas','=','nilai_uas.id_jadwal_uas');
                $j->when($cari,function($q,$cari){
                    $q->where('nama_jadwal_uas','like','%'.$cari.'%');
                });
            })
            ->leftJoin(DB::raw('(select a.id_jadwal_uas,a.id_user,a.tanggal_jadwal_susulan_uas from jadwal_susulan_uas a where a.id_user = '.$request->siswa.')a'),'a.id_jadwal_uas','=','jadwal_uas.id_jadwal_uas')
            ->join('mapel','mapel.id_mapel','=','jadwal_uas.id_mapel')
            ->join('user','user.id_user','=','jadwal_uas.id_user')
            ->join('profile_gukar','profile_gukar.id_user','=','user.id_user')
            ->join('rombel',function($j)use($request){
                $j->on('rombel.id_rombel','=','jadwal_uas.id_rombel');
                $kelas = $request->kelas ?? null;
                $j->when($kelas,function($q,$kelas){
                    $q->where('rombel.id_level_kelas',$kelas);
                });
            })
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('paket','paket.id_paket','=','jadwal_uas.id_paket')
            ->join('tahun_ajaran',function($j){
                $j->on('tahun_ajaran.id_tahun_ajaran','=','jadwal_uas.id_tahun_ajaran');
                $j->where('tahun_ajaran.status',0);
            })
            ->where('nilai_uas.id_user',$request->siswa)
            ->orderBy('tahun_ajaran.id_tahun_ajaran')
            ->orderBy('jadwal_uas.tanggal_jadwal_uas','desc')
            ->get();
            //->paginate(10);
        return F::respon($data);
    }

    public function susulan_terapkan(Request $request){
        $data = json_decode($request->siswa);
        
        foreach ($data as $key => $value) {
            $cek = DB::table('jadwal_susulan_uas')
                ->where('id_user',$value->id)
                ->where('id_jadwal_uas',$request->jadwal)
                ->count();
            if($cek>0){
                DB::table('jadwal_susulan_uas')
                    ->where('id_user',$value->id)
                    ->where('id_jadwal_uas',$request->jadwal)
                    ->update([
                        'tanggal_jadwal_susulan_uas' => $request->tanggal,
                        'status_jadwal_susulan_uas' => 0
                    ]);
            }else{
                DB::table('jadwal_susulan_uas')
                    ->where('id_user',$value->id)
                    ->where('id_jadwal_uas',$request->jadwal)
                    ->insert([
                        'id_user' => $value->id,
                        'tanggal_jadwal_susulan_uas' => $request->tanggal,
                        'id_jadwal_uas' => $request->jadwal,
                        'status_jadwal_susulan_uas' => 0
                    ]);
            }

        }
        return self::susulan($request);
    }

    public function susulan(Request $request){
        $nilai = F::filter(DB::table('nilai_uas'), [
            'profile_siswa.id_user' => 'id',
            'profile_siswa.nis'    => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'jadwal_susulan_uas.tanggal_jadwal_susulan_uas' => 'tanggal',
        ])
            ->addSelect(DB::raw('false as tandai'))
            ->whereNull('nilai_uas.tanggal_mulai_ujian')
            ->where('nilai_uas.id_jadwal_uas', $request->jadwal)
            ->leftJoin('jadwal_susulan_uas', function($j){
                $j->on('jadwal_susulan_uas.id_jadwal_uas', '=', 'nilai_uas.id_jadwal_uas');
                $j->whereRaw('jadwal_susulan_uas.id_user = nilai_uas.id_user');
            })
            ->join('profile_siswa', 'profile_siswa.id_user', '=', 'nilai_uas.id_user')
            ->orderBy('profile_siswa.nama_siswa')
            ->get();

        return F::respon($nilai);
    }

    public function susulan_delete(Request $request){
        DB::table('jadwal_susulan_uas')
                ->where('id_jadwal_uas',$request->jadwal)
                ->where('id_user',$request->siswa)
                ->delete();
        return self::susulan($request);
    }

    public function hasil_list(Request $request)
    {
        $info = F::filter(DB::table('jadwal_uas'), [
            'jadwal_uas.id_jadwal_uas'      => 'id',
            'mapel.nama_mapel'              => 'mapel',
            'profile_gukar.nama_gukar'      => 'pengawas',
            'jurusan.nama_jurusan'          => 'jurusan',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jadwal_uas.nama_jadwal_uas'    => 'nama',
            'paket.nama_paket'              => 'paket',
            'rombel.nama_rombel'            => 'rombel',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas'  => 'durasi',
            'jadwal_uas.status_jadwal_uas'  => 'status',

        ])
            ->where('jadwal_uas.id_jadwal_uas', $request->jadwal)
            ->join('mapel', 'mapel.id_mapel', '=', 'jadwal_uas.id_mapel')
            ->join('user', 'user.id_user', '=', 'jadwal_uas.id_user')
            ->join('profile_gukar', 'profile_gukar.id_user', '=', 'user.id_user')
            ->join('rombel', 'rombel.id_rombel', '=', 'jadwal_uas.id_rombel')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->join('paket', 'paket.id_paket', '=', 'jadwal_uas.id_paket')
            ->first();

        $nilai = F::filter(DB::table('profile_siswa'), [
            'nilai_uas.id_nilai_uas'   => 'id',
            'profile_siswa.id_user'    => 'user',
            'profile_siswa.nis'        => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'nilai_uas.total_soal_nilai_uas' => 'total_soal',
            'nilai_uas.total_dijawab_nilai_uas' => 'total_dijawab',
            'nilai_uas.total_benar_nilai_uas' => 'total_benar',
            'nilai_uas.tanggal_mulai_ujian' => 'mulai',
            'nilai_uas.tanggal_selesai_ujian' => 'selesai',
            'nilai_uas.nilai_huruf_nilai_uas' => 'nilai_huruf',
            'nilai_uas.nilai_angka_nilai_uas'   => 'nilai_angka',
        ])
            ->join('nilai_uas', function ($j) use ($request) {
                $j->on('nilai_uas.id_user', '=', 'profile_siswa.id_user')
                    ->where('nilai_uas.id_jadwal_uas',$request->jadwal);
            })
            ->get();
        return F::respon([
            'nilai' => $nilai,
            'info'  => $info,
        ]);
    }

    public function hasil(Request $request)
    {
        $info = F::filter(DB::table('jadwal_uas'), [
            'id_jadwal_uas'                 => 'id',
            'mapel.nama_mapel'              => 'mapel',
            'profile_gukar.nama_gukar'      => 'pengawas',
            'jurusan.nama_jurusan'          => 'jurusan',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jadwal_uas.nama_jadwal_uas'    => 'nama',
            'paket.nama_paket'              => 'paket',
            'rombel.nama_rombel'            => 'rombel',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas'  => 'durasi',
            'jadwal_uas.status_jadwal_uas'  => 'status',

        ])
            ->where('jadwal_uas.id_jadwal_uas', $request->jadwal)
            ->join('mapel', 'mapel.id_mapel', '=', 'jadwal_uas.id_mapel')
            ->join('user', 'user.id_user', '=', 'jadwal_uas.id_user')
            ->join('profile_gukar', 'profile_gukar.id_user', '=', 'user.id_user')
            ->join('rombel', 'rombel.id_rombel', '=', 'jadwal_uas.id_rombel')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->join('paket', 'paket.id_paket', '=', 'jadwal_uas.id_paket')
            ->first();

        $nilai = F::filter(DB::table('nilai_uas'), [
            'total_soal_nilai_uas'    => 'jumlah_soal',
            'total_dijawab_nilai_uas' => 'jumlah_dijawab',
            'total_benar_nilai_uas'   => 'jumlah_benar',
            'nilai_angka_nilai_uas'   => 'nilai',
            'nilai_huruf_nilai_uas'   => 'huruf',
            'tanggal_selesai_ujian'   => 'tanggal_selesai'
        ])
            ->where('id_user', $request->siswa)
            ->where('id_jadwal_uas', $request->jadwal)
            ->first();

        if($nilai->tanggal_selesai==null){
            DB::table('nilai_uas')
                ->where('id_user', $request->siswa)
                ->where('id_jadwal_uas', $request->jadwal)
                ->update(['tanggal_selesai_ujian'=>date('Y-m-d H:i:s')]);
        }

        if ($nilai == null) {
            return F::respon([
                'jumlah_soal'    => null,
                'jumlah_dijawab' => null,
                'jumlah_benar'   => null,
                'nilai'          => null,
                'huruf'          => null,
                'info_ujian'     => $info,
            ]);
        } else {
            $nilai->info_ujian = $info;
            return F::respon($nilai);
        }
    }

    public function index(Request $request)
    {
        $soal = DB::table('soal_uas')
            ->whereRaw('id_paket in (select id_paket from jadwal_uas where id_jadwal_uas = ?) ', [$request->jadwal])
            ->count();
        $info = F::filter(DB::table('jadwal_uas'), [
            'mapel.nama_mapel'              => 'mapel',
            'profile_gukar.nama_gukar'      => 'pengawas',
            'jurusan.nama_jurusan'          => 'jurusan',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jadwal_uas.nama_jadwal_uas'    => 'nama',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas'  => 'durasi',

        ])
            ->where('jadwal_uas.id_jadwal_uas', $request->jadwal)
            ->join('mapel', 'mapel.id_mapel', '=', 'jadwal_uas.id_mapel')
            ->join('user', 'user.id_user', '=', 'jadwal_uas.id_user')
            ->join('profile_gukar', 'profile_gukar.id_user', '=', 'user.id_user')
            ->join('rombel', 'rombel.id_rombel', '=', 'jadwal_uas.id_rombel')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->join('paket', 'paket.id_paket', '=', 'jadwal_uas.id_paket')
            ->first();

        $siswa = F::filter(DB::table('profile_siswa'), [
            'nilai_uas.id_nilai_uas'   => 'id',
            'profile_siswa.id_user'    => 'user',
            'profile_siswa.nis'        => 'nis',
            'profile_siswa.nama_siswa' => 'nama',
            'nilai_uas.total_soal_nilai_uas' => 'total_soal',
            'nilai_uas.total_dijawab_nilai_uas' => 'total_dijawab',
            'nilai_uas.total_benar_nilai_uas' => 'total_benar',
            'nilai_uas.tanggal_mulai_ujian' => 'mulai',
            'nilai_uas.tanggal_selesai_ujian' => 'selesai',
            'nilai_uas.nilai_huruf_nilai_uas' => 'nilai_huruf',
            'nilai_uas.nilai_angka_nilai_uas'   => 'nilai_angka',
        ])
            ->join('nilai_uas', function ($j) use ($request) {
                $j->on('nilai_uas.id_user', '=', 'profile_siswa.id_user')
                    ->where('nilai_uas.id_jadwal_uas',$request->jadwal);
            })
            // ->leftJoin('jawaban_uas_siswa', function ($j) use ($request) {
            //     $j->on('jawaban_uas_siswa.id_user', '=', 'profile_siswa.id_user')
            //         ->where('jawaban_uas_siswa.id_jadwal_uas', $request->jadwal);
            // })
            // ->addSelect(DB::raw('count(jawaban_uas_siswa.id_jadwal_uas) as dijawab'))
            // //->groupBy('jawaban_uas_siswa.id_jadwal_uas')
            // ->leftJoin('jawaban_uas', function ($j) use ($request) {
            //     $j->on('jawaban_uas.id_jawaban_uas', '=', 'jawaban_uas_siswa.id_jawaban_uas')
            //         ->where('jawaban_uas.benar_jawaban_uas', 1);
            // })
            // ->addSelect(DB::raw('count(jawaban_uas.benar_jawaban_uas) as benar'))
            // ->addSelect(DB::raw('CAST(' . $soal . ' AS int) as soal'))
            ->get();
        return F::respon([
            'info'  => $info,
            'siswa' => $siswa,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
