<?php

namespace App\Http\Controllers\Uas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Adb as F;
use Validator;

class Jadwal extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function siswa(Request $request)
    {
        $cari = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $data = F::filter(DB::table('jadwal_uas'),[
            'jadwal_uas.id_jadwal_uas' => 'id',
            'mapel.nama_mapel' => 'mapel',
            'profile_gukar.nama_gukar' => 'pengawas',
            'jurusan.nama_jurusan' => 'jurusan',
            'level_kelas.nama_level_kelas' =>'kelas',
            'jadwal_uas.nama_jadwal_uas' => 'nama',
            'paket.nama_paket' => 'paket',
            'paket.id_paket' => 'idpaket',
            'rombel.nama_rombel' => 'rombel',
            'jadwal_uas.durasi_jadwal_uas' => 'durasi',
            'jadwal_uas.status_jadwal_uas' => 'status',
            'nilai_uas.tanggal_selesai_ujian' => 'tanggal_selesai'
        ])
            ->addSelect(DB::raw('IFNULL(a.tanggal_jadwal_susulan_uas,jadwal_uas.tanggal_jadwal_uas) as tanggal'))
            ->addSelect(DB::raw('CASE WHEN a.tanggal_jadwal_susulan_uas IS NOT NULL THEN 1 ELSE 0 END AS tipe_ujian'))
            ->addSelect(DB::raw('null as statusChange'))
            ->where('jadwal_uas.status_jadwal_uas',1)
            ->whereRaw('jadwal_uas.id_tahun_ajaran in (select id_tahun_ajaran from tahun_ajaran where status = ?)',[1])
            ->join('mapel','mapel.id_mapel','=','jadwal_uas.id_mapel')
            ->join('user','user.id_user','=','jadwal_uas.id_user')
            ->join('profile_gukar','profile_gukar.id_user','=','user.id_user')
            ->join('rombel','rombel.id_rombel','=','jadwal_uas.id_rombel')
            ->join('profile_siswa',function($j) use ($request) {
                $j->on('profile_siswa.id_rombel','=','rombel.id_rombel')
                    ->where('profile_siswa.id_user',$request->siswa);
            })
            ->join('nilai_uas',function($j) use ($request) {
                $j->on('nilai_uas.id_jadwal_uas','=','jadwal_uas.id_jadwal_uas')
                    ->where('nilai_uas.id_user',$request->siswa);
            })
            ->leftJoin(DB::raw('(select a.id_jadwal_uas,a.id_user,a.tanggal_jadwal_susulan_uas from jadwal_susulan_uas a where a.id_user = '.$request->siswa.')a'),'a.id_jadwal_uas','=','jadwal_uas.id_jadwal_uas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('paket','paket.id_paket','=','jadwal_uas.id_paket')
            ->get();

        return F::respon([
            'jadwal' => $data,
            'datetime' => date('Y-m-d H:i:s')
        ]);
    }


    public function guru(Request $request)
    {
        $cari = $request->cari ?? null;
        $ajaran = $request->ajaran ?? null;
        $kelas = $request->kelas ?? null;
        $mapel = $request->mapel ?? null;
        $data = F::filter(DB::table('jadwal_uas'),[
            'jadwal_uas.id_jadwal_uas' => 'id',
            'mapel.nama_mapel' => 'mapel',
            'mapel.id_mapel' => 'kodemapel',
            'profile_gukar.nama_gukar' => 'pengawas',
            'profile_gukar.id_user' => 'kode_pengawas',
            'jurusan.nama_jurusan' => 'jurusan',
            'level_kelas.nama_level_kelas' =>'kelas',
            'jadwal_uas.nama_jadwal_uas' => 'nama',
            'paket.nama_paket' => 'paket',
            'paket.id_paket' => 'idpaket',
            'rombel.nama_rombel' => 'rombel',
            'rombel.id_rombel' => 'koderombel',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas' => 'durasi',
            'jadwal_uas.status_jadwal_uas' => 'status',
            'tahun_ajaran.nama_tahun_ajaran' => 'semester',
            'tahun_ajaran.status' => 'status_semester'
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('jadwal_uas.nama_jadwal_uas', 'like', '%' . $cari . '%');
            })
            ->when($ajaran, function ($q) use ($ajaran) {
                return $q->where('jadwal_uas.id_tahun_ajaran',$ajaran);
            })
            ->when($kelas, function ($q) use ($kelas) {
                return $q->where('jadwal_uas.id_rombel',$kelas);
            })
            ->when($mapel, function ($q) use ($mapel) {
                return $q->where('jadwal_uas.id_mapel',$mapel);
            })
            ->where('jadwal_uas.id_user',$request->guru)
            ->where('jadwal_uas.status_jadwal_uas',1)
            ->join('mapel','mapel.id_mapel','=','jadwal_uas.id_mapel')
            ->join('user','user.id_user','=','jadwal_uas.id_user')
            ->join('profile_gukar','profile_gukar.id_user','=','user.id_user')
            ->join('rombel','rombel.id_rombel','=','jadwal_uas.id_rombel')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('paket','paket.id_paket','=','jadwal_uas.id_paket')
            ->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran','=','jadwal_uas.id_tahun_ajaran')
            ->leftJoin(DB::raw(' (select a.id_jadwal_uas, count(a.id_jadwal_uas) as total, a.tanggal_mulai_ujian from nilai_uas a where a.tanggal_mulai_ujian is not null)a'),function($j){
                $j->on('a.id_jadwal_uas','=','jadwal_uas.id_jadwal_uas');
            })
            ->addSelect(DB::raw(' a.total as digunakan'))
            ->orderBy('status','desc')
            ->orderBy('status_semester','desc')
            ->orderBy('tanggal','desc');
            if($request->paging=='off')
                $data = $data->get();
            else
                $data = $data->paginate(10);

        return F::respon($data);
    }

    public function index(Request $request)
    {   
        $cari = $request->cari_f ?? null;
        $kelas = $request->kelas_f ?? null;
        $ajaran = $request->ajaran_f ?? null;
        $data = F::filter(DB::table('jadwal_uas'),[
            'jadwal_uas.id_jadwal_uas' => 'id',
            'mapel.nama_mapel' => 'mapel',
            'mapel.id_mapel' => 'kodemapel',
            'profile_gukar.nama_gukar' => 'pengawas',
            'profile_gukar.id_user' => 'kode_pengawas',
            'jurusan.nama_jurusan' => 'jurusan',
            'level_kelas.nama_level_kelas' =>'kelas',
            'jadwal_uas.nama_jadwal_uas' => 'nama',
            'paket.nama_paket' => 'paket',
            'paket.id_paket' => 'idpaket',
            'rombel.nama_rombel' => 'rombel',
            'rombel.id_rombel' => 'koderombel',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas' => 'durasi',
            'jadwal_uas.status_jadwal_uas' => 'status',
            'tahun_ajaran.nama_tahun_ajaran' => 'semester',
            'tahun_ajaran.status' => 'status_semester'
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('jadwal_uas.nama_jadwal_uas', 'like', '%' . $cari . '%');
            })
            ->when($ajaran, function ($q) use ($ajaran) {
                return $q->where('jadwal_uas.id_tahun_ajaran',$ajaran);
            })
            ->when($kelas, function ($q) use ($kelas) {
                return $q->where('jadwal_uas.id_rombel',$kelas);
            })
            ->join('mapel','mapel.id_mapel','=','jadwal_uas.id_mapel')
            ->join('user','user.id_user','=','jadwal_uas.id_user')
            ->join('profile_gukar','profile_gukar.id_user','=','user.id_user')
            ->join('rombel','rombel.id_rombel','=','jadwal_uas.id_rombel')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('paket','paket.id_paket','=','jadwal_uas.id_paket')
            ->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran','=','jadwal_uas.id_tahun_ajaran')
            ->leftJoin(DB::raw(' (select a.id_jadwal_uas, count(a.id_jadwal_uas) as total, a.tanggal_mulai_ujian from nilai_uas a where a.tanggal_mulai_ujian is not null GROUP BY a.id_jadwal_uas)a'),function($j){
                $j->on('a.id_jadwal_uas','=','jadwal_uas.id_jadwal_uas');
            })
            ->addSelect(DB::raw(' a.total as digunakan'))
            ->orderBy('status','desc')
            ->orderBy('status_semester','desc')
            ->orderBy('tanggal','desc')
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(),[
            'nama' => 'required',
            'paket' => 'required',
            'kelas' => 'required',
            'mapel' => 'required',
            'pengawas' => 'required',
            'tanggal' => 'required|date_format:Y-m-d H:i:s',
            'durasi' => 'required|date_format:H:i:s'
        ],[
            'nama.required' => 'Nama ujian harus diisi',
            'paket.required' => 'Paket soal harus ditentukan',
            'kelas.required' => 'Kelas harus ditentukan',
            'mapel.required' => 'Mata pelajaran harus ditentukan',
            'pengawas.required' => 'Guru pengawas harus ditentukan',
            'tanggal.required' => 'Tanggal ujian harus ditentukan',
            'tanggal.date_format' => 'Format tanggal tidak diperbolehkan',
            'durasi.required' => 'Durasi ujian harus ditentukan',
            'durasi.date_format' => 'Format durasi tidak diperbolehkan'
        ]);

        if($v->fails()){
            return F::respon($v->errors(),411);
        }
        $jadwal = DB::table('jadwal_uas')
            ->insertGetId([
                'id_mapel' => $request->mapel,
                'id_user' => $request->pengawas,
                'id_rombel' => $request->kelas,
                'id_paket' => $request->paket,
                'nama_jadwal_uas' => $request->nama,
                'tanggal_jadwal_uas' => $request->tanggal,
                'durasi_jadwal_uas' => $request->durasi,
                'status_jadwal_uas' => 0,
                'id_tahun_ajaran' => DB::table('tahun_ajaran')->where('status',1)->first()->id_tahun_ajaran
            ]);

        $siswa = DB::table('profile_siswa')
            ->where('id_rombel',$request->kelas)
            ->get();

        $data = [];
        foreach ($siswa as $key => $value) {
            array_push($data,[
                    'id_user'                 => $value->id_user,
                    'id_jadwal_uas'           => $jadwal,
                    'total_soal_nilai_uas'    => null,
                    'total_dijawab_nilai_uas' => null,
                    'total_benar_nilai_uas'   => null,
                    'nilai_huruf_nilai_uas'   => null,
                    'nilai_angka_nilai_uas'   => null,
                    'tanggal_mulai_ujian'     => null,
                    'tanggal_selesai_ujian'   => null
                ]);
        }

        DB::table('nilai_uas')
                ->insert($data);
        return self::index($request);
    }

    public function state(Request $request)
    {
        DB::table('jadwal_uas')
            ->where('id_jadwal_uas',$request->id)
            ->update([
                'status_jadwal_uas' => $request->status
            ]);
        return F::respon('Berhasil diproses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(),[
            'nama' => 'required',
            'paket' => 'required',
            'kelas' => 'required',
            'mapel' => 'required',
            'pengawas' => 'required',
            'tanggal' => 'required|date_format:Y-m-d H:i:s',
            'durasi' => 'required|date_format:H:i:s'
        ],[
            'nama.required' => 'Nama ujian harus diisi',
            'paket.required' => 'Paket soal harus ditentukan',
            'kelas.required' => 'Kelas harus ditentukan',
            'mapel.required' => 'Mata pelajaran harus ditentukan',
            'pengawas.required' => 'Guru pengawas harus ditentukan',
            'tanggal.required' => 'Tanggal ujian harus ditentukan',
            'tanggal.date_format' => 'Format tanggal tidak diperbolehkan',
            'durasi.required' => 'Durasi ujian harus ditentukan',
            'durasi.date_format' => 'Format durasi tidak diperbolehkan'
        ]);

        if($v->fails()){
            return F::respon($v->errors(),411);
        }

        DB::table('jadwal_uas')
            ->where('id_jadwal_uas',$request->id)
            ->update([
                'id_mapel' => $request->mapel,
                'id_user' => $request->pengawas,
                'id_rombel' => $request->kelas,
                'id_paket' => $request->paket,
                'nama_jadwal_uas' => $request->nama,
                'tanggal_jadwal_uas' => $request->tanggal,
                'durasi_jadwal_uas' => $request->durasi,
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('nilai_uas')
            ->where('id_jadwal_uas', $request->id)
            ->delete();
        DB::table('jadwal_uas')
            ->where('id_jadwal_uas', $request->id)
            ->delete();
        return self::index($request);
    }
}
