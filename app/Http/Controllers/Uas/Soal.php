<?php

namespace App\Http\Controllers\Uas;

use App\Helpers\Adb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Soal extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function berhenti(Request $request){
        DB::table('nilai_uas')
            ->where('id_user', $request->siswa)
            ->where('id_jadwal_uas', $request->jadwal)
            ->update(['tanggal_selesai_ujian'=>date('Y-m-d H:i:s')]);
    }

    public function siswa_jawab(Request $request)
    {
        DB::table('jawaban_uas_siswa')
            ->where('id_soal_uas', $request->soal)
            ->where('id_user', $request->siswa)
            ->where('id_jadwal_uas', $request->jadwal)
            ->delete();
        DB::table('jawaban_uas_siswa')
            ->insert([
                'id_soal_uas'    => $request->soal,
                'id_user'        => $request->siswa,
                'id_jadwal_uas'  => $request->jadwal,
                'id_jawaban_uas' => $request->jawab,
            ]);
        return F::respon([
            'dijawab' => self::show($request),
            'hasil'   => self::siswa_hasil($request),
        ]);
    }

    public function siswa_hasil(Request $request)
    {
        $j_soal = DB::table('soal_uas')
            ->whereRaw('id_paket in (select id_paket from jadwal_uas where id_jadwal_uas = ?  )', [$request->jadwal])
            ->count();

        $j_dijawab = DB::table('jawaban_uas_siswa')
            ->where('jawaban_uas_siswa.id_user', $request->siswa)
            ->where('jawaban_uas_siswa.id_jadwal_uas', $request->jadwal)
            ->count();

        $benar = DB::table('jawaban_uas_siswa')
            ->join('jawaban_uas', function ($j) {
                $j->on('jawaban_uas.id_jawaban_uas', '=', 'jawaban_uas_siswa.id_jawaban_uas')
                    ->where('jawaban_uas.benar_jawaban_uas', 1);
            })
            ->where('jawaban_uas_siswa.id_user', $request->siswa)
            ->where('jawaban_uas_siswa.id_jadwal_uas', $request->jadwal)
            ->count();

        $info = F::filter(DB::table('jadwal_uas'), [
            'id_jadwal_uas'                 => 'id',
            'mapel.nama_mapel'              => 'mapel',
            'profile_gukar.nama_gukar'      => 'pengawas',
            'jurusan.nama_jurusan'          => 'jurusan',
            'level_kelas.nama_level_kelas'  => 'kelas',
            'jadwal_uas.nama_jadwal_uas'    => 'nama',
            'paket.nama_paket'              => 'paket',
            'rombel.nama_rombel'            => 'rombel',
            'jadwal_uas.tanggal_jadwal_uas' => 'tanggal',
            'jadwal_uas.durasi_jadwal_uas'  => 'durasi',
            'jadwal_uas.status_jadwal_uas'  => 'status',

        ])
            ->where('jadwal_uas.id_jadwal_uas', $request->jadwal)
            ->join('mapel', 'mapel.id_mapel', '=', 'jadwal_uas.id_mapel')
            ->join('user', 'user.id_user', '=', 'jadwal_uas.id_user')
            ->join('profile_gukar', 'profile_gukar.id_user', '=', 'user.id_user')
            ->join('rombel', 'rombel.id_rombel', '=', 'jadwal_uas.id_rombel')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'rombel.id_jurusan')
            ->join('level_kelas', 'level_kelas.id_level_kelas', '=', 'rombel.id_level_kelas')
            ->join('paket', 'paket.id_paket', '=', 'jadwal_uas.id_paket')
            ->first();

        if ($j_soal > 0) {
            $nilai = (100 / $j_soal) * $benar;
            $nilai = round($nilai, 2);
        } else {
            $nilai = 0;
        }

        if ($nilai === null) {
            $nilai_huruf = '-';
        } else if ($nilai == 0) {
            $nilai_huruf = 'E';
        } else if ($nilai >= 1 && $nilai <= 55) {
            $nilai_huruf = 'D';
        } else if ($nilai >= 56 && $nilai <= 62) {
            $nilai_huruf = 'C';
        } else if ($nilai >= 63 && $nilai <= 69) {
            $nilai_huruf = 'BC';
        } else if ($nilai >= 70 && $nilai <= 76) {
            $nilai_huruf = 'B';
        } else if ($nilai >= 77 && $nilai <= 84) {
            $nilai_huruf = 'AB';
        } else if ($nilai >= 85 && $nilai <= 100) {
            $nilai_huruf = 'A';
        } else {
            $nilai_huruf = '?';
        }

        $update = [
            'total_soal_nilai_uas'    => $j_soal,
            'total_dijawab_nilai_uas' => $j_dijawab,
            'total_benar_nilai_uas'   => $benar,
            'nilai_huruf_nilai_uas'   => $nilai_huruf,
            'nilai_angka_nilai_uas'   => $nilai,
        ];

        $cek = DB::table('nilai_uas')
            ->where('id_user', $request->siswa)
            ->where('id_jadwal_uas', $request->jadwal)
            ->first();

        if(!isset($cek->tanggal_mulai_ujian) && $cek->tanggal_mulai_ujian==null)
            $update['tanggal_mulai_ujian'] = date('Y-m-d H:i:s');

        DB::table('nilai_uas')
        ->where('id_user', $request->siswa)
        ->where('id_jadwal_uas', $request->jadwal)
        ->update($update);

        return [
            'jumlah_soal'    => $j_soal,
            'jumlah_dijawab' => $j_dijawab,
            'jumlah_benar'   => $benar,
            'nilai'          => $nilai,
            'huruf'          => $nilai_huruf,
            'info_ujian'     => $info,
        ];
    }

    public function siswa(Request $request)
    {
        self::siswa_hasil($request);
        $paging = $request->paging ?? 'on';
        $soal_d = F::filter(DB::table('soal_uas'), [
            'soal_uas.id_soal_uas' => 'id',
            'teks_soal_uas'        => 'isi',
        ])
            ->whereRaw('soal_uas.id_paket in (select id_paket from jadwal_uas where id_jadwal_uas = ? ) ', [$request->jadwal])
            ->when($paging,function($q,$paging){
                if($paging=='on')
                    return $q->paginate(5);
                else
                    return $q->get();
            });

        $soal   = [];
        $filter = [
            'jawaban_uas.id_jawaban_uas'       => 'id',
            'jawaban_uas.teks_jawaban_uas'     => 'isi',
            'jawaban_uas_siswa.id_jawaban_uas' => 'dijawab',
        ];

        if ($request->benar == 'on') {
            $filter['jawaban_uas.benar_jawaban_uas'] = 'benar';
        }

        foreach ($soal_d as $key => $value) {
            $jawaban = F::filter(DB::table('jawaban_uas'), $filter)
                ->leftJoin('jawaban_uas_siswa', function ($j) use ($value, $request) {
                    $j->on('jawaban_uas_siswa.id_jawaban_uas', '=', 'jawaban_uas.id_jawaban_uas')
                        ->where('jawaban_uas_siswa.id_soal_uas', $value->id)
                        ->where('jawaban_uas_siswa.id_user', $request->siswa)
                        ->where('jawaban_uas_siswa.id_jadwal_uas', $request->jadwal);
                })
                ->where('jawaban_uas.id_soal_uas', $value->id)
                ->get();
            array_push($soal, [
                'soal'    => $value,
                'jawaban' => $jawaban,
            ]);
        }
        $asli = json_encode($soal_d);
        $asli = json_decode($asli);
        unset($asli->data);
        if($paging=='off')
            return F::respon($soal);    
        return F::respon([
            'opsi' => $asli,
            'hasil' => $soal
        ]);
    }

    public function index(Request $request)
    {
        $cari   = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $soal_d = F::filter(DB::table('soal_uas'), [
            'soal_uas.id_soal_uas' => 'id',
            'teks_soal_uas'        => 'isi',
        ])
            ->where('soal_uas.id_paket', $request->paket)
            ->when($cari, function ($q) use ($cari) {
                return $q->where(function ($q) use ($cari) {
                    return $q->Where('soal_uas.teks_soal_uas', 'like', '%' . $cari . '%')
                        ->orWhereRaw('soal_uas.id_soal_uas in (select id_soal_uas from jawaban_uas where teks_jawaban_uas like \'%' . $cari . '%\') ');
                });
            })
            ->paginate(5);

        $soal = [];
        foreach ($soal_d as $key => $value) {
            $jawaban = F::filter(DB::table('jawaban_uas'), [
                'id_jawaban_uas'    => 'id',
                'teks_jawaban_uas'  => 'isi',
                'benar_jawaban_uas' => 'benar',
            ])
                ->where('id_soal_uas', $value->id)
                ->get();
            array_push($soal, [
                'soal'    => $value,
                'jawaban' => $jawaban,
            ]);
        }
        $n       = json_encode($soal_d);
        $n       = json_decode($n);
        $n->data = $soal;
        return F::respon($n);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cekPket = DB::table('paket')
            ->where('paket.id_paket', $request->paket)
            ->leftJoin('soal_uas', 'soal_uas.id_paket', '=', 'paket.id_paket')
            ->select('paket.maks_soal_paket as maks')
            ->addSelect(DB::raw('count(soal_uas.id_paket) as isi'))
            ->groupBy('soal_uas.id_paket')
            ->first();
        if ($cekPket->isi >= $cekPket->maks) {
            return F::respon('Penambahan tidak dapat diproses, maksimal soal sebanyak ' . $cekPket->maks, 201);
        }

        $v = Validator::make($request->all(), [
            'jawaban' => ['required', 'array', 'min:2', function ($attr, $val, $err) {
                foreach ($val as $key => $value) {
                    if ($value == '') {
                        return $err('Form jawaban harus diisi semua');
                    }

                }
            }],
            'soal'    => 'required',
            'benar'   => 'required',
        ], [
            'jawaban.required' => 'Jawaban dari soal harus ada',
            'jawaban.array'    => 'Terjadi kesalahan pada baris jawaban',
            'jawaban.min'      => 'Jumlah jawaban minimal harus 2',
            'soal.required'    => 'Soal harus diisi',
            'benar.required'   => 'Pilihan yang benar harus ada',
        ]);
        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $soal = DB::table('soal_uas')
            ->insertGetId([
                'id_paket'      => $request->paket,
                'teks_soal_uas' => $request->soal,
            ]);

        $jawaban = [];
        foreach ($request->jawaban as $key => $value) {
            array_push($jawaban, [
                'id_soal_uas'       => $soal,
                'teks_jawaban_uas'  => $value,
                'benar_jawaban_uas' => ($request->benar == $key) ? 1 : 0,
            ]);
        }

        DB::table('jawaban_uas')
            ->insert($jawaban);

        return self::index($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $value = F::filter(DB::table('soal_uas'), [
            'soal_uas.id_soal_uas' => 'id',
            'teks_soal_uas'        => 'isi',
        ])
            ->where('soal_uas.id_soal_uas', $request->soal)
            ->first();

        $jawaban = F::filter(DB::table('jawaban_uas'), [
            'jawaban_uas.id_jawaban_uas'       => 'id',
            'jawaban_uas.teks_jawaban_uas'     => 'isi',
            'jawaban_uas_siswa.id_jawaban_uas' => 'dijawab',
        ])
            ->leftJoin('jawaban_uas_siswa', function ($j) use ($value,$request) {
                $j->on('jawaban_uas_siswa.id_jawaban_uas', '=', 'jawaban_uas.id_jawaban_uas')
                    ->where('jawaban_uas_siswa.id_soal_uas', $value->id)
                    ->where('jawaban_uas_siswa.id_user', $request->siswa)
                    ->where('jawaban_uas_siswa.id_jadwal_uas', $request->jadwal);
            })
            ->where('jawaban_uas.id_soal_uas', $value->id)
            ->get();
        return [
            'soal'    => $value,
            'jawaban' => $jawaban,
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'jawaban' => ['required', 'array', 'min:2', function ($attr, $val, $err) {
                foreach ($val as $key => $value) {
                    if ($value == '') {
                        return $err('Form jawaban harus diisi semua');
                    }

                }
            }],
            'soal'    => 'required',
            'benar'   => 'required',
        ], [
            'jawaban.required' => 'Jawaban dari soal harus ada',
            'jawaban.array'    => 'Terjadi kesalahan pada baris jawaban',
            'jawaban.min'      => 'Jumlah jawaban minimal harus 2',
            'soal.required'    => 'Soal harus diisi',
            'benar.required'   => 'Pilihan yang benar harus ada',
        ]);
        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('soal_uas')
            ->where('id_soal_uas', $request->id)
            ->update([
                'teks_soal_uas' => $request->soal,
            ]);

        DB::table('jawaban_uas')
            ->where('id_soal_uas', $request->id)
            ->delete();

        $jawaban = [];
        foreach ($request->jawaban as $key => $value) {
            array_push($jawaban, [
                'id_soal_uas'       => $request->id,
                'teks_jawaban_uas'  => $value,
                'benar_jawaban_uas' => ($request->benar == $key) ? 1 : 0,
            ]);
        }

        DB::table('jawaban_uas')
            ->insert($jawaban);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        DB::table('jawaban_uas')
            ->where('id_soal_uas', $request->id)
            ->delete();
        DB::table('soal_uas')
            ->where('id_soal_uas', $request->id)
            ->delete();

        return self::index($request);
    }
}
