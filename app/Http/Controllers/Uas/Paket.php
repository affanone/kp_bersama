<?php

namespace App\Http\Controllers\Uas;

use App\Helpers\Adb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Paket extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = ($request->cari == '' || $request->cari == null) ? null : $request->cari;
        $data = F::filter(DB::table('paket'), [
            'paket.id_paket'        => 'id',
            'paket.nama_paket'      => 'nama',
            'paket.maks_soal_paket' => 'jumlah',
        ])
            ->when($cari, function ($q) use ($cari) {
                return $q->where('paket.nama_paket', 'like', '%' . $cari . '%');
            })
            ->leftJoin('soal_uas', 'soal_uas.id_paket', '=', 'paket.id_paket')
            ->addSelect(DB::raw('count(soal_uas.id_paket) as isi'))
            ->groupBy('paket.id_paket');
        if ($request->paging == 'off') {
            $data = $data->get();
        } else {
            $data = $data->paginate(10);
        }

        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama'   => 'required',
            'jumlah' => 'required|numeric|min:1',
        ], [
            'nama.required'   => 'Nama pelanggaran harus diisi',
            'jumlah.required' => 'Jumlah soal pada paket harus ditentukan',
            'jumlah.numeric'  => 'Jumlah soal pada paket harus angka',
            'jumlah.min'      => 'Jumlah soal pada paket minimal bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('paket')
            ->insert([
                'nama_paket'      => $request->nama,
                'maks_soal_paket' => $request->jumlah,
            ]);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama'   => 'required',
            'jumlah' => 'required|numeric|min:1',
        ], [
            'nama.required'   => 'Nama pelanggaran harus diisi',
            'jumlah.required' => 'Jumlah soal pada paket harus ditentukan',
            'jumlah.numeric'  => 'Jumlah soal pada paket harus angka',
            'jumlah.min'      => 'Jumlah soal pada paket minimal bernilai 1',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        DB::table('paket')
            ->where('id_paket', $request->id)
            ->update([
                'nama_paket'      => $request->nama,
                'maks_soal_paket' => $request->jumlah,
            ]);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('paket')
            ->where('id_paket', $request->id)
            ->delete();
        return self::index($request);
    }
}
