<?php

namespace App\Http\Controllers\Uas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Adb as F;
use Validator;

class Data extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function mapel(Request $request){
        $data = F::filter(DB::table('plot_ajar'),[
            'mapel.id_mapel' => 'id',
            'mapel.nama_mapel' => 'nama',
            'profile_gukar.id_user' => 'pengawas',
            'profile_gukar.nama_gukar' => 'nama_pengawas'
        ])
            ->join('rombel',function($j) use ($request) {
                $j->on('rombel.id_rombel','=','plot_ajar.id_rombel');
            })
            ->join('tahun_ajaran',function($j) use ($request) {
                $j->on('tahun_ajaran.id_tahun_ajaran','=','plot_ajar.id_tahun_ajaran')
                    ->where('tahun_ajaran.status',1);
            })
            ->join('mapel',function($j) use ($request) {
                $j->on('mapel.id_mapel','=','plot_ajar.id_mapel');
            })
            ->join('profile_gukar',function($j) use ($request) {
                $j->on('profile_gukar.id_user','=','plot_ajar.id_user');
            })
            ->where('plot_ajar.id_rombel',$request->kelas)
            ->groupBy('mapel.id_mapel')
            ->get();
        return F::respon($data);
    }

    public function mapel2(Request $request){
        $data = F::filter(DB::table('mapel'),[
            'mapel.id_mapel' => 'id',
            'mapel.nama_mapel' => 'nama',
        ])
            // ->join('rombel',function($j) use ($request) {
            //     $j->on('rombel.id_jurusan','=','mapel.id_jurusan')
            //         ->where('rombel.id_rombel',$request->kelas)
            //         ->whereRaw('rombel.id_level_kelas = mapel.id_level_kelas');
            // })
            ->whereRaw('mapel.id_mapel in (select id_mapel from plot_ajar where id_rombel = ? and id_tahun_ajaran = ? ) ',[$request->kelas,DB::table('tahun_ajaran')->where('status',1)->first()->id_tahun_ajaran])
            ->get();
        return F::respon($data);
    }


    public function guru(Request $request){
        $data = F::filter(DB::table('profile_gukar'),[
                'profile_gukar.id_profile_gukar' => 'id',
                'user.id_user' => 'user',
                'nama_gukar' => 'nama'
            ])
            ->join('user','user.id_user','=','profile_gukar.id_user')
            ->join('level_user','level_user.id_user','=','user.id_user')
            ->join('type_user',function($j){
                $j->on('type_user.id_type','=','level_user.id_type')
                    ->where('type_user.nama_type','like','%guru%');
            })
            ->get();
        return F::respon($data);
    }

    public function kelas(Request $request){
        $data = F::filter(DB::table('rombel'),[
            'rombel.id_rombel' => 'id',
            'level_kelas.nama_level_kelas' => 'level',
            'jurusan.nama_jurusan' => 'jurusan',
            'rombel.nama_rombel' => 'kelas'
        ])
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->orderBy('jurusan')
            ->orderBy('level')
            ->orderBy('kelas')
            ->get();
        return F::respon($data);
    }

    public function jurusan(Request $request){
        $data = F::filter(DB::table('jurusan'),[
            'id_jurusan' => 'id',
            'nama_jurusan' => 'nama'
        ])
            ->get();
        return F::respon($data);
    }

    public function tajaran(Request $request){
        $data = F::filter(DB::table('tahun_ajaran'),[
            'id_tahun_ajaran' => 'id',
            'nama_tahun_ajaran' => 'nama'
        ])
            ->get();
        return F::respon($data);
    }

    public function paket(Request $request){
        $data = F::filter(DB::table('paket'),[
            'id_paket' => 'id',
            'nama_paket' => 'nama',
            'maks_soal_paket' => 'max_soal'
        ])
            ->get();
        return F::respon($data);
    }

    public function level_kelas(Request $request){
        $data = F::filter(DB::table('level_kelas'),[
            'id_level_kelas' => 'id',
            'nama_level_kelas' => 'nama'
        ])
            ->get();
        return F::respon($data);
    }

    public function kelas_guru(Request $request){
        $data = F::filter(DB::table('rombel'),[
            'rombel.id_rombel' => 'id',
            'level_kelas.nama_level_kelas' => 'level',
            'jurusan.nama_jurusan' => 'jurusan',
            'rombel.nama_rombel' => 'kelas'
        ])  
            ->join('level_kelas','level_kelas.id_level_kelas','=','rombel.id_level_kelas')
            ->join('jurusan','jurusan.id_jurusan','=','rombel.id_jurusan')
            ->whereRaw(' rombel.id_rombel in (select id_rombel from jadwal_uas where id_user = ?) ',[$request->guru])
            ->get();
        return F::respon($data);
    }

    public function tajaran_guru(Request $request){
        $data = F::filter(DB::table('tahun_ajaran'),[
            'id_tahun_ajaran' => 'id',
            'nama_tahun_ajaran' => 'nama'
        ])
            ->whereRaw(' tahun_ajaran.id_tahun_ajaran in (select id_tahun_ajaran from jadwal_uas where id_user = ?) ',[$request->guru])
            ->get();
        return F::respon($data);
    }

    public function mapel_guru(Request $request){
        $data = F::filter(DB::table('mapel'),[
            'mapel.id_mapel' => 'id',
            'mapel.nama_mapel' => 'nama',
        ])
            ->whereRaw(' mapel.id_mapel in (select id_mapel from jadwal_uas where id_user = ?) ',[$request->guru])
            ->get();
        return F::respon($data);
    }
}
