<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Kode extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = F::filter(DB::table('kode_surat'), [
            'id_kode_surat'         => 'id',
            'kode_kode_surat'       => 'kode',
            'keterangan_kode_surat' => 'keterangan',
        ])
            ->get();
        return F::respon($data);
    }

    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('kode_surat'), [
            'id_kode_surat'         => 'id',
            'kode_kode_surat'       => 'kode',
            'keterangan_kode_surat' => 'keterangan',
        ])
            ->when($cari,function($q,$cari){
                return $q->where('kode_kode_surat','like','%'.$cari.'%')
                            ->orWhere('keterangan_kode_surat','like','%'.$cari.'%');
            })
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'kode'       => 'required',
            'keterangan' => 'required',
        ], [
            'kode.required'       => 'Kode surat harus diisi',
            'keterangan.required' => 'Keterangan surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('kode_surat')
            ->insert([
                'kode_kode_surat'       => $request->kode,
                'keterangan_kode_surat' => $request->keterangan,
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'kode'       => 'required',
            'keterangan' => 'required',
        ], [
            'kode.required'       => 'Kode surat harus diisi',
            'keterangan.required' => 'Keterangan surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('kode_surat')
            ->where('id_kode_surat', $request->id)
            ->update([
                'kode_kode_surat'       => $request->kode,
                'keterangan_kode_surat' => $request->keterangan,
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = DB::table('kode_surat')
            ->where('id_kode_surat', $request->id)
            ->delete();
        return self::index($request);
    }
}
