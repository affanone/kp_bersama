<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Jenis extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function template(Request $request)
    {
        $data = F::filter(DB::table('template_jenis_surat'), [
            'id_template_jenis_surat'   => 'id',
            'nama_template_jenis_surat' => 'nama',
            'template_jenis_surat'      => 'template',
        ])
            ->where('id_jenis_surat',$request->id)
            ->get();
        return F::respon($data);
    }

    public function all()
    {
        $data = F::filter(DB::table('jenis_surat'), [
            'id_jenis_surat'   => 'id',
            'nama_jenis_surat' => 'nama',
            'kode_jenis_surat' => 'kode',
        ])
            ->get();
        return F::respon($data);
    }

    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('jenis_surat'), [
            'id_jenis_surat'   => 'id',
            'nama_jenis_surat' => 'nama',
            'kode_jenis_surat' => 'kode',
        ])
            ->when($cari, function ($q, $cari) {
                return $q->where('kode_jenis_surat', 'like', '%' . $cari . '%')
                    ->orWhere('template_jenis_surat', 'like', '%' . $cari . '%');
            })
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'kode' => ['required', function ($attr, $val, $err) {
                $cek = DB::table('jenis_surat')
                    ->where('kode_jenis_surat', $val)
                    ->count();
                if ($cek > 0) {
                    return $err('Kode "' . $val . '" sudah ada!');
                }

            }],
            'nama' => 'required',
        ], [
            'kode.required'     => 'Kode jenis surat harus diisi',
            'nama.required'     => 'Nama jenis surat harus diisi',
            'template.required' => 'Template jenis surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $jenis_surat = DB::table('jenis_surat')
            ->insertGetId([
                'kode_jenis_surat' => $request->kode,
                'nama_jenis_surat' => $request->nama,
            ]);

        $template = json_decode($request->template);

        DB::table('template_jenis_surat')
            ->where('id_template_jenis_surat',$request->id)
            ->delete();
        $insert = [];
        foreach ($template as $key => $value) {
            array_push($insert,[
                'id_jenis_surat' => $jenis_surat,
                'nama_template_jenis_surat' => $value->nama,
                'template_jenis_surat' => $value->template
            ]);
        }
        DB::table('template_jenis_surat')
            ->insert($insert);

        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'kode' => ['required', function ($attr, $val, $err) use ($request) {
                $cek = DB::table('jenis_surat')
                    ->where('id_jenis_surat', '<>', $request->id)
                    ->where('kode_jenis_surat', $val)
                    ->count();
                if ($cek > 0) {
                    return $err('Kode "' . $val . '" sudah ada!');
                }

            }],
            'nama' => 'required',
        ], [
            'kode.required'     => 'Kode jeni surat harus diisi',
            'nama.required'     => 'Nama jeni surat harus diisi',
            'template.required' => 'Template jenis surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('jenis_surat')
            ->where('id_jenis_surat', $request->id)
            ->update([
                'kode_jenis_surat' => $request->kode,
                'nama_jenis_surat' => $request->nama,
            ]);

        $template = json_decode($request->template);

        DB::table('template_jenis_surat')
            ->where('id_jenis_surat',$request->id)
            ->delete();

        $update = [];
        foreach ($template as $key => $value) {
            array_push($update,[
                'id_jenis_surat' => $request->id,
                'nama_template_jenis_surat' => $value->nama,
                'template_jenis_surat' => $value->template
            ]);
        }
        
        DB::table('template_jenis_surat')
            ->insert($update);

        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('template_jenis_surat')
            ->where('id_jenis_surat',$request->id)
            ->delete();

        $data = DB::table('jenis_surat')
            ->where('id_jenis_surat', $request->id)
            ->delete();
        return self::index($request);
    }
}
