<?php

namespace App\Http\Controllers\Surat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\Tdb as F;
use Validator;

use App\Http\Controllers\Surat\Kode;
use App\Http\Controllers\Surat\Jenis;
use App\Http\Controllers\Surat\Adisposisi;
class Data extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->data ?? null;

        $my_data = [
            'jenis',
            'disposisi'
        ];

        $v    = Validator::make($request->all(), [
            'data'    => 'required|string',
        ], [
            'data.required'  => 'Data apa saja yang dibutuhkan?',
            'data.string'    => 'Format data harus string, ex: data1,data2,data3...',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        } else {
            $res = [];
            $ex = explode(',', $request->data);

            foreach ($ex as $key => $value) {
                if(!in_array($value, $my_data)){
                    $v->errors()->add('data', 'data [' . $value . '] tidak tersedia, gunakan data ini : ' . substr(join($my_data, ', '), 0,strlen(join($my_data, ', '))));
                    return F::respon($v->errors(), 411);
                }

                if ($value=='jenis') $ctrl = new Jenis;
                else if ($value=='disposisi') $ctrl = new Adisposisi;
                $res[$value] = $ctrl->all()->original['result'];
            }
            return F::respon($res);
        }
    }
}