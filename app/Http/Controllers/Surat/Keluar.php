<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class Keluar extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    public function status(Request $request){
        $data = DB::table('surat_keluar')
            ->where('id_surat_keluar', $request->id)
            ->update([
                'status_surat_keluar' => ($request->status==1)?0:1
            ]);
        return self::index($request);
    }

    public function arsip(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'tanggal_awal' => 'required:date_format:Y-m-d',
            'tanggal_akhir' => 'required:date_format:Y-m-d',
            'nama'  => ['required',function($attr,$val,$err){
                $cek = DB::table('arsip_surat')
                    ->where('nama_arsip_surat',$val)
                    ->count();
                if($cek>0)
                    return $err('Nama arsip sudah ada!');
            }]
        ],[
            'tanggal_awal.required' => 'Tanggal awal pengarsipan harus ada',
            'tanggal_awal.date_format' => 'Format tanggal harus tahun-bulan-tanggal',
            'tanggal_akhir.required' => 'Tanggal akhir pengarsipan harus ada',
            'tanggal_akhir.date_format' => 'Format tanggal harus tahun-bulan-tanggal',
            'nama.required' => 'Nama arsip diperlukan!'
        ]);

        if($validator->fails())
            return F::respon($validator->errors(),411);

        $arsip = DB::table('arsip_surat')
            ->insertGetId([
                'nama_arsip_surat' => $request->nama,
                'tanggal_arsip_surat' => date('Y-m-d H:i:s'),
                'dari_tanggal_arsip_surat' => $request->tanggal_awal,
                'sampai_tanggal_arsip_surat' => $request->tanggal_akhir
            ]);

        $tanggal_awal = $request->tanggal_awal ?? null;
        $tanggal_akhir = $request->tanggal_akhir ?? null;

        $select = DB::table('surat_keluar')
            ->when($tanggal_awal, function ($q) use ($tanggal_awal,$tanggal_akhir) {
                $from = date($tanggal_awal);
                $to = date($tanggal_akhir);
                return $q->whereBetween('tanggal_surat_keluar', [$from , $to]);
            })
            ->whereRaw('id_surat_keluar NOT IN (select id_surat_keluar from arsip_surat_detail)')
            ->select('id_surat_keluar')
            ->addSelect(DB::raw('"'.$arsip.'" as id_arsip_surat'));
        $binding = $select->getBindings();
        $insertQuery = 'insert into arsip_surat_detail (id_surat_keluar,id_arsip_surat)'.$select->toSql();
        DB::insert($insertQuery,$binding);
        return self::index($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $tanggal_awal = $request->tanggal_awal ?? null;
        $tanggal_akhir = $request->tanggal_akhir ?? null;
        $data = F::filter(DB::table('surat_keluar'), [
            'id_surat_keluar'       => 'id',
            'no_surat_keluar'       => 'nomor',
            'tujuan_surat_keluar'   => 'tujuan',
            'alamat_surat_keluar'   => 'alamat',
            'tanggal_surat_keluar'  => 'tanggal',
            'lampiran_surat_keluar' => 'lampiran',
            'perihal_surat_keluar'  => 'perihal',
            'isi_surat_keluar'      => 'isi',
            'scan_surat_keluar'     => 'download',
            'id_jenis_surat'        => 'jenis',
            'status_surat_keluar'   => 'status',
        ])
            ->when($cari, function ($q, $cari) {
                return $q->where('no_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('tujuan_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('alamat_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('tanggal_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('lampiran_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('perihal_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('isi_surat_keluar', 'like', '%' . $cari . '%');
            })
            ->when($tanggal_awal, function ($q) use ($tanggal_awal,$tanggal_akhir) {
                $from = date($tanggal_awal);
                $to = date($tanggal_akhir);
                return $q->whereBetween('tanggal_surat_keluar', [$from , $to]);
            })
            ->whereRaw('id_surat_keluar NOT IN (select id_surat_keluar from arsip_surat_detail)')
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nomor'   => 'required',
            'tujuan'  => 'required',
            'alamat'  => 'required',
            'tanggal' => 'required|date_format:Y-m-d',
            'perihal' => 'required',
            'isi'     => 'required',
            'jenis'   => 'required',
        ], [
            'nomor.required'      => 'Nomor surat harus diisi',
            'kode_surat.required' => 'Kode surat harus diisi',
            'tujuan.required'     => 'tujuan surat harus diisi',
            'alamat.required'     => 'Alamat surat harus diisi',
            'tanggal.required'    => 'Tanggal surat harus diisi',
            'perihal.required'    => 'Perihal surat harus diisi',
            'tanggal.date_format' => 'Format tangga harus Tahun-Bulan-tanggal',
            'isi.required'        => 'Isi surat harus diisi',
            'jenis.required'      => 'Jenis surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $lampiran = $request->lampiran ?? '-';

        $link = null;
        if (isset($_FILES['file'])) {
            $File          = $request->file('file');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $link          = $newname;
            $images_format = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx']; // auto thumbnail
            if (!in_array(strtolower($format), $images_format)) {
                $v->errors()->add('file', 'Format file tidak didukung oleh sistem!');
                return F::respon($v->errors(), 411);
            }
            $request->file('file')->storeAs('', $newname, 'surat');
        }

        $data = DB::table('surat_keluar')
            ->insert([
                'no_surat_keluar'       => $request->nomor,
                'tujuan_surat_keluar'   => $request->tujuan,
                'alamat_surat_keluar'   => $request->alamat,
                'tanggal_surat_keluar'  => $request->tanggal,
                'perihal_surat_keluar'  => $request->perihal,
                'isi_surat_keluar'      => $request->isi,
                'id_jenis_surat'        => $request->jenis,
                'scan_surat_keluar'     => $link,
                'lampiran_surat_keluar' => $lampiran,
                'status_surat_keluar'   => 0
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function download(Request $request)
    {
        $f = explode('.', $request->file);
        $c = DB::table('surat_keluar')
            ->where('scan_surat_keluar',$request->file)
            ->count();
        if($c==0)
            return 'FILE NOT FOUND';

        return Storage::disk('surat')->download($request->file, 'SURAR_KELUAR_'.date('YmdHis').'.'.end($f));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nomor'   => 'required',
            'tujuan'  => 'required',
            'alamat'  => 'required',
            'tanggal' => 'required|date_format:Y-m-d',
            'perihal' => 'required',
            'isi'     => 'required',
            'jenis'   => 'required',
        ], [
            'nomor.required'      => 'Nomor surat harus diisi',
            'tujuan.required'     => 'tujuan surat harus diisi',
            'alamat.required'     => 'Alamat surat harus diisi',
            'tanggal.required'    => 'Tanggal surat harus diisi',
            'perihal.required'    => 'Perihal surat harus diisi',
            'tanggal.date_format' => 'Format tangga harus Tahun-Bulan-tanggal',
            'isi.required'        => 'Isi surat harus diisi',
            'jenis.required'      => 'Jenis surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $lampiran = $request->lampiran ?? '-';

        $update = [
                'no_surat_keluar'       => $request->nomor,
                'tujuan_surat_keluar'   => $request->tujuan,
                'alamat_surat_keluar'   => $request->alamat,
                'tanggal_surat_keluar'  => $request->tanggal,
                'perihal_surat_keluar'  => $request->perihal,
                'isi_surat_keluar'      => $request->isi,
                'id_jenis_surat'        => $request->jenis,
                'lampiran_surat_keluar' => $lampiran,
            ];

        $link = null;
        if (isset($_FILES['file'])) {
            $file = DB::table('surat_keluar')
                ->where('id_surat_keluar',$request->id)
                ->first();
            if($file->scan_surat_keluar!=null)
                Storage::disk('surat')->delete($file->scan_surat_keluar);

            $File          = $request->file('file');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $link          = $newname;
            $images_format = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx']; // auto thumbnail
            if (!in_array(strtolower($format), $images_format)) {
                $v->errors()->add('file', 'Format file tidak didukung oleh sistem!');
                return F::respon($v->errors(), 411);
            }
            $request->file('file')->storeAs('', $newname, 'surat');
            $update['scan_surat_keluar'] = $link;
        }else{
            if($request->file_hapus=='true'){
                $file = DB::table('surat_keluar')
                    ->where('id_surat_keluar',$request->id)
                    ->first();
                if($file->scan_surat_keluar!=null)
                    Storage::disk('surat')->delete($file->scan_surat_keluar);
                $update['scan_surat_keluar'] = null;
            }
        }


        $data = DB::table('surat_keluar')
            ->where('id_surat_keluar', $request->id)
            ->update($update);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $file = DB::table('surat_keluar')
            ->where('id_surat_keluar',$request->id)
            ->first();
        if($file->scan_surat_keluar!=null)
            Storage::disk('surat')->delete($file->scan_surat_keluar);

        $data = DB::table('surat_keluar')
            ->where('id_surat_keluar', $request->id)
            ->delete();
        return self::index($request);
    }

    public function jenis(Request $request)
    {
        $sparator = '/';
        $tahun    = date('Y');
        $bulan    = null;
        switch (intval(date('m'))) {
            case 1:
                $bulan = 'I';
                break;
            case 2:
                $bulan = 'II';
                break;
            case 3:
                $bulan = 'III';
                break;
            case 4:
                $bulan = 'IV';
                break;
            case 5:
                $bulan = 'V';
                break;
            case 6:
                $bulan = 'VI';
                break;
            case 7:
                $bulan = 'VII';
                break;
            case 8:
                $bulan = 'VIII';
                break;
            case 9:
                $bulan = 'IX';
                break;
            case 10:
                $bulan = 'X';
                break;
            case 11:
                $bulan = 'XI';
                break;
            case 12:
                $bulan = 'XII';
                break;
        }
        $jenis = DB::table('jenis_surat')
            ->where('id_jenis_surat', $request->id)
            ->first();

        $template = DB::table('template_jenis_surat')
            ->where('id_template_jenis_surat', $request->template)
            ->first();


        $surat_keluar = DB::table('surat_keluar')
            ->where('id_jenis_surat',$request->id)
            ->orderBy('id_surat_keluar','desc')
            ->first();

        $nomor_a       = explode($sparator, $jenis->kode_jenis_surat);
        $nomor_a_index = array_search('%nomor%', $nomor_a);
        $arsip_index = array_search('%arsip%', $nomor_a);

        if($surat_keluar!=null){
            $arsip_detail = DB::table('arsip_surat_detail')
                ->where('id_surat_keluar',$surat_keluar->id_surat_keluar)
                ->orderBy('id_arsip_surat_detail','desc');

            if($arsip_detail->count()>0){
                // arsip baru
                $arsip_array = explode($sparator, $surat_keluar->no_surat_keluar);
                if(($arsip_array[$arsip_index]+1)<10)
                    $arsip_get = '00' .($arsip_array[$arsip_index]+1);
                else if(($arsip_array[$arsip_index]+1)<100)
                    $arsip_get = '0' .($arsip_array[$arsip_index]+1);
                else 
                    $arsip_get = ($arsip_array[$arsip_index]+1);
                $nomor = '001';
                $nomor_surat = str_replace(['%nomor%', '%arsip%', '%bulan%', '%tahun%'], [$nomor, $arsip_get, $bulan, $tahun], $jenis->kode_jenis_surat);
                return F::respon([
                    'type'             => 1,
                    'perdana'        => false,
                    'posisi_nomor'   => $nomor_a_index,
                    'posisi_arsip'   => $arsip_index,
                    'sparator'       => $sparator,
                    'template_surat' => $template->template_jenis_surat,
                    'nomor_surat'    => $nomor_surat,
                ]);
            }
        }else{
            $nomor_surat = str_replace(['%nomor%', '%arsip%', '%bulan%', '%tahun%'], ['001', '001', $bulan, $tahun], $jenis->kode_jenis_surat);
            return F::respon([
                'type'             => 2,
                'perdana'        => true,
                'posisi_nomor'   => $nomor_a_index,
                'posisi_arsip'   => $arsip_index,
                'sparator'       => $sparator,
                'template_surat' => $template->template_jenis_surat,
                'nomor_surat'    => $nomor_surat,
            ]);
        }

        $arsip_array  = explode($sparator, $surat_keluar->no_surat_keluar);
        $arsip_nomor = $arsip_array[$arsip_index];

        if ($nomor_a_index == false) {
            $nomor_surat = str_replace(['%nomor%', '%arsip%', '%bulan%', '%tahun%'], ['???', $arsip_nomor, $bulan, $tahun], $jenis->kode_jenis_surat);
            return F::respon([
                'type'             => 3,
                'perdana'        => false,
                'posisi_nomor'   => $nomor_a_index,
                'posisi_arsip'   => $arsip_index,
                'sparator'       => $sparator,
                'template_surat' => $template->template_jenis_surat,
                'nomor_surat'    => $nomor_surat,
            ]);
        }

        
        $bulan_a_index = array_search('%bulan%', $nomor_a);
        $tahun_a_index = array_search('%tahun%', $nomor_a);
        $surat         = DB::table('surat_keluar')
            ->where('id_jenis_surat', $request->id)
            ->orderBy('id_surat_keluar', 'desc')
            ->first();
        $awal = false;
        if ($surat != null) {
            $nomor_b = explode($sparator, $surat->no_surat_keluar);
            $nomor_c = 0;
        } else {
            $awal    = true;
            $nomor_b = str_replace(['%nomor%', '%arsip%', '%bulan%', '%tahun%'], [000, 000, $bulan, $tahun], $jenis->kode_jenis_surat);
            $nomor_c = 0;
        }
        if (isset($nomor_b[$nomor_a_index]) && is_numeric($nomor_b[$nomor_a_index])) {
            if ($request->id == $request->edit) {
                $nomor_c = intval($nomor_b[$nomor_a_index]);
            } else {
                $nomor_c = intval($nomor_b[$nomor_a_index]) + 1;
            }

        } else {
            $nomor_c += 1;
        }
        if ($nomor_c < 10) {
            $nomor_c = '00' . $nomor_c;
        } else if ($nomor_c < 100) {
            $nomor_c = '0' . $nomor_c;
        }

        $nomor_surat = str_replace(['%nomor%', '%arsip%', '%bulan%', '%tahun%'], [$nomor_c, $arsip_nomor, $bulan, $tahun], $jenis->kode_jenis_surat);
        return F::respon([
            'type'           => 4,
            'perdana'        => $awal,
            'posisi_nomor'   => $nomor_a_index,
            'posisi_arsip'   => $arsip_index,
            'sparator'       => $sparator,
            'template_surat' => $template->template_jenis_surat,
            'nomor_surat'    => $nomor_surat,
        ]);
    }
}
