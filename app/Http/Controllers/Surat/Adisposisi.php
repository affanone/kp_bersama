<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Adisposisi extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = F::filter(DB::table('adisposisi'), [
            'id_adisposisi'   => 'id',
            'nama_adisposisi' => 'nama',
        ])
            ->get();
        return F::respon($data);

    }

    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('adisposisi'), [
            'id_adisposisi'   => 'id',
            'nama_adisposisi' => 'nama',
        ])
            ->when($cari,function($q,$cari){
                return $q->where('nama_adisposisi','like','%'.$cari.'%');
            })
            ->paginate(10);
        return F::respon($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
        ], [
            'nama.required' => 'Nama disposisi harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('adisposisi')
            ->insert([
                'nama_adisposisi' => $request->nama,
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nama' => 'required',
        ], [
            'nama.required' => 'Nama disposisi harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('adisposisi')
            ->where('id_adisposisi', $request->id)
            ->update([
                'nama_adisposisi' => $request->nama,
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = DB::table('adisposisi')
            ->where('id_adisposisi', $request->id)
            ->delete();
        return self::index($request);
    }
}
