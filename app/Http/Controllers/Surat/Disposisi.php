<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Disposisi extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function rincian(Request $request){
        $data = F::filter(DB::table('disposisi'), [
            'id_disposisi'      => 'id',
            'nama_adisposisi'     => 'disposisi',
            'tanggal_disposisi' => 'tanggal',
            'isi_disposisi'     => 'isi',
        ])
            ->join('adisposisi','adisposisi.id_adisposisi','=','disposisi.id_adisposisi')
            ->where('disposisi.id_surat_masuk',$request->id)
            ->get();
        $dsp = [];
        foreach ($data as $key => $value) {
            array_push($dsp, $value->disposisi);
        }
        if(count($data)>0){
            return F::respon([
                'disposisi' => $dsp,
                'tanggal' => $data[0]->tanggal,
                'keterangan' => $data[0]->isi,
            ]);   
        }else{
            return F::respon(null);
        }
    }

    public function index()
    {
        $data = F::filter(DB::table('disposisi'), [
            'id_disposisi'      => 'id',
            'id_surat_masuk'    => 'surat_masuk',
            'id_adisposisi'     => 'disposisi',
            'tanggal_disposisi' => 'tanggal',
            'isi_disposisi'     => 'isi',
        ])
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'surat_masuk' => 'required',
            'disposisi'   => 'required',
            'tanggal'     => 'required|date_format:Y-m-d',
            'isi'         => 'required',
        ], [
            'surat_masuk.required' => 'Surat masuk harus dipilih',
            'disposisi.required'   => 'Disposisi harus ada',
            'tanggal.required'     => 'Tanggal disposisi harus ada',
            'tanggal.date_format'  => 'Format tangga harus Tahun-Bulan-tanggal',
            'isi.required'         => 'Isi disposisi dibutuhkan',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('disposisi')
            ->insert([
                'id_surat_masuk'    => $request->surat_masuk,
                'id_adisposisi'     => $request->disposisi,
                'tanggal_disposisi' => $request->tanggal,
                'isi_disposisi'     => $request->isi,
            ]);
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'surat_masuk' => 'required',
            'disposisi'   => 'required',
            'tanggal'     => 'required|date_format:Y-m-d',
            'isi'         => 'required',
        ], [
            'surat_masuk.required' => 'Surat masuk harus dipilih',
            'disposisi.required'   => 'Disposisi harus ada',
            'tanggal.required'     => 'Tanggal disposisi harus ada',
            'tanggal.date_format'  => 'Format tangga harus Tahun-Bulan-tanggal',
            'isi.required'         => 'Isi disposisi dibutuhkan',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $data = DB::table('disposisi')
            ->where('id_disposisi', $request->id)
            ->update([
                'id_surat_masuk'    => $request->surat_masuk,
                'id_adisposisi'     => $request->disposisi,
                'tanggal_disposisi' => $request->tanggal,
                'isi_disposisi'     => $request->isi,
            ]);
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = DB::table('disposisi')
            ->where('id_disposisi', $request->id)
            ->delete();
        return self::index($request);
    }

    public function masuk(Request $request){
        $data = F::filter(DB::table('disposisi'), [
            'disposisi.id_disposisi'      => 'id',
            'disposisi.id_adisposisi'      => 'disposisi',
            'disposisi.tanggal_disposisi' => 'tanggal',
            'disposisi.isi_disposisi'     => 'isi',
        ])
            ->where('id_surat_masuk',$request->id)
            ->get();
        return F::respon($data);
    }
}
