<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class Masuk extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $tanggal_awal = $request->tanggal_awal ?? null;
        $tanggal_akhir = $request->tanggal_akhir ?? null;
        $data = F::filter(DB::table('surat_masuk'), [
            'id_surat_masuk'       => 'id',
            'no_surat_masuk'       => 'nomor',
            'instansi_surat_masuk' => 'instansi',
            'alamat_surat_masuk'   => 'alamat',
            'tanggal_surat_masuk'  => 'tanggal',
            'lampiran_surat_masuk' => 'lampiran',
            'perihal_surat_masuk'  => 'perihal',
            'scan_surat_masuk'     => 'download',
            'isi_surat_masuk'      => 'isi',
        ])
            ->when($cari,function($q,$cari){
                return $q->where('no_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('no_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('instansi_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('alamat_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('tanggal_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('lampiran_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('perihal_surat_masuk','like','%'.$cari.'%')
                            ->orWhere('isi_surat_masuk','like','%'.$cari.'%');
            })

            ->when($tanggal_awal, function ($q) use ($tanggal_awal,$tanggal_akhir) {
                $from = date($tanggal_awal);
                $to = date($tanggal_akhir);
                return $q->whereBetween('tanggal_surat_masuk', [$from , $to]);
            })
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nomor'    => 'required',
            'instansi' => 'required',
            'alamat'   => 'required',
            'tanggal'  => 'required|date_format:Y-m-d',
            // 'lampiran'  => 'required',
            // 'perihal'  => 'required',
            // 'scan'     => 'required',
            // 'isi'      => 'required',
        ], [
            'nomor.required'      => 'Nomor surat harus diisi',
            'instansi.required'   => 'Instansi surat harus diisi',
            'alamat.required'     => 'Alamat surat harus diisi',
            'tanggal.required'    => 'Tanggal surat harus diisi',
            'tanggal.date_format' => 'Format tangga harus Tahun-Bulan-tanggal',
            'perihal.required'    => 'Perihal surat harus diisi',
            'scan.required'       => 'Scan surat harus diisi',
            'isi.required'        => 'Isi surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $link = null;
        if (isset($_FILES['file'])) {
            $File          = $request->file('file');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $link          = $newname;
            $images_format = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx']; // auto thumbnail
            if (!in_array(strtolower($format), $images_format)) {
                $v->errors()->add('file', 'Format file tidak didukung oleh sistem!');
                return F::respon($v->errors(), 411);
            }
            $request->file('file')->storeAs('', $newname, 'surat');
        }

        $surat_masuk = DB::table('surat_masuk')
            ->insertGetId([
                'no_surat_masuk'       => $request->nomor,
                'instansi_surat_masuk' => $request->instansi,
                'alamat_surat_masuk'   => $request->alamat,
                'tanggal_surat_masuk'  => $request->tanggal,
                'lampiran_surat_masuk' => $request->lampiran??'-',
                'perihal_surat_masuk'  => $request->perihal??'-',
                'scan_surat_masuk'     => $link,
                'isi_surat_masuk'      => $request->isi??'-',
            ]);

        $disposisi = json_decode($request->salindisposisi);
        $data = [];
        foreach ($disposisi->disposisi as $key => $value) {
            array_push($data, [
                'id_adisposisi' => $value,
                'id_surat_masuk' => $surat_masuk,
                'tanggal_disposisi' => $disposisi->tanggal,
                'isi_disposisi' => $disposisi->isi
            ]);
        }
        DB::table('disposisi')
            ->insert($data);
        
        return self::index($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function download(Request $request)
    {
        $f = explode('.', $request->file);
        $c = DB::table('surat_masuk')
            ->where('scan_surat_masuk',$request->file)
            ->count();
        if($c==0)
            return 'FILE NOT FOUND';
        
        return Storage::disk('surat')->download($request->file, 'SURAR_MASUK_'.date('YmdHis').'.'.end($f));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nomor'    => 'required',
            'instansi' => 'required',
            'alamat'   => 'required',
            'tanggal'  => 'required|date_format:Y-m-d',
            // 'lampiran'  => 'required',
            // 'perihal'  => 'required',
            // 'scan'     => 'required',
            // 'isi'      => 'required',
        ], [
            'nomor.required'      => 'Nomor surat harus diisi',
            'instansi.required'   => 'Instansi surat harus diisi',
            'alamat.required'     => 'Alamat surat harus diisi',
            'tanggal.required'    => 'Tanggal surat harus diisi',
            'tanggal.date_format' => 'Format tangga harus Tahun-Bulan-tanggal',
            'perihal.required'    => 'Perihal surat harus diisi',
            'scan.required'       => 'Scan surat harus diisi',
            'isi.required'        => 'Isi surat harus diisi',
        ]);

        if ($v->fails()) {
            return F::respon($v->errors(), 411);
        }

        $update = [
                'no_surat_masuk'       => $request->nomor,
                'instansi_surat_masuk' => $request->instansi,
                'alamat_surat_masuk'   => $request->alamat,
                'tanggal_surat_masuk'  => $request->tanggal,
                'lampiran_surat_masuk' => $request->lampiran??'-',
                'perihal_surat_masuk'  => $request->perihal??'-',
                'isi_surat_masuk'      => $request->isi??'-',
            ];

        $link = null;
        if (isset($_FILES['file'])) {
            $file = DB::table('surat_masuk')
                ->where('id_surat_masuk',$request->id)
                ->first();
            if($file->scan_surat_masuk!=null)
                Storage::disk('surat')->delete($file->scan_surat_masuk);

            $File          = $request->file('file');
            $filename      = $File->getClientOriginalName();
            $format        = $File->getClientOriginalExtension();
            $newname       = md5(date('Y-m-d H:i:s')) . '.' . $format;
            $link          = $newname;
            $images_format = ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx']; // auto thumbnail
            if (!in_array(strtolower($format), $images_format)) {
                $v->errors()->add('file', 'Format file tidak didukung oleh sistem!');
                return F::respon($v->errors(), 411);
            }
            $request->file('file')->storeAs('', $newname, 'surat');
            $update['scan_surat_masuk'] = $link;
        }else{
            if($request->file_hapus=='true'){
                $file = DB::table('surat_masuk')
                    ->where('id_surat_masuk',$request->id)
                    ->first();
                if($file->scan_surat_masuk!=null)
                    Storage::disk('surat')->delete($file->scan_surat_masuk);
                $update['scan_surat_masuk'] = null;
            }
        }

        DB::table('surat_masuk')
            ->where('id_surat_masuk',$request->id)
            ->update($update);
        DB::table('disposisi')
            ->where('id_surat_masuk',$request->id)
            ->delete();
        $disposisi = json_decode($request->salindisposisi);
        $data = [];
        foreach ($disposisi->disposisi as $key => $value) {
            array_push($data, [
                'id_adisposisi' => $value,
                'id_surat_masuk' => $request->id,
                'tanggal_disposisi' => $disposisi->tanggal,
                'isi_disposisi' => $disposisi->isi
            ]);
        }
        DB::table('disposisi')
            ->insert($data);
        
        return self::index($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $file = DB::table('surat_masuk')
            ->where('id_surat_masuk',$request->id)
            ->first();
        if($file->scan_surat_masuk!=null)
            Storage::disk('surat')->delete($file->scan_surat_masuk);

        DB::table('disposisi')
            ->where('id_surat_masuk',$request->id)
            ->delete();
            
        $data = DB::table('surat_masuk')
            ->where('id_surat_masuk', $request->id)
            ->delete();
        return self::index($request);
    }
}
