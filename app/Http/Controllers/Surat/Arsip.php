<?php

namespace App\Http\Controllers\Surat;

use App\Helpers\Tdb as F;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Arsip extends Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cari = $request->cari ?? null;
        $data = F::filter(DB::table('arsip_surat'), [
            'arsip_surat.id_arsip_surat'             => 'id',
            'nama_arsip_surat'           => 'nama',
            'tanggal_arsip_surat'        => 'tanggal',
            'dari_tanggal_arsip_surat'   => 'dari',
            'sampai_tanggal_arsip_surat' => 'sampai',
            'a.jumlah'                   => 'jumlah_surat',
        ])
            ->when($cari, function ($q, $cari) {
                return $q->where('nama_arsip_surat', 'like', '%' . $cari . '%');
            })

            ->join(DB::raw(' (select a.id_arsip_surat, a.id_surat_keluar, count(a.id_arsip_surat) as jumlah from arsip_surat_detail a group by a.id_arsip_surat)a'), function ($j) {
                $j->on('a.id_arsip_surat', '=', 'arsip_surat.id_arsip_surat');
            })
            ->orderBy('tanggal_arsip_surat', 'desc')
            ->paginate(10);
        return F::respon($data);
    }

    public function detail(Request $request)
    {
        $cari          = $request->cari ?? null;
        $tanggal_awal  = $request->tanggal_awal ?? null;
        $tanggal_akhir = $request->tanggal_akhir ?? null;
        $data          = F::filter(DB::table('surat_keluar'), [
            'id_surat_keluar'       => 'id',
            'no_surat_keluar'       => 'nomor',
            'tujuan_surat_keluar'   => 'tujuan',
            'alamat_surat_keluar'   => 'alamat',
            'tanggal_surat_keluar'  => 'tanggal',
            'lampiran_surat_keluar' => 'lampiran',
            'perihal_surat_keluar'  => 'perihal',
            'isi_surat_keluar'      => 'isi',
            'scan_surat_keluar'     => 'download',
            'id_jenis_surat'        => 'jenis',
        ])
            ->when($cari, function ($q, $cari) {
                 return $q->where('no_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('tujuan_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('alamat_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('tanggal_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('lampiran_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('perihal_surat_keluar', 'like', '%' . $cari . '%')
                    ->orWhere('isi_surat_keluar', 'like', '%' . $cari . '%');
            })
            ->when($tanggal_awal, function ($q) use ($tanggal_awal, $tanggal_akhir) {
                $from = date($tanggal_awal);
                $to   = date($tanggal_akhir);
                return $q->whereBetween('tanggal_surat_keluar', [$from, $to]);
            })
            ->whereRaw('id_surat_keluar IN (select id_surat_keluar from arsip_surat_detail where id_arsip_surat = ?)', [$request->arsip])
            ->paginate(10);
        return F::respon($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('arsip_surat_detail')
            ->where('id_arsip_surat', $request->arsip)
            ->delete();

        DB::table('arsip_surat')
            ->where('id_arsip_surat', $request->arsip)
            ->delete();

        return self::index($request);
    }
}
