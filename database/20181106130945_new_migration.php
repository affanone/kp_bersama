<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class NewMigration extends AbstractMigration
{
    public function change()
    {

        //TABEL AINUN ------------------------------------------------------------
        //Data Induk
        $type_user = $this->table('type_user', ['id' => false, 'primary_key' => 'id_type']);
        $type_user->addColumn('id_type', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_type', 'string', ['limit' => 20])
            ->save();

        $user = $this->table('user', ['id' => false, 'primary_key' => 'id_user']);
        $user->addColumn('id_user', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('email', 'string', ['limit' => 40])
            ->addColumn('password', 'string', ['limit' => 100])
            ->addIndex(['email'], ['unique' => true])
            ->save();

        $level_user = $this->table('level_user', ['id' => false, 'primary_key' => 'id_level_user']);
        $level_user->addColumn('id_level_user', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_type', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_level_user'))
            ->addForeignKey('id_type', 'type_user', 'id_type', $this->myConstraint('type_user_leveluser'))
            ->save();

        $jurusan = $this->table('jurusan', ['id' => false, 'primary_key' => 'id_jurusan']);
        $jurusan->addColumn('id_jurusan', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_SMALL])
            ->addColumn('nama_jurusan', 'string', ['limit' => 30])
            ->save();

        $level_kelas = $this->table('level_kelas', ['id' => false, 'primary_key' => 'id_level_kelas']);
        $level_kelas->addColumn('id_level_kelas', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_level_kelas', 'string', ['limit' => 20])
            ->save();

        $rombel = $this->table('rombel', ['id' => false, 'primary_key' => 'id_rombel']);
        $rombel->addColumn('id_rombel', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nama_rombel', 'string', ['limit' => 20])
            ->addColumn('id_jurusan', 'integer', ['limit' =>  MysqlAdapter::INT_REGULAR])
            ->addColumn('id_level_kelas', 'integer', ['limit' =>  MysqlAdapter::INT_REGULAR])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addForeignKey('id_jurusan', 'jurusan', 'id_jurusan', $this->myConstraint('jurusan_rombel'))
            ->addForeignKey('id_level_kelas', 'level_kelas', 'id_level_kelas', $this->myConstraint('level_kelas_rombel'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_rombel'))
            ->save();

        $tahun_ajaran = $this->table('tahun_ajaran', ['id' => false, 'primary_key' => 'id_tahun_ajaran']);
        $tahun_ajaran->addColumn('id_tahun_ajaran', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_tahun_ajaran', 'string', ['limit' => 20])
            ->addColumn('status', 'boolean')
            ->save();

        $status_siswa = $this->table('status_siswa', ['id' => false, 'primary_key' => 'id_status_siswa']);
        $status_siswa->addColumn('id_status_siswa', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_status_siswa', 'string', ['limit' => 10])
            ->save();

        $slot_jam = $this->table('slot_jam', ['id' => false, 'primary_key' => 'id_slot_jam']);
        $slot_jam->addColumn('id_slot_jam', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_SMALL])
            ->addColumn('nama_slot_jam', 'string', ['limit' => 15])
            ->addColumn('jam_mulai', 'time', ['null' => false])
            ->addColumn('jam_selesai', 'time', ['null' => false])
            ->save();

        $mapel = $this->table('mapel', ['id' => false, 'primary_key' => 'id_mapel']);
        $mapel->addColumn('id_mapel', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nama_mapel', 'string', ['limit' => 20])
            ->addColumn('id_jurusan', 'integer', ['limit' => MysqlAdapter::INT_SMALL])
            ->addColumn('id_level_kelas', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_jurusan', 'jurusan', 'id_jurusan', $this->myConstraint('jurusan_mapel'))
            ->addForeignKey('id_level_kelas', 'level_kelas', 'id_level_kelas', $this->myConstraint('level_kelas_mapel'))
            ->save();

        $plot_ajar = $this->table('plot_ajar', ['id' => false, 'primary_key' => 'id_plot_ajar']);
        $plot_ajar->addColumn('id_plot_ajar', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_rombel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_mapel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_rombel', 'rombel', 'id_rombel', $this->myConstraint('rombel_plot_ajar'))
            ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_plot_ajar'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_plot_ajar'))
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_plot_ajar'))
            ->save();

        $jadwal = $this->table('jadwal', ['id' => false, 'primary_key' => 'id_jadwal']);
        $jadwal->addColumn('id_jadwal', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('hari', 'string', ['limit' => 10])
            ->addColumn('id_slot_jam', 'integer', ['limit' => MysqlAdapter::INT_SMALL])
            ->addColumn('id_plot_ajar', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_slot_jam', 'slot_jam', 'id_slot_jam', $this->myConstraint('slot_jam_jadwal'))
            ->addForeignKey('id_plot_ajar', 'plot_ajar', 'id_plot_ajar', $this->myConstraint('plot_ajar_jadwal'))
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_jadwal'))
            ->save();

        $profile_gukar = $this->table('profile_gukar', ['id' => false, 'primary_key' => 'id_profile_gukar']);
        $profile_gukar->addColumn('id_profile_gukar', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nik', 'string', ['limit' => 20])
            ->addColumn('nama_gukar', 'string', ['limit' => 40])
            ->addColumn('jenis_kelamin_gukar', 'string', ['limit' => 10])
            ->addColumn('alamat_gukar', 'string', ['limit' => 255])
            ->addColumn('tempat_lahir_gukar', 'string', ['limit' => 30])
            ->addColumn('tanggal_lahir_gukar', 'date')
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('foto', 'string', ['limit' => 100])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_profile_gukar'))
            ->save();

        $profile_siswa = $this->table('profile_siswa', ['id' => false, 'primary_key' => 'id_profile_siswa']);
        $profile_siswa->addColumn('id_profile_siswa', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nis', 'string', ['limit' => 20])
            ->addColumn('nama_siswa', 'string', ['limit' => 40])
            ->addColumn('jenis_kelamin_siswa', 'string', ['limit' => 10])
            ->addColumn('alamat_siswa', 'string', ['limit' => 255])
            ->addColumn('tempat_lahir_siswa', 'string', ['limit' => 30])
            ->addColumn('tanggal_lahir_siswa', 'date')
            ->addColumn('sekolah_asal', 'string', ['limit' => 50])
            ->addColumn('alamat_sekolah_asal', 'string', ['limit' => 255])
            ->addColumn('nama_ayah', 'string', ['limit' => 40])
            ->addColumn('nama_ibu', 'string', ['limit' => 40])
            ->addColumn('alamat_orang_tua', 'string', ['limit' => 255])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('foto', 'string', ['limit' => 100])
            ->addColumn('id_rombel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_status_siswa', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addColumn('id_jurusan', 'integer', ['limit' => MysqlAdapter::INT_SMALL])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_profile_siswa'))
            ->addForeignKey('id_rombel', 'rombel', 'id_rombel', $this->myConstraint('rombel_profile_siswa'))
            ->addForeignKey('id_status_siswa', 'status_siswa', 'id_status_siswa', $this->myConstraint('status_siswa_profile_siswa'))
            ->addForeignKey('id_jurusan', 'jurusan', 'id_jurusan', $this->myConstraint('jurusan_profile_siswa'))
            ->save();

        $log_rombel = $this->table('log_rombel', ['id' => false, 'primary_key' => 'id_log_rombel']);
        $log_rombel->addColumn('id_log_rombel', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nama_rombel', 'string', ['limit' => 40])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_log_rombel'))
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_log_rombel'))
            ->save();

        //TABEL SYAFII -------------------------------------------------------
        $masapemilu = $this->table('masa_pemilu', ['id' => false, 'primary_key' => 'id_masa_pemilu']);
        $masapemilu->addColumn('id_masa_pemilu', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('waktu_mulai_pendaftaran_masa_pemilu', 'date')
            ->addColumn('waktu_selesai_pendaftaran_masa_pemilu', 'date')
            ->addColumn('waktu_mulai_pemilihan_masa_pemilu', 'date')
            ->addColumn('waktu_selesai_pemilihan_masa_pemilu', 'date')
            ->addColumn('waktu_pengumuman_masa_pemilu', 'date')
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_masa_pemilu'))
            ->save();

        $kandidat = $this->table('kandidat', ['id' => false, 'primary_key' => 'id_kandidat']);
        $kandidat->addColumn('id_kandidat', 'integer', ['identity' => true, 'limit' => 3])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('visi_kandidat', 'text')
            ->addColumn('misi_kandidat', 'text')
            ->addColumn('status_kandidat', 'string', ['limit' => 10])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_kandidat'))
            ->save();

        $pemilihan = $this->table('pemilihan', ['id' => false, 'primary_key' => 'id_pemilihan']);
        $pemilihan->addColumn('id_pemilihan', 'integer', ['identity' => true, 'limit' => 3])
            ->addColumn('id_kandidat', 'integer', ['limit' => 3])
            ->addColumn('id_masa_pemilu', 'integer', ['limit' => 3])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('waktu_pemilihan', 'datetime')
            ->addForeignKey('id_kandidat', 'kandidat', 'id_kandidat', $this->myConstraint('kandidat_pemilihan'))
            ->addForeignKey('id_masa_pemilu', 'masa_pemilu', 'id_masa_pemilu', $this->myConstraint('masapemilu_pemilihan'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_pemilihan'))
            ->save();

        //TABEL RIDHO ------------------------------------------------------------------
        $fileaudio = $this->table('file_audio', ['id' => false, 'primary_key' => 'id_file_audio']);
        $fileaudio->addColumn('id_file_audio', 'integer', ['identity' => true, 'limit' => 2])
            ->addColumn('nama_file_audio', 'string', ['limit' => 25])
            ->addColumn('lokasi_file_audio', 'string', ['limit' => 30])
            ->addColumn('ukuran_file_audio', 'integer', ['limit' => 4])
            ->save();

        $jadwalalarm = $this->table('jadwal_alarm', ['id' => false, 'primary_key' => 'id_jadwal_alarm']);
        $jadwalalarm->addColumn('id_jadwal_alarm', 'integer', ['identity' => true, 'limit' => 4])
            ->addColumn('hari_jadwal_alarm', 'string', ['limit' => 7])
            ->addColumn('jam_jadwal_alarm', 'time')
            ->addColumn('keterangan_jadwal_alarm', 'string', ['limit' => 25])
            ->addColumn('id_file_audio', 'integer', ['limit' => 2])
            ->addForeignKey('id_file_audio', 'file_audio', 'id_file_audio', $this->myConstraint('fileaudio_jadwalalarm'))
            ->save();

        //TABEL TRISNAWATI
        //TAMBAH RELASI SURAT_MASUK KE DISPOSISI
        $jenissurat = $this->table('jenis_surat', ['id' => false, 'primary_key' => 'id_jenis_surat']);
        $jenissurat->addColumn('id_jenis_surat', 'integer', ['identity' => true])
            ->addColumn('kode_jenis_surat', 'string') // mengganti type data dari integer ke string
            ->addColumn('nama_jenis_surat', 'string') // menambah kolom nama_jenis_surat
            // hapus kolom template_jenis_surat
            ->save();

        // tambah tabel template_jenis_surat
        $templatejenissurat = $this->table('template_jenis_surat', ['id' => false, 'primary_key' => 'id_template_jenis_surat']);
        $templatejenissurat->addColumn('id_template_jenis_surat', 'integer', ['identity' => true])
            ->addColumn('id_jenis_surat', 'integer')
            ->addColumn('nama_template_jenis_surat', 'string')
            ->addColumn('template_jenis_surat', 'longtext')
            ->addForeignKey('id_jenis_surat', 'jenis_surat', 'id_jenis_surat', $this->myConstraint('templatesurat_jenissurat'))
            ->save();

        // menghapus tabel kode_surat



        // tambah tabel arsip_surat
        $disposisi = $this->table('arsip_surat', ['id' => false, 'primary_key' => 'id_arsip_surat']);
        $disposisi->addColumn('id_arsip_surat', 'integer', ['identity' => true])
            ->addColumn('nama_arsip_surat', 'string')
            ->addColumn('tanggal_arsip_surat', 'datetime')
            ->addColumn('dari_tanggal_arsip_surat', 'date')
            ->addColumn('sampai_tanggal_arsip_surat', 'date')
            ->save();

        // tambah tabel arsip_surat_detail
        $disposisi = $this->table('arsip_surat_detail', ['id' => false, 'primary_key' => 'id_arsip_surat_detail']);
        $disposisi->addColumn('id_arsip_surat_detail', 'integer', ['identity' => true])
            ->addColumn('id_surat_keluar', 'integer')
            ->addColumn('id_arsip_surat', 'integer')
            ->addForeignKey('id_surat_keluar', 'surat_keluar', 'id_surat_keluar', $this->myConstraint('arsipsuratdetail_surakkeluar'))
            ->addForeignKey('id_arsip_surat', 'arsip_surat', 'id_arsip_surat', $this->myConstraint('arsipsuratdetail_arsipsurat'))
            ->save();    

        $suratkeluar = $this->table('surat_keluar', ['id' => false, 'primary_key' => 'id_surat_keluar']);
        $suratkeluar->addColumn('id_surat_keluar', 'integer', ['identity' => true])        
            ->addColumn('id_jenis_surat', 'integer')
            ->addColumn('id_arsip_surat', 'integer')
            ->addColumn('no_surat_keluar', 'string')
            // menghapus kolom id_kode_surat
            ->addColumn('tujuan_surat_keluar', 'string')
            // menambah kolom lampiran_surat_keluar
            ->addColumn('lampiran_surat_keluar', 'string')
            ->addColumn('alamat_surat_keluar', 'string')
            ->addColumn('tanggal_surat_keluar', 'date')
            ->addColumn('perihal_surat_keluar', 'string')
            ->addColumn('status_surat_keluar', 'integer') // tambah kolom status_surat_keluar
            ->addColumn('scan_surat_keluar', 'string',['null' => false])
            ->addColumn('isi_surat_keluar', 'longtext') // mengganti text ke longtext
            // menghapus foreignkey id_kode_surat
            // menambah foreignkey id_arsip_surat_keluar 
            ->addForeignKey('id_arsip_surat', 'arsip_surat', 'id_arsip_surat', $this->myConstraint('arsipsuratkeluar_suratkeluar'))
            ->addForeignKey('id_jenis_surat', 'jenis_surat', 'id_jenis_surat', $this->myConstraint('jenissurat_suratkeluar'))
            ->save();

        $suratmasuk = $this->table('surat_masuk', ['id' => false, 'primary_key' => 'id_surat_masuk']);
        $suratmasuk->addColumn('id_surat_masuk', 'integer', ['identity' => true])
            ->addColumn('no_surat_masuk', 'string')
            ->addColumn('instansi_surat_masuk', 'string')
            ->addColumn('alamat_surat_masuk', 'string')
            ->addColumn('tanggal_surat_masuk', 'date')
            // menambah kolom lampiran_surat_masuk
            ->addColumn('lampiran_surat_masuk', 'string')
            ->addColumn('perihal_surat_masuk', 'string')
            ->addColumn('scan_surat_masuk', 'string',['null' => false])
            ->addColumn('isi_surat_masuk', 'longtext') // mengganti text ke longtext
            ->save();

        // menambah tabel adisposisi
        $adisposisi = $this->table('adisposisi', ['id' => false, 'primary_key' => 'id_adisposisi']);
        $adisposisi->addColumn('id_adisposisi', 'integer', ['identity' => true])
            ->addColumn('nama_adisposisi', 'string', ['limit' => 255])
            ->save();

        
        $disposisi = $this->table('disposisi', ['id' => false, 'primary_key' => 'id_disposisi']);
        $disposisi->addColumn('id_disposisi', 'integer', ['identity' => true])
            ->addColumn('id_surat_masuk', 'integer')
            // menambah kolom id_adisposisi
            ->addColumn('id_adisposisi', 'integer')
            ->addColumn('tanggal_disposisi', 'date')
            ->addColumn('isi_disposisi', 'string')
            ->addForeignKey('id_surat_masuk', 'surat_masuk', 'id_surat_masuk', $this->myConstraint('suratmasuk_disposisi'))
            // menambah foreignkey id_adisposisi ke tabel adisposisi
            ->addForeignKey('id_adisposisi', 'adisposisi', 'id_adisposisi', $this->myConstraint('adisposisi_disposisi'))
            ->save();
    

        //TABEL USWATUN
        $jenisfile = $this->table('jenis_file', ['id' => false, 'primary_key' => 'id_jenis_file']);
        $jenisfile->addColumn('id_jenis_file', 'integer', ['identity' => true, 'limit' => 10])
            ->addColumn('nama_jenis_file', 'string', ['limit' => 15])
            ->save();

        $hakakses = $this->table('hak_akses', ['id' => false, 'primary_key' => 'id_hak_akses']);
        $hakakses->addColumn('id_hak_akses', 'integer', ['identity' => true, 'limit' => 10])
            ->addColumn('nama_hak_akses', 'string', ['limit' => 100]) // mengganti limit dari 15 ke 100
            ->save();

        // tambah tabel hak_akses_group
        $hakaksesgroup = $this->table('hak_akses_group', ['id' => false, 'primary_key' => 'hak_akses_group']);
        $hakaksesgroup->addColumn('id_hak_akses_group', 'integer', ['identity' => true, 'limit' => 10])
            ->addColumn('id_hak_akses', 'integer', ['limit' => 10])
            ->addColumn('type_hak_akses_group', 'text')
            ->addForeignKey('id_hak_akses', 'hak_akses', 'id_hak_akses', $this->myConstraint('hakakses_hakaksesgroup'))
            ->save();

        // kolom id_jenis_file dihapus
        $uploadfileberkas = $this->table('upload_file', ['id' => false, 'primary_key' => 'id_upload_file_berkas']);
        $uploadfileberkas->addColumn('id_upload_file_berkas', 'integer', ['identity' => true, 'limit' => 10])
            ->addColumn('nama_upload_file_berkas', 'string', ['limit' => 255]) // mengganti limit dari 15 ke 255
            ->addColumn('ukuran_upload_file_berkas', 'float', ['limit' => 10])
            ->addColumn('lokasi_upload_file_berkas', 'string', ['limit' => 255]) // mengganti limit dari 10 ke 255
            ->addColumn('tanggal_upload_file_berkas', 'datetime') // tambah kolom tanggal_upload_file_berkas
            ->addColumn('keterangan_upload_file_berkas', 'text') // tambah kolom keterangan_upload_file_berkas
            ->addColumn('ikon_upload_file_berkas', 'string', ['limit' => 255]) // tambah kolom ikon_uploadi_file_berkas
            ->addColumn('ekstensi_upload_file_berkas', 'string', ['limit' => 7]) // tambah kolom ekstensi_upload_file_berkas
            ->addColumn('total_download_upload_file_berkas', 'integer', ['limit' => 10]) // tambah kolom total_download_upload_file_berkas
            ->addColumn('total_lihat_upload_file_berkas', 'integer', ['limit' => 10]) // tambah kolom total_lihat_upload_file_berkas
            ->addColumn('hash_upload_file_berkas', 'str5ng', ['limit' => 100]) // tambah kolom hash_upload_file_berkas 
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_hak_akses_group', 'integer', ['limit' => 10]) // menggantik id_hak_akses ke id_hak_akses_group
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_uploadfileberkas'))

            ->addForeignKey('id_hak_akses_group', 'hak_akses_group', 'id_hak_akses_group', $this->myConstraint('hakaksesgroup_uploadfileberkas')) // mengganti foregen key dari id_hak_akses ke id_hak_akses_group
            ->save();

        //TABEL HANDI
        //KURANG TABEL MATERI EDAN UPLOAD MATERI == MATERI BERUPA FILE UNTUK DIAPLOAD ATAU GIMANA? (diibuat)
        //kurang komentar
        $quis = $this->table('quis', ['id' => false, 'primary_key' => 'id_quis']);
        $quis->addColumn('id_quis', 'integer', ['identity' => true])
            ->addColumn('id_mapel', 'integer')
            ->addColumn('waktu_quis', 'datetime',['null' => false]) //mengganti tipe data kolom dari time menjadi datetime, dan di set null menjadi true
            ->addColumn('nama_quis', 'string')
            ->addColumn('lama_quis', 'time') //mengganti nama kolom dari lama menjadi lama_quis, format dari integer ke time
            ->addColumn('durasi_quis', 'integer') //menambah kolom durasi_quis
            ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_quis'))
            ->save();

        $soal = $this->table('soal', ['id' => false, 'primary_key' => 'id_soal']);
        $soal->addColumn('id_soal', 'integer', ['identity' => true, 'limit' => 5])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_quis', 'integer')
            ->addColumn('soal', 'text')  //mengganti tipe data kolom dari string menjadi text
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_soal'))
            ->addForeignKey('id_quis', 'quis', 'id_quis', $this->myConstraint('quis_soal'))
            ->save();

        $pilihanjawaban = $this->table('pilihan_jawaban', ['id' => false, 'primary_key' => 'id_pilihan_jawaban']);
        $pilihanjawaban->addColumn('id_pilihan_jawaban', 'integer', ['identity' => true])
            ->addColumn('id_soal', 'integer', ['limit' => 5])
            ->addColumn('text_pilihan_jawaban', 'string')
            ->addForeignKey('id_soal', 'soal', 'id_soal', $this->myConstraint('soal_pilihanjawaban'))
            ->save();

        $kuncijawaban = $this->table('kunci_jawaban', ['id' => false, 'primary_key' => 'id_kunci_jawaban']);
        $kuncijawaban->addColumn('id_kunci_jawaban', 'integer', ['identity' => true])
            ->addColumn('id_soal', 'integer', ['limit' => 5])
            ->addColumn('id_pilihan_jawaban', 'integer')
            ->addForeignKey('id_soal', 'soal', 'id_soal', $this->myConstraint('soal_kuncijawaban'))
            ->addForeignKey('id_pilihan_jawaban', 'pilihan_jawaban', 'id_pilihan_jawaban', $this->myConstraint('pilihanjawaban_kuncijawaban'))
            ->save();

        $jawabansiswa = $this->table('jawaban_siswa', ['id' => false, 'primary_key' => 'id_jawaban_siswa']);
        $jawabansiswa->addColumn('id_jawaban_siswa', 'integer', ['identity' => true])
            ->addColumn('id_soal', 'integer', ['limit' => 5])
            ->addColumn('id_pilihan_jawaban', 'integer')
            // menambah kolom id_user
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('status_jawaban_siswa', 'boolean')
            // menambah foreignkey user_jawabansiswa
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_jawabansiswa'))
            ->addForeignKey('id_soal', 'soal', 'id_soal', $this->myConstraint('soal_jawabansiswa'))
            ->addForeignKey('id_pilihan_jawaban', 'pilihan_jawaban', 'id_pilihan_jawaban', $this->myConstraint('pilihanjawaban_jawabansiswa'))
            ->save();

        $nilaiquis = $this->table('nilai_quis', ['id' => false, 'primary_key' => 'id_nilai_quis']);
        $nilaiquis->addColumn('id_nilai_quis', 'integer', ['identity' => true])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('nilai_nilai_quis', 'float') // mengubah type integer ke float
            ->addColumn('id_quis', 'integer')
            ->addColumn('status_nilai_quis', 'integer') // tambah kolom status_nilai_quis
            ->addColumn('tanggal_quis', 'datetime') // tambah kolom tanggal_quis
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_nilaiquis'))
            ->addForeignKey('id_quis', 'quis', 'id_quis', $this->myConstraint('quis_nilaiquis'))
            ->save();

        $materi = $this->table('materi', ['id' => false, 'primary_key' => 'id_materi']);
        $materi->addColumn('id_materi', 'integer', ['identity' => true, 'limit' => 5])
            ->addColumn('id_rombel', 'integer')
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_mapel', 'integer')
            ->addColumn('tanggal_materi', 'datetime') // tambah kolom tanggal_materi
            ->addColumn('nama_materi', 'string')
            ->addColumn('isi_materi', 'text')
            ->addColumn('file_materi', 'string',['null'=>true]) // set null menjadi true
            ->addColumn('status_materi', 'integer') // tambah kolom status_materi
            ->addForeignKey('id_rombel', 'rombel', 'id_rombel', $this->myConstraint('rombel_materi'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_materi'))
            ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_materi'))
            ->save();

        /*********************hapus table upload_materi*************************/

        // $uploadmateri = $this->table('upload_materi', ['id' => false, 'primary_key' => 'id_upload_materi']);
        // $uploadmateri->addColumn('id_upload_materi', 'integer', ['identity' => true, 'limit' => 5])
        //     ->addColumn('id_mapel', 'integer')
        //     ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
        //     ->addColumn('file_upload_materi', 'string')
        //     ->addColumn('judul_upload_materi', 'string')
        //     ->addColumn('keterangan_upload_materi', 'string')
        //     ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_uploadmateri'))
        //     ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_uploadmateri'))
        //     ->save();

        /***********************************************************************/

        $komentar = $this->table('komentar', ['id' => false, 'primary_key' => 'id_komentar']);
        $komentar->addColumn('id_komentar', 'integer', ['identity' => true, 'limit' => 5])
            ->addColumn('id_materi', 'integer') // id_mapel diganti menjadi id_materi
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            // menghapus kolom file_komentar, judul_komentar
            ->addColumn('isi_komentar', 'string', ['limit' => 160]) // mengganti nama kolom dari keterangan_komentar ke isi_komentar dan mengganti type string ke text
            // menambah kolom tanggal_komentar
            ->addColumn('tanggal_komentar', 'datetime')
            ->addForeignKey('id_materi', 'materi', 'id_materi', $this->myConstraint('materi_komentar'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_komentar'))
            ->save();

        //AGUS EFENDI
        $paket = $this->table('paket', ['id' => false, 'primary_key' => 'id_paket']);
        $paket->addColumn('id_paket', 'integer', ['identity' => true])
            ->addColumn('nama_paket', 'string', ['limit' => 255]) // mengganti limit dari 18 ke 255
            ->addColumn('maks_soal_paket', 'integer')
            ->save();

        $soaluas = $this->table('soal_uas', ['id' => false, 'primary_key' => 'id_soal_uas']);
        $soaluas->addColumn('id_soal_uas', 'integer', ['identity' => true])
            ->addColumn('id_paket', 'integer')
            ->addColumn('teks_soal_uas', 'text')
            ->addForeignKey('id_paket', 'paket', 'id_paket', $this->myConstraint('paket_soaluas'))
            ->save();

        $jawabanuas = $this->table('jawaban_uas', ['id' => false, 'primary_key' => 'id_jawaban_uas']);
        $jawabanuas->addColumn('id_jawaban_uas', 'integer', ['identity' => true])
            ->addColumn('id_soal_uas', 'integer')
            ->addColumn('teks_jawaban_uas', 'text')
            ->addColumn('benar_jawaban_uas', 'integer')
            ->addForeignKey('id_soal_uas', 'soal_uas', 'id_soal_uas', $this->myConstraint('soal_jawabanuas'))
            ->save();

        $jadwaluas = $this->table('jadwal_uas', ['id' => false, 'primary_key' => 'id_jadwal_uas']);
        $jadwaluas->addColumn('id_jadwal_uas', 'integer', ['identity' => true])
            ->addColumn('id_mapel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_rombel', 'integer')
            ->addColumn('id_paket', 'integer')
            ->addColumn('id_tahun_ajaran', 'integer')
            ->addColumn('nama_jadwal_uas', 'string', ['limit' => 32])
            ->addColumn('tanggal_jadwal_uas', 'datetime')
            ->addColumn('durasi_jadwal_uas', 'time')
            ->addColumn('status_jadwal_uas', 'integer')
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahunajaran_jadwaluas'))
            ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_jadwaluas'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_jadwaluas'))
            ->addForeignKey('id_rombel', 'rombel', 'id_rombel', $this->myConstraint('rombel_jadwaluas'))
            ->addForeignKey('id_paket', 'paket', 'id_paket', $this->myConstraint('paket_jadwaluas'))
            ->save();

        // tambah tabel jadwal_susulan_uas
        $jawabanuassiswa = $this->table('jadwal_susulan_uas', ['id' => false, 'primary_key' => 'id_jadwal_susulan_uas']);
        $jawabanuassiswa->addColumn('id_jadwal_susulan_uas', 'integer', ['identity' => true])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_jadwal_uas', 'integer')
            ->addColumn('tanggal_jadwal_susulan_uas', 'datetime')
            ->addColumn('status_jadwal_susulan_uas', 'boolean')
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_jadwalsusulanuas'))
            ->addForeignKey('id_jadwal_uas', 'jadwal_uas', 'id_jadwal_uas', $this->myConstraint('jadwaluas_jadwalsusulanuas'))
            ->save();

        $jawabanuassiswa = $this->table('jawaban_uas_siswa', ['id' => false, 'primary_key' => 'id_jawaban_uas_siswa']);
        $jawabanuassiswa->addColumn('id_jawaban_uas_siswa', 'integer', ['identity' => true])
            ->addColumn('id_soal_uas', 'integer')
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_jadwal_uas', 'integer')
            ->addColumn('id_jawaban_uas', 'integer')
            ->addForeignKey('id_soal_uas', 'soal_uas', 'id_soal_uas', $this->myConstraint('soal_jawabanuassiswa'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_jawabanuassiswa'))
            ->addForeignKey('id_jadwal_uas', 'jadwal_uas', 'id_jadwal_uas', $this->myConstraint('jadwaluas_jawabanuassiswa'))
            ->addForeignKey('id_jawaban_uas', 'jawaban_uas', 'id_jawaban_uas', $this->myConstraint('jawabanuas_jawabanuassiswa'))
            ->save();

        $nilaiuas = $this->table('nilai_uas', ['id' => false, 'primary_key' => 'id_nilai_uas']);
        $nilaiuas->addColumn('id_nilai_uas', 'integer', ['identity' => true])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_jadwal_uas', 'integer')
            ->addColumn('total_soal_nilai_uas', 'integer',['null'=>true])
            ->addColumn('total_dijawab_nilai_uas', 'integer',['null'=>true])
            ->addColumn('total_benar_nilai_uas', 'integer',['null'=>true])
            ->addColumn('nilai_huruf_nilai_uas', 'string', ['limit' => 1,'null' => true])
            ->addColumn('nilai_angka_nilai_uas', 'fload',['null'=>true]) // mengubah tipe data integer ke float
            ->addColumn('total_benar_nilai_uas', 'integer',['null'=>true])
            // tambah kolom tanggal_mulai_ujian
            ->addColumn('tanggal_mulai_ujian', 'datetime',['null'=>true])
            // tambah kolom tanggal_selesai_ujian
            ->addColumn('tanggal_selesai_ujian', 'datetime',['null'=>true])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_nilaiuas'))
            ->addForeignKey('id_jadwal_uas', 'jadwal_uas', 'id_jadwal_uas', $this->myConstraint('jadwaluas_nilaiuas'))
            ->save();

        //FAJAR
        $suratbk = $this->table('surat_bk', ['id' => false, 'primary_key' => 'id_surat_bk']);
        $suratbk->addColumn('id_surat_bk', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('surat_surat_bk', 'string', ['limit' => 32])
            ->addColumn('tipe_surat_bk', 'string', ['limit' => 20])
            ->addColumn('isi_surat_bk', 'longtext') // menambahkan kolom isi_surat_bk
            ->save();

        $pengaturan = $this->table('pengaturan', ['id' => false, 'primary_key' => 'label_pengaturan']);
        $pengaturan->addColumn('label_pengaturan', 'string', ['limit' => 32])
            ->addColumn('value_pengaturan', 'text')
            ->save();

        $pelanggaran = $this->table('pelanggaran', ['id' => false, 'primary_key' => 'id_pelanggaran']);
        $pelanggaran->addColumn('id_pelanggaran', 'integer', ['identity' => true])
            ->addColumn('pelanggaran_pelanggaran', 'string', ['limit' => 255])
            ->addColumn('poin_pelanggaran', 'float')
            ->save();

        $sanksi = $this->table('sanksi', ['id' => false, 'primary_key' => 'id_sanksi']);
        $sanksi->addColumn('id_sanksi', 'integer', ['identity' => true])
            ->addColumn('sanksi_sanksi', 'string', ['limit' => 255])
            ->addColumn('max_point_sanksi', 'float')
            ->addColumn('min_point_sanksi', 'float')
            ->save();

        $pelanggaransiswa = $this->table('pelanggaran_siswa', ['id' => false, 'primary_key' => 'id_pelanggaran_siswa']);
        $pelanggaransiswa->addColumn('id_pelanggaran_siswa', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('view_pelanggaran_siswa', 'text')
            ->addColumn('view_sanksi_pelanggaran_siswa', 'text')
            ->addColumn('view_surat_pelanggaran_siswa', 'longtext',['null' => true]) // mengganti tipe data text ke longtext dan set null true
            ->addColumn('apoint_pelanggaran_siswa', 'float')
            ->addColumn('tgl_pelanggaran_siswa', 'datetime')
            ->addColumn('log_pelanggaran_siswa', 'text') // menambah kolom log untuk mencatat waktu dan tempat kejadian pelanggaran
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_pelanggaransiswa'))
            ->save();

        // penambahan tabel perizinan_siswa
        $pelanggaransiswa = $this->table('perizinan_siswa', ['id' => false, 'primary_key' => 'id_perizinan_siswa']);
        $pelanggaransiswa->addColumn('id_perizinan_siswa', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('keterangan_perizinan_siswa', 'text')
            ->addColumn('view_surat_perizinan_siswa', 'longtext')
            ->addColumn('tgl_perizinan_siswa', 'datetime')
            ->addColumn('kembali_perizinan_siswa', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('status_kembali_perizinan_siswa', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('log_perizinan_siswa', 'text')
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_perizinansiswa'))
            ->save();

        //AMIN
        $biaya = $this->table('biaya', ['id' => false, 'primary_key' => 'id_biaya']);
        $biaya->addColumn('id_biaya', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_biaya', 'string', ['limit' => 20])
            ->addColumn('nilai_biaya', 'integer')
            ->save();

        $tagihan = $this->table('tagihan', ['id' => false, 'primary_key' => 'id_tagihan']);
        $tagihan->addColumn('id_tagihan', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addColumn('nama_tagihan', 'string', ['limit' => 20])
            ->addColumn('total_tagihan', 'integer')
            ->addColumn('sisa_tunggakan', 'integer')
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_tagihan'))
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_tagihan'))
            ->save();

        $pilih_biaya = $this->table('pilih_biaya', ['id' => false, 'primary_key' => 'id_pilih_biaya']);
        $pilih_biaya->addColumn('id_pilih_biaya', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('id_tagihan', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addColumn('id_biaya', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_biaya', 'biaya', 'id_biaya', $this->myConstraint('biaya_pilih'))
            ->addForeignKey('id_tagihan', 'tagihan', 'id_tagihan', $this->myConstraint('tagihan_pilih'))
            ->save();

        $pembayaran = $this->table('pembayaran', ['id' => false, 'primary_key' => 'id_pembayaran']);
        $pembayaran->addColumn('id_pembayaran', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
            ->addColumn('jumlah_pembayaran', 'integer')
            ->addColumn('waktu_pembayaran', 'datetime')
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tagihan', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_pembayaran'))
            ->addForeignKey('id_tagihan', 'tagihan', 'id_tagihan', $this->myConstraint('tagihan_pembayaran'))
            ->save();

        //INA
        $raport = $this->table('raport', ['id' => false, 'primary_key' => 'id_raport']);
        $raport->addColumn('id_raport', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_rombel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_user', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
            ->addColumn('deskripsi_raport', 'text')
            ->addColumn('catatan_walikelas_raport', 'text')
            ->addColumn('catatan_orangtua_raport', 'text')
            ->addColumn('sakit_raport', 'integer', ['limit' => 11])
            ->addColumn('ijin_raport', 'integer', ['limit' => 11])
            ->addColumn('alpa_raport', 'integer', ['limit' => 11])
            ->addColumn('kelakuan_raport', 'string', ['limit' => 1])
            ->addColumn('kerajian_raport', 'string', ['limit' => 1])
            ->addColumn('kerapian_raport', 'string', ['limit' => 1])
            ->addForeignKey('id_rombel', 'rombel', 'id_rombel', $this->myConstraint('rombel_raport'))
            ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('user_raport'))
            ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_raport'))
            ->save();

        $nilai_raport = $this->table('nilai_raport', ['id' => false, 'primary_key' => 'id_nilai_raport']);
        $nilai_raport->addColumn('id_nilai_raport', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_raport', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('id_mapel', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('kkm_nilai_raport', 'integer',['limit' => 11])
            ->addColumn('jenis_nilai_raport', 'string', ['limit' => 15])
            ->addColumn('nilai_nilai_raport', 'float')
            ->addColumn('deskripsi_nilai_raport', 'text')
            ->addForeignKey('id_raport', 'raport', 'id_raport', $this->myConstraint('raport_nilai_raport'))
            ->addForeignKey('id_mapel', 'mapel', 'id_mapel', $this->myConstraint('mapel_nilai_raport'))
            ->save();

        $raport_kp = $this->table('raport_kp', ['id' => false, 'primary_key' => 'id_raport_kp']);
        $raport_kp->addColumn('id_raport_kp', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_raport', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('mitra_raport_kp', 'string')
            ->addColumn('lokasi_raport_kp', 'string')
            ->addColumn('lama_raport_kp', 'integer', ['limit' => 11])
            ->addColumn('keterangan_raport_kp', 'string')
            ->addForeignKey('id_raport', 'raport', 'id_raport', $this->myConstraint('raport_raport_kp'))
            ->save();

        $raport_ekstr = $this->table('raport_ekstr', ['id' => false, 'primary_key' => 'id_raport_ekstr']);
        $raport_ekstr->addColumn('id_raport_ekstr', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_raport', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('kegiatan_raport_ekstr', 'string')
            ->addColumn('keterangan_raport_ekstr', 'string')
            ->addColumn('predikat_raport_ekstr', 'string', ['limit' => 10])
            ->addForeignKey('id_raport', 'raport', 'id_raport', $this->myConstraint('raport_raport_ekstr'))
            ->save();

        $raport_prestasi = $this->table('raport_prestasi', ['id' => false, 'primary_key' => 'id_raport_prestasi']);
        $raport_prestasi->addColumn('id_raport_prestasi', 'integer', ['identity' => true, 'limit' => 11])
            ->addColumn('id_raport', 'integer', ['limit' => MysqlAdapter::INT_REGULAR])
            ->addColumn('jenis_prestasi', 'string')
            ->addColumn('keterangan_prestasi', 'string')
            ->addForeignKey('id_raport', 'raport', 'id_raport', $this->myConstraint('raport_raport_prestasi'))
            ->save();
    }

    private function myConstraint($constraint)
    {
        return ['delete' => 'RESTRICT', 'update' => 'CASCADE', 'constraint' => $constraint];
    }
}
