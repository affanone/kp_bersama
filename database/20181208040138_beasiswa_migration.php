<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class BeasiswaMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //TABEL HOSARI ---------------------------------------------------------- 
        $kategori_beasiswa = $this->table('kategori_beasiswa', ['id' => false, 'primary_key' => 'id_kategori_beasiswa']);
        $kategori_beasiswa->addColumn('id_kategori_beasiswa', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
                ->addColumn('nama_kategori_beasiswa', 'string', ['limit' => 20 ])
                ->addColumn('id_tahun_ajaran', 'integer', ['limit' => MysqlAdapter::INT_TINY])
                ->addForeignKey('id_tahun_ajaran', 'tahun_ajaran', 'id_tahun_ajaran', $this->myConstraint('tahun_ajaran_kategori_beasiswa'))
                ->save();

        $kriteria_beasiswa = $this->table('kriteria_beasiswa', ['id' => false, 'primary_key' => 'id_kriteria_beasiswa']);
        $kriteria_beasiswa->addColumn('id_kriteria_beasiswa', 'integer',['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
                ->addColumn('id_kategori_beasiswa', 'integer', ['limit' => MysqlAdapter::INT_TINY])
                ->addForeignKey('id_kategori_beasiswa', 'kategori_beasiswa', 'id_kategori_beasiswa', $this->myConstraint('kategori_beasiswa_kriteria_beasiswa'))
                ->addColumn('nama_kriteria_beasiswa', 'string', ['limit' => 255]) // mengganti limit dari 20 ke 255
                ->addColumn('bobot_kriteria_beasiswa', 'integer', ['limit' => MysqlAdapter::INT_TINY])
                ->save();
				
	$peserta_beasiswa = $this->table('peserta_beasiswa', ['id' => false, 'primary_key' => 'id_peserta_beasiswa']);
        $peserta_beasiswa->addColumn('id_peserta_beasiswa', 'integer', ['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
                ->addColumn('id_user', 'integer',['limit' => MysqlAdapter::INT_REGULAR])
                ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('pesertabeasiswaiduser'))
                ->save();

        $tbl_nilai_kriteria = $this->table('tbl_nilai_kriteria', ['id' => false, 'primary_key' => 'id_nilai_kriteria']);
        $tbl_nilai_kriteria->addColumn('id_nilai_kriteria', 'integer',['identity' => true, 'limit' => MysqlAdapter::INT_TINY])
                ->addColumn('id_kriteria_beasiswa', 'integer', ['limit' => true, 'limit' => MysqlAdapter::INT_TINY])
                ->addForeignKey('id_kriteria_beasiswa', 'kriteria_beasiswa', 'id_kriteria_beasiswa', $this->myConstraint('kriteria_beasiswa_nilai_kriteria'))
                ->addColumn('nilai_kriteria', 'integer', ['limit' => MysqlAdapter::INT_TINY])
                ->addColumn('nilai_normalisasi', 'integer', ['limit' => MysqlAdapter::INT_TINY])
                ->addColumn('id_user', 'integer',['limit' => MysqlAdapter::INT_REGULAR])// menggati nama kolom dari id_peserta_beasiswa menjadi id_user
                ->addColumn('status_beasiswa', 'boolean')
                ->addForeignKey('id_peserta_beasiswa', 'peserta_beasiswa', 'id_peserta_beasiswa', $this->myConstraint('peserta_beasiswa_nilai_kriteria'))
                ->addForeignKey('id_user', 'user', 'id_user', $this->myConstraint('nilaibeasiswaiduser'))
                ->save();
    }

    private function myConstraint($constraint) 
    {
        return ['delete' => 'RESTRICT', 'update' => 'CASCADE', 'constraint' => $constraint];
    }
}
