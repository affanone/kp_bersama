<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::options('{a?}/{b?}/{c?}/{d?}/{e?}/{f?}/{g?}/{h?}', function ($page) {
	header('Access-Control-Allow-Headers: *');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: *');
});

// Route::get('coba/{username}/{password}', function($username,$password){
// 	if (Auth::attempt(['email' => $username, 'password' => $password])) {
// 		$user    = Auth::user();
// 		return json_encode($user);
// 	}else{
// 		return 'salah';
// 	}
// 	//return bcrypt($tes);
// });

Route::get('password/{password}', function($password){
	// $a = explode(' ', 'nama saya affan');
	// return json_encode(array_search('affans', $a));
	return bcrypt($password);
});


Route::prefix('bk')->group(function () {
	Route::post('', 'Bk\LoginC@show');
	Route::prefix('pelanggaran')->group(function () {
		Route::get('', 'Bk\PelanggaranC@index');
		Route::get('view', 'Bk\PelanggaranC@view');
		Route::post('', 'Bk\PelanggaranC@store');
		Route::post('rekap', 'Bk\PelanggaranC@rekap');
		Route::put('', 'Bk\PelanggaranC@update');
		Route::delete('', 'Bk\PelanggaranC@destroy');
		Route::get('cari', 'Bk\SetpelanggarC@cari');
		Route::prefix('set')->group(function () {
			Route::get('', 'Bk\SetpelanggarC@index');
			Route::post('', 'Bk\SetpelanggarC@store');
			Route::put('', 'Bk\SetpelanggarC@update');
			Route::delete('', 'Bk\SetpelanggarC@destroy');
		});
	});
	Route::prefix('sanksi')->group(function () {
		Route::get('', 'Bk\SanksiC@index');
		Route::post('', 'Bk\SanksiC@store');
		Route::put('', 'Bk\SanksiC@update');
		Route::delete('', 'Bk\SanksiC@destroy');
	});
	Route::prefix('surat')->group(function () {
		Route::get('', 'Bk\SuratC@index');
		Route::post('', 'Bk\SuratC@store');
		Route::put('', 'Bk\SuratC@update');
		Route::delete('', 'Bk\SuratC@destroy');
	});
	Route::prefix('env')->group(function () {
		Route::get('', 'Bk\EnvirontC@index');
		Route::post('', 'Bk\EnvirontC@store');
		Route::put('', 'Bk\EnvirontC@update');
		Route::delete('', 'Bk\EnvirontC@destroy');
	});
	Route::prefix('izin')->group(function () {
		Route::get('cari', 'Bk\SetperizinanC@cari');
		Route::get('', 'Bk\SetperizinanC@index');
		Route::post('', 'Bk\SetperizinanC@store');
		Route::put('', 'Bk\SetperizinanC@update');
		Route::delete('', 'Bk\SetperizinanC@destroy');
	});
});

Route::prefix('uas')->group(function () {
	Route::post('', 'Uas\Login@show');
	Route::prefix('paket')->group(function () {
		Route::get('', 'Uas\Paket@index');
		Route::post('', 'Uas\Paket@store');
		Route::put('', 'Uas\Paket@update');
		Route::delete('', 'Uas\Paket@destroy');
		Route::prefix('detail')->group(function () {
			Route::get('', 'Uas\Soal@index');
			Route::post('', 'Uas\Soal@store');
			Route::put('', 'Uas\Soal@update');
			Route::delete('', 'Uas\Soal@destroy');
		});
	});
	Route::prefix('jadwal')->group(function () {
		Route::get('', 'Uas\Jadwal@index');
		Route::post('', 'Uas\Jadwal@store');
		Route::post('status', 'Uas\Jadwal@state');
		Route::put('', 'Uas\Jadwal@update');
		Route::delete('', 'Uas\Jadwal@destroy');
	});
	Route::prefix('nilai')->group(function () {
		Route::get('', 'Uas\Nilai@index');
		Route::post('', 'Uas\Nilai@store');
		Route::post('status', 'Uas\Nilai@state');
		Route::put('', 'Uas\Nilai@update');
		Route::delete('', 'Uas\Nilai@destroy');
	});
	Route::prefix('guru')->group(function () {
		Route::prefix('jadwal')->group(function () {
			Route::get('', 'Uas\Jadwal@guru');
		});
		Route::prefix('nilai')->group(function () {
			Route::get('', 'Uas\Nilai@hasil_list');
		});
		Route::prefix('susulan')->group(function () {
			Route::get('', 'Uas\Nilai@susulan');
			Route::post('', 'Uas\Nilai@susulan_terapkan');
			Route::delete('', 'Uas\Nilai@susulan_delete');
		});
	});
	Route::prefix('siswa')->group(function () {
		Route::prefix('jadwal')->group(function () {
			Route::get('', 'Uas\Jadwal@siswa');
		});
		Route::get('soal', 'Uas\Soal@siswa');
		Route::post('soal/jawab', 'Uas\Soal@siswa_jawab');
		Route::post('soal/berhenti', 'Uas\Soal@berhenti');
		Route::get('hasil', 'Uas\Nilai@hasil');
		Route::get('nilai', 'Uas\Nilai@log_ujian');
	});
	Route::prefix('data')->group(function () {
		Route::get('mapel','Uas\Data@mapel');
		Route::get('mapel/guru','Uas\Data@mapel_guru');
		Route::get('guru','Uas\Data@guru');
		Route::get('kelas','Uas\Data@kelas');
		Route::get('kelas/guru','Uas\Data@kelas_guru');
		Route::get('kelas/level','Uas\Data@level_kelas');
		Route::get('jurusan','Uas\Data@jurusan');
		Route::get('paket','Uas\Data@paket');
		Route::get('tajaran','Uas\Data@tajaran');
		Route::get('tajaran/guru','Uas\Data@tajaran_guru');
	});
});

Route::prefix('drive')->group(function () {
	Route::post('', 'Drive\Login@show');
	Route::get('semua', 'Drive\File@index');
	Route::get('bersama', 'Drive\File@bersama');
	Route::get('pribadi', 'Drive\File@pribadi');
	Route::get('manajer', 'Drive\File@manajer');
	Route::delete('manajer', 'Drive\File@destroy');
	Route::post('upload', 'Drive\File@store');
	Route::post('update', 'Drive\File@update');
	Route::get('download', 'Drive\File@download');
	Route::get('detail', 'Drive\File@lihat');
	Route::get('file', 'Drive\File@show');
	Route::get('ekstensi', 'Drive\Ekstensi@index');
	Route::post('ekstensi', 'Drive\Ekstensi@store');
	Route::delete('ekstensi', 'Drive\Ekstensi@destroy');
	Route::prefix('data')->group(function () {
		Route::get('akses','Drive\Data@hak_akses');
		Route::get('format','Drive\Data@format');
		Route::get('user','Drive\Data@user');
		Route::get('filter','Drive\Data@filter');
	});	
});	

Route::prefix('beasiswa')->group(function () {
	Route::post('', 'Beasiswa\Login@show');
	Route::prefix('kategori')->group(function () {
		Route::get('', 'Beasiswa\Kategori@index');
		Route::post('', 'Beasiswa\Kategori@store');
		Route::put('', 'Beasiswa\Kategori@update');
		Route::delete('', 'Beasiswa\Kategori@destroy');
	});
	Route::prefix('kriteria')->group(function () {
		Route::get('', 'Beasiswa\Kriteria@index');
		Route::post('', 'Beasiswa\Kriteria@store');
		Route::put('', 'Beasiswa\Kriteria@update');
		Route::delete('', 'Beasiswa\Kriteria@destroy');
	});
	Route::prefix('nilai')->group(function () {
		Route::get('', 'Beasiswa\Nilai@index');
		Route::get('detail', 'Beasiswa\Nilai@detail');
		Route::get('cetak', 'Beasiswa\Nilai@semua');
		Route::post('', 'Beasiswa\Nilai@store');
		Route::post('status', 'Beasiswa\Nilai@status');
		Route::put('', 'Beasiswa\Nilai@update');
		Route::delete('', 'Beasiswa\Nilai@destroy');
	});
	Route::prefix('peserta')->group(function () {
		Route::get('', 'Beasiswa\Peserta@index');
		Route::get('kelas', 'Beasiswa\Peserta@kelas');
		Route::post('', 'Beasiswa\Peserta@store');
		Route::delete('', 'Beasiswa\Peserta@destroy');
	});
	Route::get('tahun', 'Beasiswa\Data@tahun');
});

Route::prefix('surat')->group(function () {
	Route::post('', 'Surat\Login@show'); //login
	Route::get('data', 'Surat\Data@index');
	Route::prefix('kode')->group(function () {
		Route::get('', 'Surat\Kode@index');
		Route::post('', 'Surat\Kode@store');
		Route::put('', 'Surat\Kode@update');
		Route::delete('', 'Surat\Kode@destroy');
	});
	Route::prefix('jenis')->group(function () {
		Route::get('', 'Surat\Jenis@index');
		Route::get('template', 'Surat\Jenis@template');
		Route::post('', 'Surat\Jenis@store');
		Route::put('', 'Surat\Jenis@update');
		Route::delete('', 'Surat\Jenis@destroy');
	});
	Route::prefix('keluar')->group(function () {
		Route::get('', 'Surat\Keluar@index');
		Route::post('', 'Surat\Keluar@store');
		Route::post('edit', 'Surat\Keluar@update');
		Route::post('status', 'Surat\Keluar@status');
		Route::put('edit', 'Surat\Keluar@update'); // <--- beri 'edit'
		Route::delete('', 'Surat\Keluar@destroy');
		Route::get('jenis', 'Surat\Keluar@jenis');
		Route::get('download', 'Surat\Keluar@download');
	});
	Route::prefix('masuk')->group(function () {
		Route::get('', 'Surat\Masuk@index');
		Route::post('', 'Surat\Masuk@store');
		Route::post('edit', 'Surat\Masuk@update');
		Route::delete('', 'Surat\Masuk@destroy');
		Route::get('disposisi', 'Surat\Disposisi@masuk');
		Route::get('download', 'Surat\Masuk@download');
	});
	Route::prefix('adisposisi')->group(function () {
		Route::get('', 'Surat\Adisposisi@index');
		Route::post('', 'Surat\Adisposisi@store');
		Route::put('', 'Surat\Adisposisi@update');
		Route::delete('', 'Surat\Adisposisi@destroy');
	});
	Route::prefix('disposisi')->group(function () {
		Route::get('', 'Surat\Disposisi@index');
		Route::get('rincian', 'Surat\Disposisi@rincian');
		Route::post('', 'Surat\Disposisi@store');
		Route::put('', 'Surat\Disposisi@update');
		Route::delete('', 'Surat\Disposisi@destroy');
	});
	Route::prefix('arsip')->group(function () {
		Route::get('', 'Surat\Arsip@index');
		Route::get('detail', 'Surat\Arsip@detail');
		Route::post('', 'Surat\Keluar@arsip');
		Route::delete('', 'Surat\Arsip@destroy');
	});
});

Route::prefix('learning')->group(function () {
	Route::get('', 'Learning\Login@show'); //login
	Route::get('data', 'Learning\Data@index');
	
	Route::prefix('room')->group(function () {
		Route::get('', 'Learning\Room@index');
		Route::get('info', 'Learning\Quis@info');
		Route::get('aktif', 'Learning\Room@aktif');
		Route::post('', 'Learning\Quis@store');
		Route::put('', 'Learning\Quis@update');
		Route::delete('', 'Learning\Quis@destroy');
	});

	Route::prefix('materi')->group(function () {
		Route::get('', 'Learning\Materi@index');
		Route::get('siswa', 'Learning\Materi@siswa');
		Route::get('room/subjek', 'Learning\Materi@subject');
		Route::post('', 'Learning\Materi@store');
		Route::post('status', 'Learning\Materi@status');
		Route::post('update', 'Learning\Materi@update');
		Route::delete('', 'Learning\Materi@destroy');
	});

	Route::prefix('quis')->group(function () {
		Route::get('', 'Learning\Quis@index');
		Route::get('siswa', 'Learning\Quis@cek_quis');
		Route::get('info', 'Learning\Quis@info');
		Route::get('nilai', 'Learning\Quis@nilai');
		Route::get('berhenti', 'Learning\Quis@berhenti');
		Route::post('', 'Learning\Quis@store');
		Route::post('mulai', 'Learning\Quis@start');
		Route::post('selesai', 'Learning\Quis@end');
		Route::put('', 'Learning\Quis@update');
		Route::delete('', 'Learning\Quis@destroy');
	});

	Route::prefix('soal')->group(function () {
		Route::get('', 'Learning\Soal@index');
		Route::get('siswa', 'Learning\Soal@siswa');
		Route::post('', 'Learning\Soal@store');
		Route::put('', 'Learning\Soal@update');
		Route::delete('', 'Learning\Soal@destroy');
	});

	Route::prefix('pilihan')->group(function () {
		Route::get('', 'Learning\Pilihan@index');
		Route::post('', 'Learning\Pilihan@store');
		Route::put('', 'Learning\Pilihan@update');
		Route::delete('', 'Learning\Pilihan@destroy');
	});

	Route::prefix('kunci')->group(function () {
		Route::get('', 'Learning\Kunci@index');
		Route::post('', 'Learning\Kunci@store');
		Route::put('', 'Learning\Kunci@update');
		Route::delete('', 'Learning\Kunci@destroy');
	});

	Route::prefix('jawaban')->group(function () {
		Route::get('', 'Learning\Jawaban@index');
		Route::post('', 'Learning\Jawaban@store');
		Route::put('', 'Learning\Jawaban@update');
		Route::delete('', 'Learning\Jawaban@destroy');
	});

	Route::prefix('komentar')->group(function () {
		Route::get('', 'Learning\Komentar@index');
		Route::post('', 'Learning\Komentar@store');
		Route::delete('', 'Learning\Komentar@destroy');
	});

	Route::get('download', 'Learning\Materi@download');
});

Route::prefix('raport')->group(function () {
	Route::post('', 'Raport\Login@show');
	Route::prefix('walikelas')->group(function () {
		Route::get('kelas', 'Raport\Nilai@kelas');
		Route::get('mapel', 'Raport\Nilai@mapel');
		Route::get('nilai', 'Raport\Nilai@nilai_siswa');
		Route::post('nilai/pkl', 'Raport\Nilai@insert_pkl');
		Route::delete('nilai/pkl', 'Raport\Nilai@delete_pkl');
		Route::post('nilai/ekstrakurikuler', 'Raport\Nilai@insert_ekstrakurikuler');
		Route::delete('nilai/ekstrakurikuler', 'Raport\Nilai@delete_ekstrakurikuler');
		Route::post('nilai/prestasi', 'Raport\Nilai@insert_prestasi');
		Route::delete('nilai/prestasi', 'Raport\Nilai@delete_prestasi');
		Route::post('nilai', 'Raport\Nilai@store');
		Route::post('nilai/deskripsi', 'Raport\Nilai@deskripsi');
		Route::post('nilai/ketidakhadiran', 'Raport\Nilai@ketidakhadiran');
		Route::post('nilai/catatan/wali', 'Raport\Nilai@catatan_wali');
		Route::post('nilai/catatan/ortu', 'Raport\Nilai@catatan_ortu');
		Route::get('siswa', 'Raport\Nilai@siswa');
	});
	Route::prefix('guru')->group(function () {
		Route::get('kelas', 'Raport\Nilai@guru_kelas');
		Route::get('mapel', 'Raport\Nilai@guru_mapel');
		Route::get('nilai', 'Raport\Nilai@guru_nilai');
		Route::post('nilai', 'Raport\Nilai@guru_nilai_simpan');
	});
	Route::prefix('siswa')->group(function () {
		Route::get('ta', 'Raport\Nilai@siswa_tahun_ajaran');
		Route::get('mapel', 'Raport\Nilai@guru_mapel');
		Route::get('nilai', 'Raport\Nilai@siswa_nilai');
		Route::post('nilai', 'Raport\Nilai@guru_nilai_simpan');
	});
});