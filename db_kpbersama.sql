-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2019 at 06:34 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kpbersama`
--

-- --------------------------------------------------------

--
-- Table structure for table `adisposisi`
--

CREATE TABLE `adisposisi` (
  `id_adisposisi` int(11) NOT NULL,
  `nama_adisposisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adisposisi`
--

INSERT INTO `adisposisi` (`id_adisposisi`, `nama_adisposisi`) VALUES
(1, 'Kepala Sekolah'),
(2, 'Kepala Lab'),
(3, 'Kepala Perpustakaan');

-- --------------------------------------------------------

--
-- Table structure for table `arsip_surat`
--

CREATE TABLE `arsip_surat` (
  `id_arsip_surat` int(11) NOT NULL,
  `nama_arsip_surat` varchar(255) NOT NULL,
  `tanggal_arsip_surat` datetime NOT NULL,
  `dari_tanggal_arsip_surat` date NOT NULL,
  `sampai_tanggal_arsip_surat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `arsip_surat`
--

INSERT INTO `arsip_surat` (`id_arsip_surat`, `nama_arsip_surat`, `tanggal_arsip_surat`, `dari_tanggal_arsip_surat`, `sampai_tanggal_arsip_surat`) VALUES
(1, 'disrsip', '2019-02-07 13:13:22', '2019-01-07', '2019-02-07');

-- --------------------------------------------------------

--
-- Table structure for table `arsip_surat_detail`
--

CREATE TABLE `arsip_surat_detail` (
  `id_arsip_surat_detail` int(11) NOT NULL,
  `id_surat_keluar` int(11) NOT NULL,
  `id_arsip_surat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `arsip_surat_detail`
--

INSERT INTO `arsip_surat_detail` (`id_arsip_surat_detail`, `id_surat_keluar`, `id_arsip_surat`) VALUES
(1, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `id_biaya` tinyint(4) NOT NULL,
  `nama_biaya` varchar(20) NOT NULL,
  `nilai_biaya` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `disposisi`
--

CREATE TABLE `disposisi` (
  `id_disposisi` int(11) NOT NULL,
  `id_adisposisi` int(11) NOT NULL,
  `id_surat_masuk` int(11) NOT NULL,
  `tanggal_disposisi` date NOT NULL,
  `isi_disposisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `disposisi`
--

INSERT INTO `disposisi` (`id_disposisi`, `id_adisposisi`, `id_surat_masuk`, `tanggal_disposisi`, `isi_disposisi`) VALUES
(7, 1, 6, '2019-02-22', ''),
(8, 2, 6, '2019-02-22', ''),
(9, 2, 5, '0000-00-00', 'dcdc'),
(10, 1, 5, '0000-00-00', 'dcdc');

-- --------------------------------------------------------

--
-- Table structure for table `file_audio`
--

CREATE TABLE `file_audio` (
  `id_file_audio` int(2) NOT NULL,
  `nama_file_audio` varchar(25) NOT NULL,
  `lokasi_file_audio` varchar(30) NOT NULL,
  `ukuran_file_audio` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id_hak_akses` int(10) NOT NULL,
  `nama_hak_akses` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id_hak_akses`, `nama_hak_akses`) VALUES
(1, 'Semua Orang'),
(2, 'Hanya Guru'),
(3, 'Hanya Siswa'),
(4, 'Semua Orang Kecuali...'),
(5, 'Bagikan Hanya dengan...'),
(6, 'Hanya Saya');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses_group`
--

CREATE TABLE `hak_akses_group` (
  `id_hak_akses_group` int(10) NOT NULL,
  `id_hak_akses` int(10) NOT NULL,
  `type_hak_akses_group` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses_group`
--

INSERT INTO `hak_akses_group` (`id_hak_akses_group`, `id_hak_akses`, `type_hak_akses_group`) VALUES
(1, 4, ',1,'),
(2, 1, 'public'),
(3, 2, 'teacher'),
(4, 5, ',374,');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `id_slot_jam` smallint(6) NOT NULL,
  `id_plot_ajar` int(11) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `hari`, `id_slot_jam`, `id_plot_ajar`, `id_tahun_ajaran`) VALUES
(18, 'Senin', 1, 12, 2),
(19, 'Senin', 2, 12, 2),
(20, 'Senin', 4, 11, 2),
(21, 'Senin', 8, 11, 2),
(22, 'Sabtu', 2, 17, 2),
(23, 'Sabtu', 1, 17, 2),
(24, 'Kamis', 2, 16, 2),
(26, 'Kamis', 1, 16, 2),
(27, 'Senin', 3, 12, 2),
(28, 'Selasa', 1, 15, 2),
(29, 'Selasa', 2, 15, 2),
(30, 'Selasa', 3, 18, 2),
(31, 'Selasa', 4, 18, 2),
(32, 'Selasa', 8, 10, 2),
(33, 'Selasa', 9, 10, 2),
(34, 'Rabu', 2, 14, 2),
(35, 'Selasa', 11, 10, 2),
(36, 'Rabu', 1, 14, 2),
(37, 'Jum\'at', 1, 13, 2),
(38, 'Jum\'at', 2, 13, 2),
(40, 'Selasa', 12, 19, 2),
(41, 'Selasa', 11, 19, 2),
(42, 'Senin', 8, 20, 2),
(43, 'Senin', 9, 20, 2),
(44, 'Senin', 1, 21, 2),
(46, 'Senin', 2, 21, 2),
(47, 'Senin', 3, 24, 2),
(48, 'Senin', 4, 24, 2),
(49, 'Selasa', 4, 22, 2),
(51, 'Jum\'at', 3, 13, 2),
(53, 'Senin', 11, 19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_alarm`
--

CREATE TABLE `jadwal_alarm` (
  `id_jadwal_alarm` int(4) NOT NULL,
  `hari_jadwal_alarm` varchar(7) NOT NULL,
  `jam_jadwal_alarm` time NOT NULL,
  `keterangan_jadwal_alarm` varchar(25) NOT NULL,
  `id_file_audio` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_susulan_uas`
--

CREATE TABLE `jadwal_susulan_uas` (
  `id_jadwal_susulan_uas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_jadwal_uas` int(11) NOT NULL,
  `tanggal_jadwal_susulan_uas` datetime NOT NULL,
  `status_jadwal_susulan_uas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_uas`
--

CREATE TABLE `jadwal_uas` (
  `id_jadwal_uas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL,
  `nama_jadwal_uas` varchar(32) NOT NULL,
  `tanggal_jadwal_uas` datetime NOT NULL,
  `durasi_jadwal_uas` time NOT NULL,
  `status_jadwal_uas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jadwal_uas`
--

INSERT INTO `jadwal_uas` (`id_jadwal_uas`, `id_mapel`, `id_user`, `id_rombel`, `id_paket`, `id_tahun_ajaran`, `nama_jadwal_uas`, `tanggal_jadwal_uas`, `durasi_jadwal_uas`, `status_jadwal_uas`) VALUES
(11, 10, 3, 1, 1, 2, 'cac', '2019-02-08 18:14:00', '01:30:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_siswa`
--

CREATE TABLE `jawaban_siswa` (
  `id_jawaban_siswa` int(11) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_pilihan_jawaban` int(11) NOT NULL,
  `status_jawaban_siswa` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jawaban_siswa`
--

INSERT INTO `jawaban_siswa` (`id_jawaban_siswa`, `id_soal`, `id_user`, `id_pilihan_jawaban`, `status_jawaban_siswa`) VALUES
(3, 1, 374, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_uas`
--

CREATE TABLE `jawaban_uas` (
  `id_jawaban_uas` int(11) NOT NULL,
  `id_soal_uas` int(11) NOT NULL,
  `teks_jawaban_uas` text NOT NULL,
  `benar_jawaban_uas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jawaban_uas`
--

INSERT INTO `jawaban_uas` (`id_jawaban_uas`, `id_soal_uas`, `teks_jawaban_uas`, `benar_jawaban_uas`) VALUES
(39, 12, 'csdc', 1),
(40, 12, 'csd', 0),
(41, 12, 'sd', 0),
(42, 12, 'efer', 0),
(43, 13, 'gtrg', 0),
(44, 13, 'dfds', 0),
(45, 13, 'werw', 1),
(46, 13, 'gtrg', 0),
(47, 14, 'rgt', 0),
(48, 14, 'bfdb', 0),
(49, 14, 'dfvdfv', 1),
(50, 14, 'dfs', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jawaban_uas_siswa`
--

CREATE TABLE `jawaban_uas_siswa` (
  `id_jawaban_uas_siswa` int(11) NOT NULL,
  `id_soal_uas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_jadwal_uas` int(11) NOT NULL,
  `id_jawaban_uas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jawaban_uas_siswa`
--

INSERT INTO `jawaban_uas_siswa` (`id_jawaban_uas_siswa`, `id_soal_uas`, `id_user`, `id_jadwal_uas`, `id_jawaban_uas`) VALUES
(54, 13, 374, 11, 46);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_file`
--

CREATE TABLE `jenis_file` (
  `id_jenis_file` int(10) NOT NULL,
  `nama_jenis_file` varchar(15) NOT NULL,
  `ikon_jenis_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenis_file`
--

INSERT INTO `jenis_file` (`id_jenis_file`, `nama_jenis_file`, `ikon_jenis_file`) VALUES
(2, 'jpg', 'thumbnail/unknow.png'),
(3, 'png', 'thumbnail/unknow.png');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_surat`
--

CREATE TABLE `jenis_surat` (
  `id_jenis_surat` int(11) NOT NULL,
  `kode_jenis_surat` varchar(255) NOT NULL,
  `nama_jenis_surat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenis_surat`
--

INSERT INTO `jenis_surat` (`id_jenis_surat`, `kode_jenis_surat`, `nama_jenis_surat`) VALUES
(1, 'A01/%nomor%/%arsip%/SMKI.Tj/%bulan%/%tahun%', 'Surat Pemberitahuan');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` smallint(6) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'Teknik Komputer Jaringan'),
(2, 'Teknik Sepeda Motor'),
(4, 'Teknik Mesin');

-- --------------------------------------------------------

--
-- Table structure for table `kandidat`
--

CREATE TABLE `kandidat` (
  `id_kandidat` int(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  `visi_kandidat` text NOT NULL,
  `misi_kandidat` text NOT NULL,
  `status_kandidat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_beasiswa`
--

CREATE TABLE `kategori_beasiswa` (
  `id_kategori_beasiswa` tinyint(4) NOT NULL,
  `nama_kategori_beasiswa` varchar(20) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_beasiswa`
--

INSERT INTO `kategori_beasiswa` (`id_kategori_beasiswa`, `nama_kategori_beasiswa`, `id_tahun_ajaran`) VALUES
(3, 'xsax', 3),
(6, 'TERBAIK', 2),
(7, 'TERKEREN', 2);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_komentar` int(5) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `file_komentar` varchar(255) NOT NULL,
  `judul_komentar` varchar(255) NOT NULL,
  `keterangan_komentar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_beasiswa`
--

CREATE TABLE `kriteria_beasiswa` (
  `id_kriteria_beasiswa` tinyint(4) NOT NULL,
  `id_kategori_beasiswa` tinyint(4) NOT NULL,
  `nama_kriteria_beasiswa` varchar(255) NOT NULL,
  `bobot_kriteria_beasiswa` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kriteria_beasiswa`
--

INSERT INTO `kriteria_beasiswa` (`id_kriteria_beasiswa`, `id_kategori_beasiswa`, `nama_kriteria_beasiswa`, `bobot_kriteria_beasiswa`) VALUES
(20, 6, 'rf', 3),
(21, 7, 'dfaaf', 44),
(22, 6, '3', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kunci_jawaban`
--

CREATE TABLE `kunci_jawaban` (
  `id_kunci_jawaban` int(11) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `id_pilihan_jawaban` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kunci_jawaban`
--

INSERT INTO `kunci_jawaban` (`id_kunci_jawaban`, `id_soal`, `id_pilihan_jawaban`) VALUES
(1, 1, 2),
(2, 6, 7),
(3, 7, 8),
(4, 8, 14),
(5, 5, 20),
(6, 4, 21);

-- --------------------------------------------------------

--
-- Table structure for table `level_kelas`
--

CREATE TABLE `level_kelas` (
  `id_level_kelas` tinyint(4) NOT NULL,
  `nama_level_kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level_kelas`
--

INSERT INTO `level_kelas` (`id_level_kelas`, `nama_level_kelas`) VALUES
(1, 'X'),
(2, 'XI'),
(3, 'XII'),
(4, 'M');

-- --------------------------------------------------------

--
-- Table structure for table `level_user`
--

CREATE TABLE `level_user` (
  `id_level_user` tinyint(4) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_type` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level_user`
--

INSERT INTO `level_user` (`id_level_user`, `id_user`, `id_type`) VALUES
(1, 2, 1),
(2, 3, 2),
(3, 1, 3),
(4, 2, 2),
(6, 8, 3),
(7, 4, 2),
(8, 3, 1),
(9, 11, 3),
(12, 3, 5),
(29, 29, 3),
(30, 30, 3),
(31, 33, 3),
(32, 284, 3),
(33, 285, 3),
(34, 286, 3),
(35, 287, 3),
(36, 288, 3),
(37, 289, 3),
(38, 290, 3),
(39, 291, 3),
(40, 292, 3),
(41, 293, 3),
(42, 294, 3),
(43, 295, 3),
(44, 296, 3),
(45, 297, 3),
(46, 298, 3),
(47, 299, 3),
(48, 300, 3),
(49, 301, 3),
(50, 302, 3),
(51, 303, 3),
(52, 304, 3),
(53, 305, 3),
(54, 306, 3),
(55, 307, 3),
(56, 308, 3),
(57, 309, 3),
(58, 310, 3),
(59, 311, 3),
(60, 312, 3),
(61, 313, 3),
(62, 314, 3),
(63, 315, 3),
(64, 316, 3),
(65, 317, 3),
(66, 318, 3),
(67, 319, 3),
(68, 320, 3),
(69, 321, 3),
(70, 322, 3),
(71, 323, 3),
(72, 324, 3),
(73, 325, 3),
(74, 326, 3),
(75, 327, 3),
(76, 328, 3),
(77, 329, 3),
(78, 330, 3),
(79, 331, 3),
(80, 332, 3),
(81, 333, 3),
(82, 334, 3),
(83, 335, 3),
(84, 336, 3),
(85, 337, 3),
(86, 338, 3),
(87, 339, 3),
(88, 340, 3),
(89, 353, 3),
(90, 354, 3),
(91, 355, 3),
(92, 356, 3),
(93, 357, 3),
(94, 358, 3),
(95, 359, 3),
(96, 360, 3),
(97, 361, 3),
(98, 362, 3),
(99, 363, 3),
(101, 365, 3),
(102, 366, 3),
(103, 367, 3),
(104, 368, 3),
(105, 369, 3),
(107, 371, 3),
(108, 373, 1),
(109, 374, 3);

-- --------------------------------------------------------

--
-- Table structure for table `log_rombel`
--

CREATE TABLE `log_rombel` (
  `id_log_rombel` int(11) NOT NULL,
  `nama_rombel` varchar(40) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log_rombel`
--

INSERT INTO `log_rombel` (`id_log_rombel`, `nama_rombel`, `id_user`, `id_tahun_ajaran`) VALUES
(42, 'Teknik Otomotif XI A', 29, 3),
(43, 'Teknik Otomotif XI A', 11, 3),
(44, 'Teknik Komputer Jaringan XI B', 8, 3),
(45, 'Teknik Komputer Jaringan XI A', 1, 3),
(46, 'Teknik Otomotif X B', 11, 1),
(47, 'Teknik Otomotif XI B', 33, 3),
(48, 'Teknik Otomotif XI B', 30, 3),
(49, 'Teknik Otomotif XII A', 33, 4),
(50, 'Teknik Otomotif XII A', 30, 4),
(51, 'Teknik Otomotif XII A', 11, 4),
(52, 'Teknik Otomotif XII B', 33, 1),
(53, 'Teknik Otomotif XII B', 30, 1),
(54, 'Teknik Otomotif XII B', 11, 1),
(55, 'Teknik Otomotif X A', 284, 2),
(56, 'Teknik Komputer Jaringan X A', 285, 2),
(57, 'Teknik Komputer Jaringan X A', 286, 2),
(58, 'Teknik Komputer Jaringan X A', 287, 2),
(59, 'Teknik Komputer Jaringan X A', 288, 2),
(60, 'Teknik Komputer Jaringan X A', 289, 2),
(61, 'Teknik Komputer Jaringan X A', 290, 2),
(62, 'Teknik Komputer Jaringan X A', 291, 2),
(63, 'Teknik Komputer Jaringan X A', 292, 2),
(64, 'Teknik Komputer Jaringan X B', 293, 2),
(65, 'Teknik Komputer Jaringan X B', 294, 2),
(66, 'Teknik Komputer Jaringan X B', 295, 2),
(67, 'Teknik Komputer Jaringan X B', 296, 2),
(68, 'Teknik Komputer Jaringan X B', 297, 2),
(69, 'Teknik Mesin X A', 298, 2),
(70, 'Teknik Mesin X A', 299, 2),
(71, 'Teknik Mesin X A', 300, 2),
(72, 'Teknik Mesin X A', 301, 2),
(73, 'Teknik Mesin X A', 302, 2),
(74, 'Teknik Mesin X A', 303, 2),
(75, 'Teknik Mesin X A', 304, 2),
(76, 'Teknik Mesin X A', 305, 2),
(77, 'Teknik Mesin X A', 306, 2),
(78, 'Teknik Mesin X A', 307, 2),
(79, 'Teknik Mesin X A', 308, 2),
(80, 'Teknik Mesin X A', 309, 2),
(81, 'Teknik Mesin X A', 310, 2),
(82, 'Teknik Mesin X A', 311, 2),
(83, 'Teknik Mesin X A', 312, 2),
(84, 'Teknik Otomotif X B', 313, 2),
(85, 'Teknik Otomotif X B', 314, 2),
(86, 'Teknik Otomotif X B', 315, 2),
(87, 'Teknik Otomotif X B', 316, 2),
(88, 'Teknik Otomotif X B', 317, 2),
(89, 'Teknik Otomotif X B', 318, 2),
(90, 'Teknik Otomotif X B', 319, 2),
(91, 'Teknik Otomotif X B', 320, 2),
(92, 'Teknik Otomotif X B', 321, 2),
(93, 'Teknik Otomotif X B', 322, 2),
(94, 'Teknik Otomotif X B', 323, 2),
(95, 'Teknik Otomotif X B', 324, 2),
(96, 'Teknik Otomotif X A', 325, 2),
(97, 'Teknik Otomotif X A', 326, 2),
(98, 'Teknik Otomotif X A', 327, 2),
(99, 'Teknik Otomotif X A', 328, 2),
(100, 'Teknik Otomotif X A', 329, 2),
(101, 'Teknik Otomotif X A', 330, 2),
(102, 'Teknik Otomotif X A', 331, 2),
(103, 'Teknik Otomotif X A', 332, 2),
(104, 'Teknik Otomotif X A', 333, 2),
(105, 'Teknik Otomotif X A', 334, 2),
(106, 'Teknik Otomotif X A', 335, 2),
(107, 'Teknik Otomotif X A', 336, 2),
(108, 'Teknik Otomotif X A', 337, 2),
(109, 'Teknik Otomotif X A', 338, 2),
(110, 'Teknik Otomotif X A', 339, 2),
(111, 'Teknik Otomotif X A', 340, 2),
(112, 'Teknik Komputer Jaringan X B', 353, 2),
(113, 'Teknik Komputer Jaringan X B', 354, 2),
(114, 'Teknik Komputer Jaringan X B', 355, 2),
(115, 'Teknik Komputer Jaringan X B', 356, 2),
(116, 'Teknik Komputer Jaringan X B', 357, 2),
(117, 'Teknik Komputer Jaringan X B', 358, 2),
(118, 'Teknik Komputer Jaringan X B', 359, 2),
(119, 'Teknik Komputer Jaringan X B', 360, 2),
(120, 'Teknik Komputer Jaringan X B', 361, 2),
(121, 'Teknik Komputer Jaringan X B', 362, 2),
(122, 'Teknik Komputer Jaringan X B', 363, 2),
(124, 'Teknik Komputer Jaringan X A', 365, 2),
(125, 'Teknik Komputer Jaringan X A', 366, 2),
(126, 'Teknik Komputer Jaringan X A', 367, 2),
(127, 'Teknik Komputer Jaringan X A', 368, 2),
(128, 'Teknik Komputer Jaringan X A', 369, 2),
(130, 'Teknik Komputer Jaringan X A', 371, 2);

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(20) NOT NULL,
  `id_jurusan` smallint(6) NOT NULL,
  `id_level_kelas` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`, `id_jurusan`, `id_level_kelas`) VALUES
(1, 'Bahasa Indonesia', 1, 1),
(2, 'Bahasa Inggris', 1, 1),
(3, 'Bahasa Indonesia', 2, 1),
(4, 'Bahasa Inggris', 2, 1),
(6, 'Bahasa Inggris', 2, 2),
(7, 'Bahasa Indonesia', 2, 2),
(8, 'Bahasa Madura', 2, 2),
(9, 'Bahasa Madura', 1, 1),
(10, 'Matematika', 1, 1),
(11, 'Agama Islam', 1, 1),
(12, 'Penjaskes', 1, 1),
(13, 'Kewarganegaraan', 1, 1),
(14, 'Kimia', 1, 1),
(15, 'Fisika', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `masa_pemilu`
--

CREATE TABLE `masa_pemilu` (
  `id_masa_pemilu` int(3) NOT NULL,
  `waktu_mulai_pendaftaran_masa_pemilu` date NOT NULL,
  `waktu_selesai_pendaftaran_masa_pemilu` date NOT NULL,
  `waktu_mulai_pemilihan_masa_pemilu` date NOT NULL,
  `waktu_selesai_pemilihan_masa_pemilu` date NOT NULL,
  `waktu_pengumuman_masa_pemilu` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id_materi` int(5) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `nama_materi` varchar(255) NOT NULL,
  `isi_materi` text NOT NULL,
  `file_materi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `id_rombel`, `id_user`, `id_mapel`, `nama_materi`, `isi_materi`, `file_materi`) VALUES
(1, 1, 3, 1, 'MATERI PEMBELAJARAN', 'ISI', 'FILE');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_quis`
--

CREATE TABLE `nilai_quis` (
  `id_nilai_quis` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nilai_nilai_quis` int(10) NOT NULL,
  `id_quis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nilai_uas`
--

CREATE TABLE `nilai_uas` (
  `id_nilai_uas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_jadwal_uas` int(11) NOT NULL,
  `total_soal_nilai_uas` int(11) DEFAULT NULL,
  `total_dijawab_nilai_uas` int(11) DEFAULT NULL,
  `total_benar_nilai_uas` int(11) DEFAULT NULL,
  `tanggal_mulai_ujian` datetime DEFAULT NULL,
  `tanggal_selesai_ujian` datetime DEFAULT NULL,
  `nilai_huruf_nilai_uas` varchar(1) DEFAULT NULL,
  `nilai_angka_nilai_uas` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nilai_uas`
--

INSERT INTO `nilai_uas` (`id_nilai_uas`, `id_user`, `id_jadwal_uas`, `total_soal_nilai_uas`, `total_dijawab_nilai_uas`, `total_benar_nilai_uas`, `tanggal_mulai_ujian`, `tanggal_selesai_ujian`, `nilai_huruf_nilai_uas`, `nilai_angka_nilai_uas`) VALUES
(64, 285, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 286, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 287, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 288, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 289, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 290, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 291, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 292, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 365, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 366, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 367, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 368, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 369, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 371, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 374, 11, 3, 1, 0, NULL, '2019-02-08 18:14:56', 'E', 0);

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id_paket` int(11) NOT NULL,
  `nama_paket` varchar(255) NOT NULL,
  `maks_soal_paket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `nama_paket`, `maks_soal_paket`) VALUES
(1, 'aaa', 50);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggaran`
--

CREATE TABLE `pelanggaran` (
  `id_pelanggaran` int(11) NOT NULL,
  `pelanggaran_pelanggaran` varchar(255) NOT NULL,
  `poin_pelanggaran` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelanggaran`
--

INSERT INTO `pelanggaran` (`id_pelanggaran`, `pelanggaran_pelanggaran`, `poin_pelanggaran`) VALUES
(1, 'terlambat', 2),
(2, 'pulang', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggaran_siswa`
--

CREATE TABLE `pelanggaran_siswa` (
  `id_pelanggaran_siswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `view_pelanggaran_siswa` text NOT NULL,
  `view_sanksi_pelanggaran_siswa` text NOT NULL,
  `view_surat_pelanggaran_siswa` longtext,
  `apoint_pelanggaran_siswa` float NOT NULL,
  `tgl_pelanggaran_siswa` datetime NOT NULL,
  `log_pelanggaran_siswa` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelanggaran_siswa`
--

INSERT INTO `pelanggaran_siswa` (`id_pelanggaran_siswa`, `id_user`, `view_pelanggaran_siswa`, `view_sanksi_pelanggaran_siswa`, `view_surat_pelanggaran_siswa`, `apoint_pelanggaran_siswa`, `tgl_pelanggaran_siswa`, `log_pelanggaran_siswa`) VALUES
(1, 8, 'terlambat', 'TEGUR', NULL, 2, '2019-01-23 08:13:52', '{\"id\":8,\"nis\":\"2015520017\",\"nama\":\"Amin Rois\",\"jurusan\":\"Teknik Komputer Jaringan\",\"kelas\":\"B\",\"level_kelas\":\"XI\"}'),
(2, 8, 'pulang', 'JUZ 4', NULL, 3, '2019-01-23 08:14:05', '{\"id\":8,\"nis\":\"2015520017\",\"nama\":\"Amin Rois\",\"jurusan\":\"Teknik Komputer Jaringan\",\"kelas\":\"B\",\"level_kelas\":\"XI\"}'),
(3, 30, 'terlambat', 'TEGUR', NULL, 2, '2019-02-05 11:17:46', '{\"id\":30,\"nis\":\"2015520022\",\"nama\":\"Dewi Sutanti\",\"jurusan\":\"Teknik Sepeda Motor\",\"kelas\":\"B\",\"level_kelas\":\"XII\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` tinyint(4) NOT NULL,
  `jumlah_pembayaran` int(11) NOT NULL,
  `waktu_pembayaran` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tagihan` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pemilihan`
--

CREATE TABLE `pemilihan` (
  `id_pemilihan` int(3) NOT NULL,
  `id_kandidat` int(3) NOT NULL,
  `id_masa_pemilu` int(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  `waktu_pemilihan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `label_pengaturan` varchar(32) NOT NULL,
  `value_pengaturan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`label_pengaturan`, `value_pengaturan`) VALUES
('juz_quran_pelanggaran', '5');

-- --------------------------------------------------------

--
-- Table structure for table `perizinan_siswa`
--

CREATE TABLE `perizinan_siswa` (
  `id_perizinan_siswa` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan_perizinan_siswa` text,
  `view_surat_perizinan_siswa` longtext,
  `kembali_perizinan_siswa` int(11) NOT NULL,
  `tgl_perizinan_siswa` datetime NOT NULL,
  `status_kembali_perizinan_siswa` int(11) DEFAULT NULL,
  `log_perizinan_siswa` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perizinan_siswa`
--

INSERT INTO `perizinan_siswa` (`id_perizinan_siswa`, `id_user`, `keterangan_perizinan_siswa`, `view_surat_perizinan_siswa`, `kembali_perizinan_siswa`, `tgl_perizinan_siswa`, `status_kembali_perizinan_siswa`, `log_perizinan_siswa`) VALUES
(1, 8, NULL, '&#60p&#62&#60&#47p&#62&#60p&#62hhh&#60&#47p&#62', 0, '2019-01-23 08:14:24', 0, '{\"id\":8,\"nis\":\"2015520017\",\"nama\":\"Amin Rois\",\"jurusan\":\"Teknik Komputer Jaringan\",\"kelas\":\"B\",\"level_kelas\":\"XI\"}'),
(2, 29, 'keperluan', '&#60p&#62&#60&#47p&#62&#60p&#62hhh&#60&#47p&#62', 0, '2019-02-05 11:18:14', 0, '{\"id\":29,\"nis\":\"123123\",\"nama\":\"Subaidi\",\"jurusan\":\"Teknik Sepeda Motor\",\"kelas\":\"A\",\"level_kelas\":\"XI\"}');

-- --------------------------------------------------------

--
-- Table structure for table `peserta_beasiswa`
--

CREATE TABLE `peserta_beasiswa` (
  `id_peserta_beasiswa` tinyint(4) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pilihan_jawaban`
--

CREATE TABLE `pilihan_jawaban` (
  `id_pilihan_jawaban` int(11) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `text_pilihan_jawaban` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pilihan_jawaban`
--

INSERT INTO `pilihan_jawaban` (`id_pilihan_jawaban`, `id_soal`, `text_pilihan_jawaban`) VALUES
(1, 1, 'bakan tahu'),
(2, 1, 'makan sendok'),
(3, 1, 'minum susu'),
(4, 6, 'pergi'),
(5, 6, 'tinggal'),
(6, 6, 'biarkan'),
(7, 6, 'semua benar'),
(8, 7, 'vsv'),
(9, 7, 'gt'),
(10, 7, 'ger'),
(11, 7, 'bb'),
(12, 8, 'avdv'),
(13, 8, 'gerg'),
(14, 8, 'bfsb'),
(15, 8, 'vvsfv'),
(16, 1, 'tinggi'),
(17, 5, 'tak akan berhenti'),
(18, 5, 'aku berkelana'),
(19, 5, 'hoho hmhmhm'),
(20, 5, 'walaupun adanya diujung dunia aku kan kesana tuk mendapatkannya'),
(21, 4, 'csdc'),
(22, 4, 'cd'),
(23, 4, 'r23r'),
(24, 4, 'cec');

-- --------------------------------------------------------

--
-- Table structure for table `pilih_biaya`
--

CREATE TABLE `pilih_biaya` (
  `id_pilih_biaya` tinyint(4) NOT NULL,
  `id_tagihan` tinyint(4) NOT NULL,
  `id_biaya` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plot_ajar`
--

CREATE TABLE `plot_ajar` (
  `id_plot_ajar` int(11) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plot_ajar`
--

INSERT INTO `plot_ajar` (`id_plot_ajar`, `id_rombel`, `id_mapel`, `id_user`, `id_tahun_ajaran`) VALUES
(2, 2, 3, 3, 2),
(4, 2, 4, 3, 2),
(6, 4, 6, 10, 2),
(7, 4, 7, 10, 2),
(8, 1, 2, 10, 1),
(9, 4, 8, 3, 2),
(10, 1, 9, 3, 2),
(11, 1, 10, 22, 2),
(12, 1, 1, 10, 2),
(13, 1, 2, 10, 2),
(14, 1, 11, 31, 2),
(15, 1, 12, 32, 2),
(16, 1, 13, 341, 2),
(17, 1, 14, 343, 2),
(18, 1, 15, 342, 2),
(19, 7, 1, 10, 2),
(20, 7, 2, 10, 2),
(21, 7, 10, 22, 2),
(22, 7, 9, 3, 2),
(23, 7, 11, 31, 2),
(24, 7, 12, 32, 2),
(25, 7, 13, 341, 2),
(26, 7, 14, 343, 2),
(27, 7, 15, 342, 2);

-- --------------------------------------------------------

--
-- Table structure for table `profile_gukar`
--

CREATE TABLE `profile_gukar` (
  `id_profile_gukar` int(11) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama_gukar` varchar(40) NOT NULL,
  `jenis_kelamin_gukar` varchar(10) NOT NULL,
  `alamat_gukar` varchar(255) NOT NULL,
  `tempat_lahir_gukar` varchar(30) NOT NULL,
  `tanggal_lahir_gukar` date NOT NULL,
  `id_user` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_gukar`
--

INSERT INTO `profile_gukar` (`id_profile_gukar`, `nik`, `nama_gukar`, `jenis_kelamin_gukar`, `alamat_gukar`, `tempat_lahir_gukar`, `tanggal_lahir_gukar`, `id_user`, `foto`) VALUES
(1, '7002884828', 'RIA ANDRIANA', 'Perempuan', 'Jl. Rajawali No.21 Sampang', 'Sampang', '1993-11-18', 3, ''),
(4, '7002884819', 'ABDUL ROHIM', 'Laki-Laki', 'Tanjung Sampang', 'Sampang', '1992-03-30', 10, ''),
(5, '7777772222', 'Siti Mutmainah', 'Perempuan', 'Jl tmur', '', '2018-12-18', 22, ''),
(6, '7002884820', 'NASA\'I', 'Laki-Laki', '', '', '2018-12-18', 31, ''),
(7, '7777777', 'Dian Subargio', 'Perempuan', '', '', '0000-00-00', 32, ''),
(8, '701110035', 'ASRI AFRIYANI', 'P', 'KP. MAYAK RT 03/02 KEC. CIKEMBAR', 'SUKABUMI', '1994-11-01', 341, ''),
(9, '701110036', 'ASRI DWINITA PUTRA', 'P', 'KP. CIKEMBAR RT 02/04 DS/KEC CIKEMBAR SUKABUMI', 'SUKABUMI', '1994-11-02', 342, ''),
(10, '701110037', 'AVINDA EKA UTAMI', 'P', 'BTN CIKEMBANG BLOK C 1 NO 11', 'SUKABUMI', '1994-11-03', 343, ''),
(11, '701110038', 'AYU GINA KAROLINA', 'P', 'KP. TALAGASARI', 'SUKABUMI', '1994-11-04', 344, ''),
(12, '701110039', 'AYU INDAH P.', 'P', 'WARUNGKIARA RT 03/01 KEC WARUNG KIARA', 'SUKABUMI', '1994-11-05', 345, ''),
(13, '701110040', 'AZMI NURJIA', 'P', 'KP. PASIRGABIG RT 03/03 DS. BOJONG KEC. CIKEMBAR', 'SUKABUMI', '1994-11-06', 346, ''),
(14, '701110041', 'BAYU ARDYAWAN', 'L', 'JLN PELDA KM 20 CIKEMBAR', 'SUKOHARJO', '1994-11-07', 347, ''),
(15, '701110042', 'BAYU BELLA SUGMA', 'L', 'KP. CIKEMBAR KECAMATAN CIKEMBAR', 'GRESIK', '1994-11-08', 348, ''),
(16, '701110043', 'BEKTI NURCAHYANI', 'P', 'KP. GENTONG MANGUNJAYA BANTARGADUNG', 'SUKABUMI', '1994-11-09', 349, ''),
(17, '701110044', 'BELLA EKA KUSUMAWARDANI', 'P', 'ASMIL YON ARMED 13 RT 01/03 KEC. CIKEMBAR', 'SUKABUMI', '1994-11-10', 350, ''),
(18, '701110045', 'BUDIYANSYAH', 'L', 'KP. BOJONG KALER RT 02/07', 'SUKABUMI', '1994-11-11', 351, ''),
(19, '701110046', 'BUNGA SHULHATUL IMAMAH', 'P', 'JLN PELDA KM 20 CIKEMBAR', 'SUKABUMI', '1994-11-12', 352, '');

-- --------------------------------------------------------

--
-- Table structure for table `profile_siswa`
--

CREATE TABLE `profile_siswa` (
  `id_profile_siswa` int(11) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `nama_siswa` varchar(40) NOT NULL,
  `jenis_kelamin_siswa` varchar(10) NOT NULL,
  `alamat_siswa` varchar(255) NOT NULL,
  `tempat_lahir_siswa` varchar(30) NOT NULL,
  `tanggal_lahir_siswa` date NOT NULL,
  `sekolah_asal` varchar(50) NOT NULL,
  `alamat_sekolah_asal` varchar(255) NOT NULL,
  `nama_ayah` varchar(40) NOT NULL,
  `nama_ibu` varchar(40) NOT NULL,
  `alamat_orang_tua` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `id_rombel` int(11) NOT NULL,
  `id_status_siswa` tinyint(4) NOT NULL,
  `id_jurusan` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_siswa`
--

INSERT INTO `profile_siswa` (`id_profile_siswa`, `nis`, `nama_siswa`, `jenis_kelamin_siswa`, `alamat_siswa`, `tempat_lahir_siswa`, `tanggal_lahir_siswa`, `sekolah_asal`, `alamat_sekolah_asal`, `nama_ayah`, `nama_ibu`, `alamat_orang_tua`, `id_user`, `foto`, `id_rombel`, `id_status_siswa`, `id_jurusan`) VALUES
(2, '2050001', 'Sandi Rahmatullah', 'Laki-Laki', 'Jl. Trunojoyo No.72 Pamekasan', 'Pamekasan', '2002-11-12', 'SMPN 1 Pamekasan', 'Pamekasan', 'Suhairi', 'Sunarti', 'Jl. Trunojoyo No.72 Pamekasan', 1, '', 9, 1, 1),
(3, '2015520017', 'Amin Rois', 'Laki-Laki', 'Candi Burung Pamekasan', 'Pamekasan', '1997-10-11', 'SMKN 2 Pamekasan', 'Jl. Kyai Ilyas No. 32 Pamekasan', 'Suparman', 'Yanti', 'Candi Burung Pamekasan', 8, '/api/images/20181209132741.jpg', 9, 1, 1),
(4, '2015520002', 'Rahmad Subaidi', 'Laki-Laki', '', '', '2002-07-18', '', '', '', '', '', 11, '', 12, 1, 2),
(9, '123123', 'Subaidi', 'Laki-Laki', 'null', 'null', '2002-07-18', 'null', 'null', 'null', 'null', 'null', 29, '', 4, 1, 2),
(10, '2015520022', 'Dewi Sutanti', 'Laki-Laki', 'null', 'null', '0000-00-00', 'null', 'null', 'null', 'null', 'null', 30, '', 12, 1, 2),
(11, '2000000014', 'Siti Kumala Sari', 'Perempuan', '', '', '2018-12-19', '', '', '', '', '', 33, '', 12, 1, 2),
(12, '1111111111', 'AnjayLLOOO', 'Laki-Laki', 'Jl. MAKAN KAYU', 'PAMEKASAN', '1996-07-04', '', '', '', '', '', 284, '', 2, 1, 2),
(13, '111210002', 'A. SOPIYAN', 'Laki-Laki', 'KP. PASIR GABIG RT/02/03 BOJONG KEMBAR', 'SUKABUMI', '1995-08-01', '', '', '', '', '', 285, '', 1, 1, 1),
(14, '111210003', 'ABDILLAH MUHAMMAD IQBAL', 'Laki-Laki', 'KP. PANYINDANGAN JAMPANGTENGAH', 'SUKABUMI', '1995-08-02', '', '', '', '', '', 286, '', 1, 1, 1),
(15, '111210004', 'ADE HERMAWAN', 'Laki-Laki', 'KP. CILULUMPANG RT/RW 04/18 BOJONGKEMBAR', 'SUKABUMI', '1995-08-03', '', '', '', '', '', 287, '', 1, 1, 1),
(16, '111210005', 'ADITIA DWI PRAKOSO', 'Laki-Laki', 'ASRAMA YON ARMED CIKEMBANG', 'SUKABUMI', '1995-08-04', '', '', '', '', '', 288, '', 1, 1, 1),
(17, '111210006', 'AG NOER INDRI SULISTIANI', 'Perempuan', 'KP. PADABENGHAR JAMPANG TENGAH', 'SUKABUMI', '1995-08-05', '', '', '', '', '', 289, '', 1, 1, 1),
(18, '111210007', 'AGUNG GUMELAR', 'Laki-Laki', 'KP. CILANGKAP CIKEMBAR', 'SUKABUMI', '1995-08-06', '', '', '', '', '', 290, '', 1, 1, 1),
(19, '111210008', 'AGUNG GUMILAR', 'Laki-Laki', 'KP. BATU LAYANG WARUNGKIARA', 'SUKABUMI', '1995-08-07', '', '', '', '', '', 291, '', 1, 1, 1),
(20, '111210009', 'AGUS DIAN RUSDIANA', 'Laki-Laki', 'KP. MEKARJAYA SIRNARESMI', 'SUKABUMI', '1995-08-08', '', '', '', '', '', 292, '', 1, 1, 1),
(21, '111210010', 'AGUS SAEPUL MIKDAR', 'Laki-Laki', 'KP. PANGLESERAN RT/RW 04/04 SIRNARESMI', 'SUKABUMI', '1995-08-09', '', '', '', '', '', 293, '', 7, 1, 1),
(22, '111210011', 'AHMAD BAYADI ABDILLAH', 'Laki-Laki', 'KP. PADABENGHAR JAMPANG TENGAH', 'SUKABUMI', '1995-08-10', '', '', '', '', '', 294, '', 7, 1, 1),
(23, '111210012', 'AI JULITA SRI AGUSTIN', 'Perempuan', 'KP. SAMPALAN', 'SUKABUMI', '1995-08-11', '', '', '', '', '', 295, '', 7, 1, 1),
(24, '111210013', 'AI MARLIANTI', 'Perempuan', 'KP. MUARA CIKEMBANG', 'SUKABUMI', '1995-08-12', '', '', '', '', '', 296, '', 7, 1, 1),
(25, '111210014', 'AINUN MAWADDAH', 'Perempuan', 'KP. CIKAREO RT/RW 01/05 BOJONGKEMBAR', 'SUKABUMI', '1995-08-13', '', '', '', '', '', 297, '', 7, 1, 1),
(26, '111210015', 'AISAH OKTAPIANI', 'Perempuan', 'KP. TANGKIL RT/RW 06/08 DESA CIMANGU CIKEMBAR', 'SUKABUMI', '1995-08-14', '', '', '', '', '', 298, '', 15, 1, 4),
(27, '111210016', 'ALDI AGUSTIAN WIRAWANSYAH', 'Laki-Laki', 'KP. PADASUKA RT/RW 01/05 DESA KERTARAHARJA', 'SUKABUMI', '1995-08-15', '', '', '', '', '', 299, '', 15, 1, 4),
(28, '111210017', 'ALTIAN DARMAWAN', 'Laki-Laki', 'KP. SIRNARESMI', 'SUKABUMI', '1995-08-16', '', '', '', '', '', 300, '', 15, 1, 4),
(29, '111210018', 'AMALIA AGUSTINA SETIANI', 'Perempuan', 'KP. PANYINDANGAN', 'SUKABUMI', '1995-08-17', '', '', '', '', '', 301, '', 15, 1, 4),
(30, '111210019', 'AMIR', 'Laki-Laki', 'KP. CIKEMBANG RT/RW 06/03 DESA SUKAMULYA', 'JAKARTA', '1995-08-18', '', '', '', '', '', 302, '', 15, 1, 4),
(31, '111210020', 'ANA SURYANA', 'Laki-Laki', 'KP. SEDAMUKTI BOJONG', 'SUKABUMI', '1995-08-19', '', '', '', '', '', 303, '', 15, 1, 4),
(32, '111210021', 'ANDINI FITRI HANDAYANI', 'Perempuan', 'KP. CILANGKAP CIKEMBAR', 'SUKABUMI', '1995-08-20', '', '', '', '', '', 304, '', 15, 1, 4),
(33, '111210022', 'ANDRI SUTIAWAN', 'Laki-Laki', 'KP. LEUWI PENDEUY RT/19/04 PADABENGHAR', 'SUKABUMI', '1995-08-21', '', '', '', '', '', 305, '', 15, 1, 4),
(34, '111210023', 'ANDRI YUDA PRATAMA', 'Laki-Laki', 'KP. CIORAY', 'SUKABUMI', '1995-08-22', '', '', '', '', '', 306, '', 15, 1, 4),
(35, '111210024', 'ANDRIANY WINANDAR', 'Perempuan', 'KP. UBRUG RT/RW 01/08', 'SUKABUMI', '1995-08-23', '', '', '', '', '', 307, '', 15, 1, 4),
(36, '111210025', 'ANGGI MULYANI', 'Perempuan', 'KP. SAMPORA', 'SUKABUMI', '1995-08-24', '', '', '', '', '', 308, '', 15, 1, 4),
(37, '111210026', 'ANGGIH PRASETYO', 'Laki-Laki', 'KP. CISONGGOM CIKEMBAR', 'SUKABUMI', '1995-08-25', '', '', '', '', '', 309, '', 15, 1, 4),
(38, '111210027', 'ANGGRIYANI', 'Perempuan', 'KP. CIBARENGKOK CIKEMBAR', 'SUKABUMI', '1995-08-26', '', '', '', '', '', 310, '', 15, 1, 4),
(39, '111210028', 'ANNIDA SHAFIA RACHMAN', 'Perempuan', 'KP. CIKEMBAR', 'SUKABUMI', '1995-08-27', '', '', '', '', '', 311, '', 15, 1, 4),
(40, '111210029', 'ANNISA FUJIANTI', 'Perempuan', 'KP. DANO KIDUL BOJONGKEMBAR', 'SUKABUMI', '1995-08-28', '', '', '', '', '', 312, '', 15, 1, 4),
(41, '111210236', 'SELA OKTAVIANI', 'Perempuan', 'KP. SAMPORA RT.02/03 BOJONG KEC. CIKEMBAR', 'SUKABUMI', '1996-08-16', '', '', '', '', '', 313, '', 8, 1, 2),
(42, '111210237', 'SELLY RATNASARI KUSWANDI', 'Perempuan', 'KP. SUKARESMI BOJONGKERTA WARUNGKIARA', 'SUKABUMI', '1996-10-12', '', '', '', '', '', 314, '', 8, 1, 2),
(43, '111210238', 'SEPTIONO WIRA UTAMA', 'Perempuan', 'KP. CIKATE CIKEMBAR', 'SUKABUMI', '1996-05-13', '', '', '', '', '', 315, '', 8, 1, 2),
(44, '111210239', 'SERGI PRAYOGA', 'Laki-Laki', 'KP. CIHUNI CIRENGKOL', 'SUKABUMI', '1995-09-11', '', '', '', '', '', 316, '', 8, 1, 2),
(45, '111210240', 'SHANTI AFRIANI', 'Laki-Laki', 'KP. CIKEMBANG', 'SUKABUMI', '1996-08-11', '', '', '', '', '', 317, '', 8, 1, 2),
(46, '111210241', 'SILBY ZALYN AZELIKA', 'Perempuan', 'KP. SUKASARI DESA CIKEMBAR', 'SUKABUMI', '1996-01-19', '', '', '', '', '', 318, '', 8, 1, 2),
(47, '111210242', 'SINTIA MONIKA', 'Perempuan', 'KP. BOJONG MALANG RT/02/09 WARUNGKIARA', 'SUKABUMI', '1996-05-28', '', '', '', '', '', 319, '', 8, 1, 2),
(48, '111210243', 'SISKA MAULINA PURNAMASARI', 'Perempuan', 'KP. SEDAMUKTI BOJONG', 'SUKABUMI', '1996-03-10', '', '', '', '', '', 320, '', 8, 1, 2),
(49, '111210244', 'SISKA YULIANA', 'Perempuan', 'BTN CIKEMBANG, CIKEMBAR', 'SUKABUMI', '1996-07-30', '', '', '', '', '', 321, '', 8, 1, 2),
(50, '111210245', 'SITI ARDIANTI', 'Perempuan', 'KP. TEGALPANJANG SUKAMAJU', 'SUKABUMI', '1996-08-12', '', '', '', '', '', 322, '', 8, 1, 2),
(51, '111210246', 'SITI ASPIYANI', 'Perempuan', 'KP. PASIRGABIG BOJONGKEMBAR', 'SUKABUMI', '1996-05-14', '', '', '', '', '', 323, '', 8, 1, 2),
(52, '111210247', 'SITI CHOLIFATUL MIROD', 'Perempuan', 'KP. GENTONG BANTARGADUNG', 'SUKABUMI', '1996-06-05', '', '', '', '', '', 324, '', 8, 1, 2),
(53, '111210248', 'SITI HAMIDAH', 'Perempuan', 'KP. KEBONJERUK RT02/10 SUKAMULYA CIKEMBAR', 'SUKABUMI', '1995-12-20', '', '', '', '', '', 325, '', 2, 1, 2),
(54, '111210249', 'SITI MAYASEKAR', 'Perempuan', 'KP. BANTARJATI RT 33/09 SINDANGRESMI JAMPANGTENGAH', 'SUKABUMI', '1995-10-09', '', '', '', '', '', 326, '', 2, 1, 2),
(55, '111210250', 'SITI NURJANAH', 'Perempuan', 'KP. MEKARSARI BOJONGKEMBAR', 'SUKABUMI', '1996-03-02', '', '', '', '', '', 327, '', 2, 1, 2),
(56, '111210251', 'SITI NURPALAH', 'Perempuan', 'KP. NEGLASARI CIKEMBAR', 'SUKABUMI', '1996-10-13', '', '', '', '', '', 328, '', 2, 1, 2),
(57, '111210252', 'SITI RATNA DESI', 'Perempuan', 'KP. PANGLESERAN RT 01/02 SIRNARESMI', 'SUKABUMI', '1996-07-07', '', '', '', '', '', 329, '', 2, 1, 2),
(58, '111210253', 'SITI SAPNA SAKINAH', 'Perempuan', 'KP. CIBODAS', 'SUKABUMI', '1996-06-18', '', '', '', '', '', 330, '', 2, 1, 2),
(59, '111210254', 'SONIA PUTRI FEBRIANA', 'Perempuan', 'KP. CIBATU RT/01/02 CIBATU CIKEMBAR', 'SUKABUMI', '1995-11-11', '', '', '', '', '', 331, '', 2, 1, 2),
(60, '111210255', 'SOPIA AMALIA RAHMAH', 'Perempuan', 'KP. CIBARENGKOK CIMANGGU CIKEMBAR', 'SUKABUMI', '1996-02-23', '', '', '', '', '', 332, '', 2, 1, 2),
(61, '111210256', 'SRI HANDAYANI', 'Perempuan', 'KP. PANAGAN RT 01/10 DESA SUKAMULYA', 'SUKABUMI', '1997-03-19', '', '', '', '', '', 333, '', 2, 1, 2),
(62, '111210257', 'SRI RAHAYU', 'Perempuan', 'KP. GANG KEMARI PARAKANLIMA', 'SUKABUMI', '1996-10-05', '', '', '', '', '', 334, '', 2, 1, 2),
(63, '111210258', 'SRI RAHAYU', 'Perempuan', 'KP. CIBODAS RT 04/02 BOJONG CIKEMBAR', 'SUKABUMI', '1996-06-28', '', '', '', '', '', 335, '', 2, 1, 2),
(64, '111210259', 'SRI WAHYUNI SAEPUDIN', 'Perempuan', 'KP. PADASUKA RT/RW 01/05 DESA KERTARAHARJA', 'SUKABUMI', '1996-03-13', '', '', '', '', '', 336, '', 2, 1, 2),
(65, '111210260', 'SUCI ALIANISA', 'Perempuan', 'KP. KUBANG RT/03/08 BOJONG CIKEMBAR', 'SUKABUMI', '1996-05-23', '', '', '', '', '', 337, '', 2, 1, 2),
(66, '111210261', 'SUCI ANUGRAH NOORMAN', 'Perempuan', 'KP. PASAR SAPTU', 'SUKABUMI', '1996-05-20', '', '', '', '', '', 338, '', 2, 1, 2),
(67, '111210262', 'SUMINAR MUSTIKASARI', 'Laki-Laki', 'KP. SUKAHARJA', 'SUKABUMI', '1996-05-26', '', '', '', '', '', 339, '', 2, 1, 2),
(68, '111210263', 'SUSANTI', 'Perempuan', 'KP. CIJOLANG WANGUNREJA NYALINDUNG', 'SUKABUMI', '1996-06-18', '', '', '', '', '', 340, '', 2, 1, 2),
(69, '101110046', 'BUNGA SHULHATUL IMAMAH', 'P', 'JLN PELDA KM 20 CIKEMBAR', 'SUKABUMI', '1995-08-06', '', '', '', '', '', 353, '', 7, 1, 1),
(70, '101110047', 'CHANDRA DARUSMAN', 'L', 'KP. CICATIH CIKEMBAR', 'SUKABUMI', '1995-08-07', '', '', '', '', '', 354, '', 7, 1, 1),
(71, '101110048', 'CHICHI PUTRI', 'P', 'KP. SUKAMANTRI RT 01/08 DS. CIKEMBAR KEC. CIKEMBAR', 'SUKABUMI', '1995-08-08', '', '', '', '', '', 355, '', 7, 1, 1),
(72, '101110049', 'CHRISTIANTY BR SEMBIRING', 'P', 'YON ARMED 13 KOSTRAD', 'KUTIAMBARU', '1995-08-09', '', '', '', '', '', 356, '', 7, 1, 1),
(73, '101110050', 'CUCU SUSANTI', 'P', 'KP. CIKATE CIKEMBAR', 'SUKABUMI', '1995-08-10', '', '', '', '', '', 357, '', 7, 1, 1),
(74, '101110051', 'DAHLIANI SEFTIYAN', 'P', 'KP. SUKASIRNA', 'SUKABUMI', '1995-08-11', '', '', '', '', '', 358, '', 7, 1, 1),
(75, '101110052', 'DEA PUTRI PERTIWI', 'P', 'KP. CILANGKAP RT 03/06 CIKEMBAR SUKABUMI', 'SUKABUMI', '1995-08-12', '', '', '', '', '', 359, '', 7, 1, 1),
(76, '101110053', 'DEDE ABDURRAHMAN ARASYID', 'L', 'KP. JERUK NYELAP RT 05/02 LEMBURSITU SUKABUMI', 'SUKABUMI', '1995-08-13', '', '', '', '', '', 360, '', 7, 1, 1),
(77, '101110054', 'DEDE IKSAN RUSWANDI', 'L', 'KP. SIMPENAN CIKEMBAR', 'SUKABUMI', '1995-08-14', '', '', '', '', '', 361, '', 7, 1, 1),
(78, '101110055', 'DEDE LANA BAKTI', 'L', 'KP. BOJONG WARUNG RT 04/06 DS. BOJONG', 'SUKABUMI', '1995-08-15', '', '', '', '', '', 362, '', 7, 1, 1),
(79, '101110056', 'DEDEN AGUSTIAN', 'L', 'KP. CIBARENGKOK RT 03/10 CIMANGGU CIKEMBAR - SUKABUMI', 'SUKABUMI', '1995-08-16', '', '', '', '', '', 363, '', 7, 1, 1),
(81, '101110058', 'DERA AMANDA', 'P', 'KP. SUKAHARJA WARUNGKIARA', 'SUKABUMI', '1995-08-18', '', '', '', '', '', 365, '', 1, 1, 1),
(82, '101110059', 'DESI FAJAR PRATIWI', 'P', 'KP. NANGERANG RT 02/16 KEC LEMBURSITU', 'SUKABUMI', '1995-08-19', '', '', '', '', '', 366, '', 1, 1, 1),
(83, '101110060', 'DESI NURMALASARI', 'P', 'KP. BOJONG WARUNG CIKEMBAR', 'SUKABUMI', '1995-08-20', '', '', '', '', '', 367, '', 1, 1, 1),
(84, '101110061', 'DESTI NOPIANTI', 'P', 'KP. SEDAMUKTI RT 04/09 DS BOJONG', 'SUKABUMI', '1995-08-21', '', '', '', '', '', 368, '', 1, 1, 1),
(85, '101110062', 'DESTI NURHAYATI', 'P', 'KP. SIMPENAN RT 06/07 CIKEMBAR - SUKABUMI', 'SUKABUMI', '1995-08-22', '', '', '', '', '', 369, '', 1, 1, 1),
(87, '101110064', 'DEVI HERMAWAN', 'Perempuan', 'KP. TEGALPANJANG RT03/03 DS. SUKAMAJU CIKEMBAR SUKABUMI', 'SUKABUMI', '1995-08-24', '', '', '', '', '', 371, '', 1, 1, 1),
(88, '3424235', 'USWATUN HASANAH', 'Perempuan', 'KOTEM TORJUN', 'SAMPANG', '2019-01-16', 'SMP TORJUN', 'TORJUN', 'SOLIHIN', 'SOLIHAH', 'KOTEM', 374, 'NO', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quis`
--

CREATE TABLE `quis` (
  `id_quis` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `waktu_quis` datetime DEFAULT NULL,
  `nama_quis` varchar(255) NOT NULL,
  `lama_quis` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quis`
--

INSERT INTO `quis` (`id_quis`, `id_mapel`, `waktu_quis`, `nama_quis`, `lama_quis`) VALUES
(2, 2, NULL, 'KUIS BAHASA', '01:30:00'),
(9, 15, NULL, 'ac', '22:22:22');

-- --------------------------------------------------------

--
-- Table structure for table `rombel`
--

CREATE TABLE `rombel` (
  `id_rombel` int(11) NOT NULL,
  `nama_rombel` varchar(20) NOT NULL,
  `id_jurusan` smallint(6) NOT NULL,
  `id_level_kelas` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rombel`
--

INSERT INTO `rombel` (`id_rombel`, `nama_rombel`, `id_jurusan`, `id_level_kelas`) VALUES
(1, 'A', 1, 1),
(2, 'A', 2, 1),
(3, 'A', 1, 2),
(4, 'A', 2, 2),
(5, 'A', 1, 3),
(6, 'A', 2, 3),
(7, 'B', 1, 1),
(8, 'B', 2, 1),
(9, 'B', 1, 2),
(10, 'B', 2, 2),
(11, 'B', 1, 3),
(12, 'B', 2, 3),
(13, 'C', 1, 1),
(14, 'C', 2, 4),
(15, 'A', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sanksi`
--

CREATE TABLE `sanksi` (
  `id_sanksi` int(11) NOT NULL,
  `sanksi_sanksi` varchar(255) NOT NULL,
  `max_point_sanksi` float NOT NULL,
  `min_point_sanksi` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sanksi`
--

INSERT INTO `sanksi` (`id_sanksi`, `sanksi_sanksi`, `max_point_sanksi`, `min_point_sanksi`) VALUES
(1, 'TEGUR', 2, 1),
(2, 'JUZ %juz%', 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `slot_jam`
--

CREATE TABLE `slot_jam` (
  `id_slot_jam` smallint(6) NOT NULL,
  `nama_slot_jam` varchar(15) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slot_jam`
--

INSERT INTO `slot_jam` (`id_slot_jam`, `nama_slot_jam`, `jam_mulai`, `jam_selesai`) VALUES
(1, 'Ke-1', '07:00:00', '07:37:00'),
(2, 'Ke-2', '07:37:00', '08:14:00'),
(3, 'Ke-3', '08:14:00', '08:51:00'),
(4, 'Ke-4', '08:51:00', '09:28:00'),
(6, 'Istirahat', '10:05:00', '10:25:00'),
(8, 'Ke-5', '09:28:00', '10:05:00'),
(9, 'Ke-6', '10:25:00', '11:02:00'),
(11, 'Ke-7', '11:02:00', '11:39:00'),
(12, 'Ke-8', '11:39:00', '12:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(5) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_quis` int(11) NOT NULL,
  `soal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id_soal`, `id_user`, `id_quis`, `soal`) VALUES
(1, 3, 2, 'bagaimana cara mengatasi saat STRESS?'),
(4, 3, 9, 'HALO...! INI ADALAH CONTOH SOAL YANG TELAH KAMI BUAT!'),
(5, 3, 2, 'wfwe'),
(6, 3, 2, 'bagaimanakah'),
(7, 3, 2, 'fsdfsd'),
(8, 3, 2, 'dfvadscsc');

-- --------------------------------------------------------

--
-- Table structure for table `soal_uas`
--

CREATE TABLE `soal_uas` (
  `id_soal_uas` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `teks_soal_uas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `soal_uas`
--

INSERT INTO `soal_uas` (`id_soal_uas`, `id_paket`, `teks_soal_uas`) VALUES
(12, 1, 'csdc'),
(13, 1, 'vfvsghn'),
(14, 1, 'brgbgbr');

-- --------------------------------------------------------

--
-- Table structure for table `status_siswa`
--

CREATE TABLE `status_siswa` (
  `id_status_siswa` tinyint(4) NOT NULL,
  `nama_status_siswa` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status_siswa`
--

INSERT INTO `status_siswa` (`id_status_siswa`, `nama_status_siswa`) VALUES
(1, 'Aktif'),
(2, 'Lulus'),
(3, 'Drop Out'),
(4, 'Non Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `surat_bk`
--

CREATE TABLE `surat_bk` (
  `id_surat_bk` int(11) NOT NULL,
  `surat_surat_bk` varchar(32) NOT NULL,
  `isi_surat_bk` longtext NOT NULL,
  `tipe_surat_bk` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surat_bk`
--

INSERT INTO `surat_bk` (`id_surat_bk`, `surat_surat_bk`, `isi_surat_bk`, `tipe_surat_bk`) VALUES
(1, 'tak jelas', '&#60p&#62%juz%&#60&#47p&#62&#60p&#62hhh&#60&#47p&#62', 'izin');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `id_surat_keluar` int(11) NOT NULL,
  `no_surat_keluar` varchar(255) NOT NULL,
  `tujuan_surat_keluar` varchar(255) NOT NULL,
  `alamat_surat_keluar` varchar(255) NOT NULL,
  `tanggal_surat_keluar` date NOT NULL,
  `lampiran_surat_keluar` varchar(255) NOT NULL,
  `perihal_surat_keluar` varchar(255) NOT NULL,
  `isi_surat_keluar` longtext NOT NULL,
  `scan_surat_keluar` varchar(255) DEFAULT NULL,
  `id_jenis_surat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surat_keluar`
--

INSERT INTO `surat_keluar` (`id_surat_keluar`, `no_surat_keluar`, `tujuan_surat_keluar`, `alamat_surat_keluar`, `tanggal_surat_keluar`, `lampiran_surat_keluar`, `perihal_surat_keluar`, `isi_surat_keluar`, `scan_surat_keluar`, `id_jenis_surat`) VALUES
(30, 'A01/001/001/SMKI.Tj/II/2019', 'asx', 'dwe', '2019-02-07', 'fw', 'fe', '&#60!DOCTYPE html&#62\n                &#60html&#62\n                &#60head&#62\n                &#60title&#62Window Print&#60&#47title&#62\n                &#60style type=&#34text&#47css&#34&#62\n                \n                @media print {\n                    .media-no-print{\n                        display: none !important&#59\n                    }\n                }\n                \n                &#60&#47style&#62\n                &#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47quill&#47quill.css&#34&#62&#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47css&#47print.css&#34&#62\n                &#60&#47head&#62\n                &#60body onload=&#34window.print()&#59window.close()&#59&#34 &#62\n                \n                \n		&#60div class=&#34print-kop&#34&#62\n		&#60div class=&#34logo&#34&#62\n		&#60img src=&#34http:&#47&#47localhost:4200&#47assets&#47images&#47logo.jpg&#34&#62\n		&#60&#47div&#62\n		&#60div class=&#34line line-1&#34&#62YAYASAN  AL – FURQON TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-2&#34&#62SMK ISLAM TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-3&#34&#62Terakreditasi &#34B&#34&#60&#47div&#62\n		&#60div class=&#34line line-4&#34&#62Kompetensi Keahlian: 1. Teknik Komputer dan Jaringan  2. Teknik Sepeda Motor&#60&#47div&#62\n		&#60div class=&#34line line-5&#34&#62NSM. : 32.2.0527.04.010  &#47  NPSN. : 20570793&#60&#47div&#62\n		&#60div class=&#34line line-6&#34&#62Alamat : Jl. Idaman No. 01 Dharma Tanjung Camplong Sampang 69281&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepala-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Nomor&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: A01&#47001&#47001&#47SMKI.Tj&#47II&#472019&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Lamp.&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: fw&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Perihal&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: &#60span class=&#34perihal&#34&#62fe&#60&#47span&#62&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepada-surat&#34&#62\n		&#60div&#62Kepada Yth. :&#60&#47div&#62\n		&#60div&#62asx&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-alamat-surat&#34&#62\n		&#60div&#62Di&#60&#47div&#62\n		&#60div class=&#34tempat&#34&#62dwe&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-isi-surat&#34&#62\n		&#60div class=&#34ql-editor&#34&#62\n		&#60p&#62&#60em&#62asalamualaiku&#60&#47em&#62&#60&#47p&#62&#60p&#62&#60br&#62&#60&#47p&#62&#60p&#62&#60br&#62&#60&#47p&#62&#60p&#62&#60em&#62------&#60&#47em&#62&#60&#47p&#62&#60p&#62&#60br&#62&#60&#47p&#62&#60p&#62&#60br&#62&#60&#47p&#62&#60p&#62&#60em&#62walaikum salam&#60&#47em&#62&#60&#47p&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-ttd-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34tanggal&#34&#62Sampang, 07 Februari 2019&#60&#47div&#62\n		&#60div class=&#34jabatan&#34&#62Kepala SMK Islam Tanjung&#60&#47div&#62\n		&#60div class=&#34nama&#34&#62JUNAIDI, M.Pd.I&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		\n                &#60&#47body&#62\n                &#60&#47html&#62', NULL, 1),
(31, 'A01/001/002/SMKI.Tj/II/2019', 'csdc', 'vfve', '2019-02-07', 'vsd', 'sds', '&#60!DOCTYPE html&#62\n                &#60html&#62\n                &#60head&#62\n                &#60title&#62Window Print&#60&#47title&#62\n                &#60style type=&#34text&#47css&#34&#62\n                \n                @media print {\n                    .media-no-print{\n                        display: none !important&#59\n                    }\n                }\n                \n                &#60&#47style&#62\n                &#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47quill&#47quill.css&#34&#62&#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47css&#47print.css&#34&#62\n                &#60&#47head&#62\n                &#60body onload=&#34window.print()&#59window.close()&#59&#34 &#62\n                \n                \n		&#60div class=&#34print-kop&#34&#62\n		&#60div class=&#34logo&#34&#62\n		&#60img src=&#34http:&#47&#47localhost:4200&#47assets&#47images&#47logo.jpg&#34&#62\n		&#60&#47div&#62\n		&#60div class=&#34line line-1&#34&#62YAYASAN  AL – FURQON TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-2&#34&#62SMK ISLAM TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-3&#34&#62Terakreditasi &#34B&#34&#60&#47div&#62\n		&#60div class=&#34line line-4&#34&#62Kompetensi Keahlian: 1. Teknik Komputer dan Jaringan  2. Teknik Sepeda Motor&#60&#47div&#62\n		&#60div class=&#34line line-5&#34&#62NSM. : 32.2.0527.04.010  &#47  NPSN. : 20570793&#60&#47div&#62\n		&#60div class=&#34line line-6&#34&#62Alamat : Jl. Idaman No. 01 Dharma Tanjung Camplong Sampang 69281&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepala-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Nomor&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: A01&#47001&#47002&#47SMKI.Tj&#47II&#472019&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Lamp.&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: vsd&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Perihal&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: &#60span class=&#34perihal&#34&#62sds&#60&#47span&#62&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepada-surat&#34&#62\n		&#60div&#62Kepada Yth. :&#60&#47div&#62\n		&#60div&#62csdc&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-alamat-surat&#34&#62\n		&#60div&#62Di&#60&#47div&#62\n		&#60div class=&#34tempat&#34&#62vfve&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-isi-surat&#34&#62\n		&#60div class=&#34ql-editor&#34&#62\n		&#60p&#62cc&#60&#47p&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-ttd-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34tanggal&#34&#62Sampang, 10 Februari 2019&#60&#47div&#62\n		&#60div class=&#34jabatan&#34&#62Kepala SMK Islam Tanjung&#60&#47div&#62\n		&#60div class=&#34nama&#34&#62JUNAIDI, M.Pd.I&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		\n                &#60&#47body&#62\n                &#60&#47html&#62', NULL, 1),
(32, 'A01/002/002/SMKI.Tj/II/2019', 'fsdf', 'ferf', '2019-02-10', 'vdf', 'vdf', '&#60!DOCTYPE html&#62\n                &#60html&#62\n                &#60head&#62\n                &#60title&#62Window Print&#60&#47title&#62\n                &#60style type=&#34text&#47css&#34&#62\n                \n                @media print {\n                    .media-no-print{\n                        display: none !important&#59\n                    }\n                }\n                \n                &#60&#47style&#62\n                &#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47quill&#47quill.css&#34&#62&#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47css&#47print.css&#34&#62\n                &#60&#47head&#62\n                &#60body onload=&#34window.print()&#59window.close()&#59&#34 &#62\n                \n                \n		&#60div class=&#34print-kop&#34&#62\n		&#60div class=&#34logo&#34&#62\n		&#60img src=&#34http:&#47&#47localhost:4200&#47assets&#47images&#47logo.jpg&#34&#62\n		&#60&#47div&#62\n		&#60div class=&#34line line-1&#34&#62YAYASAN  AL – FURQON TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-2&#34&#62SMK ISLAM TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-3&#34&#62Terakreditasi &#34B&#34&#60&#47div&#62\n		&#60div class=&#34line line-4&#34&#62Kompetensi Keahlian: 1. Teknik Komputer dan Jaringan  2. Teknik Sepeda Motor&#60&#47div&#62\n		&#60div class=&#34line line-5&#34&#62NSM. : 32.2.0527.04.010  &#47  NPSN. : 20570793&#60&#47div&#62\n		&#60div class=&#34line line-6&#34&#62Alamat : Jl. Idaman No. 01 Dharma Tanjung Camplong Sampang 69281&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepala-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Nomor&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: A01&#47002&#47002&#47SMKI.Tj&#47II&#472019&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Lamp.&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: vdf&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Perihal&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: &#60span class=&#34perihal&#34&#62vdf&#60&#47span&#62&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepada-surat&#34&#62\n		&#60div&#62Kepada Yth. :&#60&#47div&#62\n		&#60div&#62fsdf&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-alamat-surat&#34&#62\n		&#60div&#62Di&#60&#47div&#62\n		&#60div class=&#34tempat&#34&#62ferf&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-isi-surat&#34&#62\n		&#60div class=&#34ql-editor&#34&#62\n		&#60p&#62huhuhu hehe&#60&#47p&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-ttd-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34tanggal&#34&#62Sampang, 10 Februari 2019&#60&#47div&#62\n		&#60div class=&#34jabatan&#34&#62Kepala SMK Islam Tanjung&#60&#47div&#62\n		&#60div class=&#34nama&#34&#62JUNAIDI, M.Pd.I&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		\n                &#60&#47body&#62\n                &#60&#47html&#62', NULL, 1),
(33, 'A01/003/002/SMKI.Tj/II/2019', '4234', 'ferf', '2019-02-10', 'bss', 'vdf', '&#60!DOCTYPE html&#62\n                &#60html&#62\n                &#60head&#62\n                &#60title&#62Window Print&#60&#47title&#62\n                &#60style type=&#34text&#47css&#34&#62\n                \n                @media print {\n                    .media-no-print{\n                        display: none !important&#59\n                    }\n                }\n                \n                &#60&#47style&#62\n                &#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47quill&#47quill.css&#34&#62&#60link rel=&#34stylesheet&#34 type=&#34text&#47css&#34 href=&#34http:&#47&#47localhost:4200&#47assets&#47tema&#47css&#47print.css&#34&#62\n                &#60&#47head&#62\n                &#60body onload=&#34window.print()&#59window.close()&#59&#34 &#62\n                \n                \n		&#60div class=&#34print-kop&#34&#62\n		&#60div class=&#34logo&#34&#62\n		&#60img src=&#34http:&#47&#47localhost:4200&#47assets&#47images&#47logo.jpg&#34&#62\n		&#60&#47div&#62\n		&#60div class=&#34line line-1&#34&#62YAYASAN  AL – FURQON TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-2&#34&#62SMK ISLAM TANJUNG&#60&#47div&#62\n		&#60div class=&#34line line-3&#34&#62Terakreditasi &#34B&#34&#60&#47div&#62\n		&#60div class=&#34line line-4&#34&#62Kompetensi Keahlian: 1. Teknik Komputer dan Jaringan  2. Teknik Sepeda Motor&#60&#47div&#62\n		&#60div class=&#34line line-5&#34&#62NSM. : 32.2.0527.04.010  &#47  NPSN. : 20570793&#60&#47div&#62\n		&#60div class=&#34line line-6&#34&#62Alamat : Jl. Idaman No. 01 Dharma Tanjung Camplong Sampang 69281&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepala-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Nomor&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: A01&#47003&#47002&#47SMKI.Tj&#47II&#472019&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Lamp.&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: bss&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34label&#34&#62Perihal&#60&#47div&#62\n		&#60div class=&#34isi&#34&#62: &#60span class=&#34perihal&#34&#62vdf&#60&#47span&#62&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-kepada-surat&#34&#62\n		&#60div&#62Kepada Yth. :&#60&#47div&#62\n		&#60div&#624234&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-alamat-surat&#34&#62\n		&#60div&#62Di&#60&#47div&#62\n		&#60div class=&#34tempat&#34&#62ferf&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-isi-surat&#34&#62\n		&#60div class=&#34ql-editor&#34&#62\n		&#60p&#62huhuhu hehe&#60&#47p&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		&#60div class=&#34print-ttd-surat&#34&#62\n		&#60div class=&#34row&#34&#62\n		&#60div class=&#34tanggal&#34&#62Sampang, 10 Februari 2019&#60&#47div&#62\n		&#60div class=&#34jabatan&#34&#62Kepala SMK Islam Tanjung&#60&#47div&#62\n		&#60div class=&#34nama&#34&#62JUNAIDI, M.Pd.I&#60&#47div&#62\n		&#60&#47div&#62\n		&#60&#47div&#62\n		\n                &#60&#47body&#62\n                &#60&#47html&#62', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `id_surat_masuk` int(11) NOT NULL,
  `no_surat_masuk` varchar(255) NOT NULL,
  `instansi_surat_masuk` varchar(255) NOT NULL,
  `alamat_surat_masuk` varchar(255) NOT NULL,
  `tanggal_surat_masuk` date NOT NULL,
  `lampiran_surat_masuk` varchar(255) NOT NULL,
  `perihal_surat_masuk` varchar(255) NOT NULL,
  `scan_surat_masuk` varchar(255) DEFAULT NULL,
  `isi_surat_masuk` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surat_masuk`
--

INSERT INTO `surat_masuk` (`id_surat_masuk`, `no_surat_masuk`, `instansi_surat_masuk`, `alamat_surat_masuk`, `tanggal_surat_masuk`, `lampiran_surat_masuk`, `perihal_surat_masuk`, `scan_surat_masuk`, `isi_surat_masuk`) VALUES
(5, '423432', 'dsc', 'acda', '2019-01-15', 'vadv', 'casd', 'b1272c0aa4859989b8e94a84751f87c7.png', '&#60p&#62bla bla bla&#60&#47p&#62'),
(6, '343242', '12', 'cadca', '2019-02-21', 'csdc', 'qdd', NULL, '&#60p&#62&#60br&#62&#60&#47p&#62');

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id_tagihan` tinyint(4) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tahun_ajaran` tinyint(4) NOT NULL,
  `nama_tagihan` varchar(20) NOT NULL,
  `total_tagihan` int(11) NOT NULL,
  `sisa_tunggakan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_tahun_ajaran` tinyint(4) NOT NULL,
  `nama_tahun_ajaran` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_tahun_ajaran`, `nama_tahun_ajaran`, `status`) VALUES
(1, '2017/2018 Genap', 0),
(2, '2018/2019 Ganjil', 1),
(3, '2018/2019 Genap', 0),
(4, '2019/2020 Ganjil', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nilai_kriteria`
--

CREATE TABLE `tbl_nilai_kriteria` (
  `id_nilai_kriteria` tinyint(4) NOT NULL,
  `id_kriteria_beasiswa` tinyint(4) NOT NULL,
  `nilai_kriteria` tinyint(4) NOT NULL,
  `nilai_normalisasi` tinyint(4) NOT NULL,
  `id_user` int(10) NOT NULL,
  `status_beasiswa` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_nilai_kriteria`
--

INSERT INTO `tbl_nilai_kriteria` (`id_nilai_kriteria`, `id_kriteria_beasiswa`, `nilai_kriteria`, `nilai_normalisasi`, `id_user`, `status_beasiswa`) VALUES
(33, 20, 3, 0, 286, 0);

-- --------------------------------------------------------

--
-- Table structure for table `template_jenis_surat`
--

CREATE TABLE `template_jenis_surat` (
  `id_template_jenis_surat` int(11) NOT NULL,
  `id_jenis_surat` int(11) NOT NULL,
  `nama_template_jenis_surat` varchar(255) NOT NULL,
  `template_jenis_surat` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template_jenis_surat`
--

INSERT INTO `template_jenis_surat` (`id_template_jenis_surat`, `id_jenis_surat`, `nama_template_jenis_surat`, `template_jenis_surat`) VALUES
(3, 1, 'csdcs', '&#60p&#62huhuhu hehe&#60&#47p&#62'),
(6, 1, 'csdcs', '&#60p&#62huhuhu hehe&#60&#47p&#62'),
(7, 1, '323', '&#60p&#62csc&#60&#47p&#62'),
(8, 1, '232', '&#60p&#62cadvafb&#60&#47p&#62');

-- --------------------------------------------------------

--
-- Table structure for table `type_user`
--

CREATE TABLE `type_user` (
  `id_type` tinyint(4) NOT NULL,
  `nama_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_user`
--

INSERT INTO `type_user` (`id_type`, `nama_type`) VALUES
(1, 'Admin'),
(2, 'Guru'),
(3, 'Siswa'),
(4, 'Admin Tabungan'),
(5, 'Admin BK'),
(6, 'Kesiswaan'),
(7, 'Wali Kelas');

-- --------------------------------------------------------

--
-- Table structure for table `upload_file`
--

CREATE TABLE `upload_file` (
  `id_upload_file_berkas` int(10) NOT NULL,
  `ikon_upload_file_berkas` varchar(255) NOT NULL,
  `nama_upload_file_berkas` varchar(255) NOT NULL,
  `ukuran_upload_file_berkas` float NOT NULL,
  `lokasi_upload_file_berkas` varchar(255) NOT NULL,
  `tanggal_upload_file_berkas` datetime NOT NULL,
  `keterangan_upload_file_berkas` text NOT NULL,
  `total_download_upload_file_berkas` int(10) NOT NULL,
  `total_lihat_upload_file_berkas` int(10) NOT NULL,
  `hash_upload_file_berkas` varchar(100) NOT NULL,
  `ekstensi_upload_file_berkas` varchar(7) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_hak_akses_group` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upload_file`
--

INSERT INTO `upload_file` (`id_upload_file_berkas`, `ikon_upload_file_berkas`, `nama_upload_file_berkas`, `ukuran_upload_file_berkas`, `lokasi_upload_file_berkas`, `tanggal_upload_file_berkas`, `keterangan_upload_file_berkas`, `total_download_upload_file_berkas`, `total_lihat_upload_file_berkas`, `hash_upload_file_berkas`, `ekstensi_upload_file_berkas`, `id_user`, `id_hak_akses_group`) VALUES
(1, 'thumbnail/85b22ee02467ccb5840b79a3f635e0a9.png', '2.png', 26947, 'files/373/20190130062049792.png', '2019-01-30 06:20:49', '-', 0, 0, '27f1b2f6799434ec49aee17204fbc7ea', 'png', 373, 2),
(2, 'thumbnail/9533dae5122bd0a8a3af9658320bfef1.png', 'ci.png', 2566, 'files/374/20190130062239740.png', '2019-01-30 06:22:39', '-', 0, 0, '07d084a15baa0052a242abb26cea3fea', 'png', 374, 2),
(3, 'thumbnail/0031fed311adee8458e14042eef85b26.png', 'js.png', 1414, 'files/3/20190130062318715.png', '2019-01-30 06:23:18', '-', 0, 0, '1f5b0e2d4a5d7a4dafe8567d0e3a2ab4', 'png', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `upload_materi`
--

CREATE TABLE `upload_materi` (
  `id_upload_materi` int(5) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `file_upload_materi` varchar(255) NOT NULL,
  `judul_upload_materi` varchar(255) NOT NULL,
  `keterangan_upload_materi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`) VALUES
(1, 'sandi@gmail.com', '$2y$10$8XHMD2OVc0GIaUhOHK0FJ.m8SyxO45npKbICW/lZXv9t71VPq73pS'),
(2, 'andi@gmail.com', 'tes125'),
(3, 'guru@gmail.com', '$2y$10$crYWGnQpRB6J64q9T3BxYukGISzEt4eZ/5vR.eOEUuXPN1UlRPV.6'),
(4, 'ainun@gmail.com', 'ainun123'),
(6, 'herman@gmail.com', 'ainun123'),
(8, 'amin_rois@gmail.com', '$2y$10$eEaR0b1Nt5huxOdUKAm1lOquEjgqD9KTR1W.O5WtbNH06HC9st.6.'),
(10, 'danang_darto@gmail.com', 'danangeuy'),
(11, 'rahmad_subaidi@gmail.com', '$2y$10$IiMDQqaGhCeu6Q8uJqK3uOG7WZLFfdCb58HN9nRcSccbHPAQZNM9C'),
(22, 'sitimutmain@gmail.com', '$2y$10$RjViPaAjRJ6ZwRIAtBHSGeW1i4faQe2LgN3VgZZG.mCIJXETx3n5O'),
(29, 'subaidi@gmail.com', '$2y$10$AfQVz4okAZR1ITmbOu0v6O2KvWFpv/Fq3QbJ3faULu.KnSSv4xomi'),
(30, 'dewi@gmail.com', '$2y$10$w8Lo4dAwDGHDDX4/jQFm5.WT2yu0Opu3L1cE/xevcTyOGeN6Uq.vi'),
(31, 'sintia@gmail.com', '$2y$10$iJFNMRxu0fH/i8dWdPg1Suf8CmqpR6geGMtRtLa3tu2bHQobvdXpy'),
(32, 'dian@gmail.com', '$2y$10$jFHQdyddbb8gkF3pE2bSUOVWVk7Z4X.BNQV2tseEnRnlRJQKwSsMW'),
(33, 'kumala@gmail.com', '$2y$10$ZAGfh00atJ5ndHf0yCVGCe4Ff7Vmi/ybJnrbFy5LM6KUlpn1jPpLK'),
(284, 'anjay@gmail.com', '$2y$10$T/Jihr1Ca4Yt4H2Oz05nJ./h6Uy.wtIGA2eqjTkymkGeLLRlMd/3W'),
(285, 'lipeng@me.com', '$2y$10$F4wva/AsnhI.ZL3wQrK0gO68qVcs5QhZhPZY5pg4fN7KzJ/uFgwK.'),
(286, 'greear@verizon.net', '$2y$10$EnC/bym2.TrCaZW7gqAh6OACG3YX7YKLUfB5gsO6Nwh9IEo.QrTVO'),
(287, 'mailarc@optonline.net', '$2y$10$RZq8725jSi2uIFHarZ9KYOBNxUisXyQNtbnNen2sLEzykazwHkXUa'),
(288, 'marin@live.com', '$2y$10$t0dV7Dw5EW8Fifvep63pnu7IHfXVMk.wrg5/Yl8tsemNopTe.BTVy'),
(289, 'ajlitt@msn.com', '$2y$10$1KQ1qFrTPoUU92MPLMd4Ku/kwMl/eQxUxZxfoWYWSUmrO139/JUmW'),
(290, 'gilmoure@att.net', '$2y$10$i0HRof/Sm5jAqhaxXoeMLuOh3/ZqfOao0wMTafwU9U6T8ngw8zQ8q'),
(291, 'harryh@aol.com', '$2y$10$0Ny519CeCdLJg7bafMuj0eWazhEF1m1ipQVR69fRbuwr1spWbTyVu'),
(292, 'emmanuel@optonline.net', '$2y$10$iALDA9zl1dNO50zHJahwi.R8UjhHpoJvILi.mwN0.DyQ6fduCe8Cq'),
(293, 'jaffe@sbcglobal.net', '$2y$10$AlU0cYZ4o4yV8Lx8AWnSxeCPDYEvLEaNExmy4PD6xee.u1JvFi1Em'),
(294, 'pierce@sbcglobal.net', '$2y$10$GTfM7edKD3tYIl20kdDyneHl90SoU7d./.tWdEFE13MafN5X.myDi'),
(295, 'fairbank@me.com', '$2y$10$j3U35Gdx/KMBMrmStl2rE.QdmHOeQ5nSxNZ.aG5YSLEvglvF6TUza'),
(296, 'gfody@sbcglobal.net', '$2y$10$jjbAp6taOeX5a4UqwLdY7.cpAAUzGSV9.JxbU1n4O4iYRF.dPsGdS'),
(297, 'jaffe@yahoo.ca', '$2y$10$.6dzXyst0lCqB6hP4iEtgO4bUJaeMHsTIg/AciAynZAKECZNo5i1G'),
(298, 'airship@comcast.net', '$2y$10$006In20dLsSo5V58PihtLu6OaRvsOcj3QSSHi64sHw28zGfyvcO9y'),
(299, 'stecoop@me.com', '$2y$10$o8JPtrZYtVtqPQr5t8/X3uTXUPXwLVMknRpoGKTSKioZi7K4vTxMi'),
(300, 'facet@outlook.com', '$2y$10$Y6jAQYr8TBmWy8s9FAkszO2iabFf2cabpjhkjcq8bZ3Q2beuvFy.q'),
(301, 'gerlo@hotmail.com', '$2y$10$V73/NGOjIV7H0i/b79hLr.Ra0iOPpqOL4/BIIRtp7u9shmCtv7ZLq'),
(302, 'tellis@outlook.com', '$2y$10$M84jnCqBkjgYZehkP3dWBeJNJz0KFsh9dds5W6Tj89KLFRs6f3x4S'),
(303, 'pereinar@live.com', '$2y$10$yCd4M0ghzNhZMXVR8.kkM..N5BlcQYADC1LGKb482lQrqyUZZAX6.'),
(304, 'murty@mac.com', '$2y$10$K30Novtevqs7KMnpLg35guStSDM0lNAPoiviiyLVbQ90caOgM1b4W'),
(305, 'johnbob@outlook.com', '$2y$10$cCoLeFL0pQ/1wgGG.VE4/u3gYWM6xDzoHl.ZPK5.p.VyGZ4KER4F2'),
(306, 'errxn@msn.com', '$2y$10$JWCDsbklED8xFh8/tIXk8epGqOydT.58OyYP.0MjhtgcJ7Ufl663e'),
(307, 'chrisk@att.net', '$2y$10$/ZF70.uTS/zg5vNbW4DvmuchWsB2fLhWEmVw35cXcr/E4J5Lsydqm'),
(308, 'nogin@yahoo.ca', '$2y$10$JFb51Q31Gxt..u7DW.hi2.3DjE25ozbEsZ4SGegj6twE9c/ZP8OYa'),
(309, 'chinthaka@yahoo.ca', '$2y$10$4CjFjpINFPmyWc8WPU9iIevM4UF1pxaWerN3BqBEYRfexlve6K9Jm'),
(310, 'szymansk@mac.com', '$2y$10$3xHDskk/g81gE/jSMcGJe.otEqm6O1G.JsrYER6/LyVImiMeref02'),
(311, 'wonderkid@yahoo.ca', '$2y$10$JHHgpnds3aoiiECcfR62Ku3CysXkXJOEka2N.QpvVO9xGIBstWxyy'),
(312, 'karasik@optonline.net', '$2y$10$.sjxY9V.OV3Jf2VrOZU.CO1yd1jCAMNN80UyNr81L0y/xYqr836oS'),
(313, 'dbindel@me.com', '$2y$10$ZiFsIrRosYt40h2/Exwiwe/hdpV394sZdD7O/X/pu64LjwtteHHD.'),
(314, 'jfmulder@outlook.com', '$2y$10$R..hnPPHB5gdlacaQe02zOZ9GRjxMA/zoqEuv4iSlEgCNe9Q8h956'),
(315, 'zeitlin@sbcglobal.net', '$2y$10$BOrH344CimkjT/tw4VWMJOqZw30BxC9lQyv5xDip8u6N1N9UEaBJW'),
(316, 'eurohack@optonline.net', '$2y$10$J0xCGEbp/yNOTwjlcVdxIOkk4fOrzf7.1l1/GJKZfkc6Q0GzRzeOW'),
(317, 'muzzy@optonline.net', '$2y$10$NT6t775CipZ8E3GuIsJ4/OMakBXi9qpP/3mEfQIGosVuJ5W/OSO5i'),
(318, 'lipeng@msn.com', '$2y$10$RUYpxs2Gvnyj0YMqqVMEEufFYPVTn1kkS12ukelAxMNN0pGP/47KW'),
(319, 'jbryan@yahoo.com', '$2y$10$4biO.o1U6hVylKtEU.wr8uqSiEr7ZjW4X3tD/RmkIqFSHNPd2ADpu'),
(320, 'howler@hotmail.com', '$2y$10$tbBXJ09HgHwIHo2Uro9GNenwiK6xiVHAQPq/JmJ25qMBO0lpU.dfO'),
(321, 'iapetus@yahoo.com', '$2y$10$TFHlVMMlwEcHL.IscN3EyeS.BPye.wYQjBeOi2pIsDRIXWFdT6HUG'),
(322, 'presoff@aol.com', '$2y$10$OBBv9TK74mvm9fN45SXnfOtNpOn3WdLQ0PHL8AEMV4G3/V.qoxB7C'),
(323, 'mailarc@live.com', '$2y$10$dlMki9FS9CcnTB9PeKMDJeUhVC757JxTvEEbXcRfhOa1LONAyNEGm'),
(324, 'kingjoshi@att.net', '$2y$10$kA9j8JpI4MzAVM3U0JpHhu7IMcPiWubv5dErOCHpi4dxWLTbSc5J.'),
(325, 'cumarana@sbcglobal.net', '$2y$10$4AqFY7TEPducnQlxP7DGf.CI7N9Ke/vcDrU/h4G.MhdAr.DCd8raK'),
(326, 'lbaxter@mac.com', '$2y$10$PpyGAqf.GCjuMKKFbV4OZeU0HlTnTp9OCGOHR.OpWA5IOsbMG0mn2'),
(327, 'library@yahoo.ca', '$2y$10$x/Y12LEPFj9gbq2n7JrKJ.7mLRvxB74jUlp0e7Rcv80m2h/eIJ2Au'),
(328, 'kspiteri@aol.com', '$2y$10$sEZJ/389AZeHXxu2kfKlGuMYV2D9CKHECU.FpIhjhFuCIIwOGY6MW'),
(329, 'dkrishna@me.com', '$2y$10$N0iVJGb/bv8ZKx9F/atB2ecRgHl1CHEiSyrBJimOWunwSYBOx5db6'),
(330, 'dougj@outlook.com', '$2y$10$eXSmnpfG3QRPgiM.mugaruSfKQvM35yQGqafHSb2ltane3sQ1isu.'),
(331, 'weazelman@outlook.com', '$2y$10$xaDJ7eJKM5MQ/JF2.6Mz1Oo0A/4sCal2mSvPkW2Vb1zBXhavJ53yy'),
(332, 'eminence@live.com', '$2y$10$kNoGGm1ysih8ax/dmpwZb.3YsyBtkOC0pxk0kO3M.ZZrpSl5aDHS2'),
(333, 'kmself@yahoo.com', '$2y$10$.wI/c4dt6z5HeilXmUlE8.p0wgEyV/ATXTvigrpQa8ZVB39jbxIQ6'),
(334, 'rande@icloud.com', '$2y$10$mUc2cQm55O9jz5G38lQKQur5kGdTgvpcmpO3A6Bf0zBarsPcGLKy.'),
(335, 'ateniese@me.com', '$2y$10$RVugUFzhTt1p.CEfh6XVsOeG4dFs88SNv03vXybkQNf12I86.zhQG'),
(336, 'mpiotr@att.net', '$2y$10$4sTPDr0WHjKa13Q5IT14WeHBWRt7c./gcwO0/ilHvWDpEddumhQmm'),
(337, 'wagnerch@outlook.com', '$2y$10$WMLsAwJ9KlahsBQ0mhgTDO2VTbLd45vXEo.fHDdA87nql6b/q8aVy'),
(338, 'wiseb@comcast.net', '$2y$10$PUthTzEpGp5tr34r4a0oT.ECR5UtxMaz2ZJSlZJKj6KrbmiYGrgKq'),
(339, 'mirod@optonline.net', '$2y$10$OIpMgB6ewX/gBfD30JqDHeB0IR86EhSCTqP62Vea/WHnd19vob84a'),
(340, 'cumarana@yahoo.ca', '$2y$10$umhOrvGo6UmueBFa1e0eLu4ubqV/nYNHO06SwXezihKea.RrBsz1i'),
(341, 'jguyer@verizon.net', '$2y$10$v9GlEoPVRmMlfsx5EvkBb.o1T9oP432O13631WOzQ5feqxyrORY5a'),
(342, 'nelson@sbcglobal.net', '$2y$10$4o/fobzlWromE08I.clCquTZRZ1hnkjZl4qHFhvQK9QRbvyE1gkeq'),
(343, 'amichalo@gmail.com', '$2y$10$ZgvKM9OwMMJ.FkffvuCJa.a50JHuBd2a3YpLpMFb0UleADRVol2Qu'),
(344, 'konit@comcast.net', '$2y$10$kUwcnVqVAOI89N4fr4MuI.Mkue5SOn2YNFOWmMtnK7U1hgVnxrnRu'),
(345, 'chrwin@icloud.com', '$2y$10$cN/XscXltxvsTU3h6Ah6/.QI67tJaRfIp6/ceH0sn578Wn5yDpjO6'),
(346, 'barjam@verizon.net', '$2y$10$p7gGsYaXHxVvG3xcpLZAtOq2xMG7GipCyVDXaWX/FFzTPJAAh5IIy'),
(347, 'parasite@comcast.net', '$2y$10$P.vdwN8ccofGyiEyIBzMPeYR9G28ALPAY0kW/9.uEI7./UyS0AKs6'),
(348, 'kannan@comcast.net', '$2y$10$rFIie2FS.PBEelyG0mItouG6irixs1CsVanFhsQpAuxD/9wRHgvJG'),
(349, 'esokullu@optonline.net', '$2y$10$3Y1UpWA9m..dJU7Lwj392unHQRK/lFDNfmVZxHny0MQvz2j5xa7Gq'),
(350, 'gospodin@outlook.com', '$2y$10$PyAgd4Pz307jOxVL81.mTO96ojhAktgL76GEW.NtNnlOLffadCN02'),
(351, 'ehood@att.net', '$2y$10$uybKEerqw0.lHLAjlr1F4uzRVEy9Fp8jDBnHgczhELa9riQRPVIqW'),
(352, 'bowmanbs@outlook.com', '$2y$10$VSIuKFjer6poJL.RHdHFOuG/oGlhBIvFELcK9MjQARalZfxJNK.ia'),
(353, 'geeber@att.net', '$2y$10$2tc4dVHwzs2BLefnbkE7seIhGwWdHoL/.mkWPmgtvr9Pyv75jDGOS'),
(354, 'grothoff@aol.com', '$2y$10$VreGUcJsPDTuvhVbI8FSeORADpNGjRH5NYHRXQRw7GxbL4JRr5DG2'),
(355, 'richard@hotmail.com', '$2y$10$0lsuvXUh7GIBqUd3CbFyauemdjJEwVALOSsB5s31k/7VRy0fMrzLq'),
(356, 'aegreene@msn.com', '$2y$10$OMABB5n5TBDROYjXYPnAU.8RAdVvVlk4qGtY69ySiG23rIJEDc6iO'),
(357, 'malvar@hotmail.com', '$2y$10$RErxOyqW8bZ9NEfGINiaz.T7AqQ2v.TyhguuUfT6isADAV5mWKDBS'),
(358, 'joehall@msn.com', '$2y$10$PlXRBZPcvF8JovlQPh8xfuzcrgjFkuxhT0DFf19GSrJ/D.AcWPOOi'),
(359, 'redingtn@yahoo.ca', '$2y$10$YrdKbjUSy2Xi4XIGwxTyVOZSBKzYb85TEUzXRZkkZnOOckMmKAUKu'),
(360, 'eminence@icloud.com', '$2y$10$vjIzcjKJM.ZsOfncGpDWcOicYDQWw0D9v1kKshP5V5UoHhc3EsVLy'),
(361, 'fmerges@icloud.com', '$2y$10$6/Eja3s.VZI3HRcYJTyO8OjIzxTGwGMCTrCKIWzSk75MKJB8n.dcu'),
(362, 'claesjac@optonline.net', '$2y$10$UsWDm4NTyD4oYkfchaP27eApIYc2aCvsh.DabFNcDsZtFikqJWDUG'),
(363, 'keiji@outlook.com', '$2y$10$zC4MPOIMTRGWwiji4hr15ewyTlB3sQ6SiaDxrQZgSCOfJXPJEqqQa'),
(365, 'eegsa@icloud.com', '$2y$10$aJTCT2i6aVkFOrKp1DbeL.3lJvsvWuTVVoPmRb40r75qYPdJ1Vn6O'),
(366, 'markjugg@gmail.com', '$2y$10$qHM4FXwSdTH8AJ/NuUQeyejqN2SpdXqsK57wnVFUHCrMoGJ1AVy8W'),
(367, 'jeffcovey@aol.com', '$2y$10$JRr/R4T7ew2kpleaXng4xu0wBIK3D8NTZvlGS24YCI.L/NoIwsbaG'),
(368, 'notaprguy@msn.com', '$2y$10$.5leN47c1expFztXcHgPmuShcYXZGUJzgIa6nd.52bzSHfdedSezG'),
(369, 'mdielmann@verizon.net', '$2y$10$DpDcBUqDJaRD./0lwa26ve8YxdqJNVc/oYOc04kSLduJRxdZ3RqE2'),
(371, 'hillct@yahoo.com', '$2y$10$jRcnpivxLRHEdUVazA1ZaOJNZ423IEs.mm3mfo0qWi9sw6nzuAyYy'),
(373, 'admin@gmail.com', '$2y$10$PwiklbpmPmb1lJ0T9NLuO.NkfghXCQ/6P93PnMGcarOX22G7CRnyS'),
(374, 'siswa@gmail.com', '$2y$10$eVwswZWM99ci5VzC38VkrepewzhBAz0LOMGGYRl/gyIUudiHbDaWe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adisposisi`
--
ALTER TABLE `adisposisi`
  ADD PRIMARY KEY (`id_adisposisi`);

--
-- Indexes for table `arsip_surat`
--
ALTER TABLE `arsip_surat`
  ADD PRIMARY KEY (`id_arsip_surat`);

--
-- Indexes for table `arsip_surat_detail`
--
ALTER TABLE `arsip_surat_detail`
  ADD PRIMARY KEY (`id_arsip_surat_detail`),
  ADD KEY `id_surat_keluar` (`id_surat_keluar`),
  ADD KEY `id_arsip_surat` (`id_arsip_surat`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`id_biaya`);

--
-- Indexes for table `disposisi`
--
ALTER TABLE `disposisi`
  ADD PRIMARY KEY (`id_disposisi`),
  ADD KEY `suratmasuk_disposisi` (`id_surat_masuk`),
  ADD KEY `id_adisposisi` (`id_adisposisi`);

--
-- Indexes for table `file_audio`
--
ALTER TABLE `file_audio`
  ADD PRIMARY KEY (`id_file_audio`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id_hak_akses`);

--
-- Indexes for table `hak_akses_group`
--
ALTER TABLE `hak_akses_group`
  ADD PRIMARY KEY (`id_hak_akses_group`),
  ADD KEY `id_hak_akses` (`id_hak_akses`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `slot_jam_jadwal` (`id_slot_jam`),
  ADD KEY `plot_ajar_jadwal` (`id_plot_ajar`),
  ADD KEY `tahun_ajaran_jadwal` (`id_tahun_ajaran`);

--
-- Indexes for table `jadwal_alarm`
--
ALTER TABLE `jadwal_alarm`
  ADD PRIMARY KEY (`id_jadwal_alarm`),
  ADD KEY `fileaudio_jadwalalarm` (`id_file_audio`);

--
-- Indexes for table `jadwal_susulan_uas`
--
ALTER TABLE `jadwal_susulan_uas`
  ADD PRIMARY KEY (`id_jadwal_susulan_uas`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_jadwal_uas` (`id_jadwal_uas`);

--
-- Indexes for table `jadwal_uas`
--
ALTER TABLE `jadwal_uas`
  ADD PRIMARY KEY (`id_jadwal_uas`),
  ADD KEY `mapel_jadwaluas` (`id_mapel`),
  ADD KEY `user_jadwaluas` (`id_user`),
  ADD KEY `rombel_jadwaluas` (`id_rombel`),
  ADD KEY `paket_jadwaluas` (`id_paket`),
  ADD KEY `id_tahun_ajaran` (`id_tahun_ajaran`);

--
-- Indexes for table `jawaban_siswa`
--
ALTER TABLE `jawaban_siswa`
  ADD PRIMARY KEY (`id_jawaban_siswa`),
  ADD KEY `soal_jawabansiswa` (`id_soal`),
  ADD KEY `pilihanjawaban_jawabansiswa` (`id_pilihan_jawaban`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `jawaban_uas`
--
ALTER TABLE `jawaban_uas`
  ADD PRIMARY KEY (`id_jawaban_uas`),
  ADD KEY `soal_jawabanuas` (`id_soal_uas`);

--
-- Indexes for table `jawaban_uas_siswa`
--
ALTER TABLE `jawaban_uas_siswa`
  ADD PRIMARY KEY (`id_jawaban_uas_siswa`),
  ADD KEY `soal_jawabanuassiswa` (`id_soal_uas`),
  ADD KEY `user_jawabanuassiswa` (`id_user`),
  ADD KEY `jadwaluas_jawabanuassiswa` (`id_jadwal_uas`),
  ADD KEY `jawabanuas_jawabanuassiswa` (`id_jawaban_uas`);

--
-- Indexes for table `jenis_file`
--
ALTER TABLE `jenis_file`
  ADD PRIMARY KEY (`id_jenis_file`);

--
-- Indexes for table `jenis_surat`
--
ALTER TABLE `jenis_surat`
  ADD PRIMARY KEY (`id_jenis_surat`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kandidat`
--
ALTER TABLE `kandidat`
  ADD PRIMARY KEY (`id_kandidat`),
  ADD KEY `user_kandidat` (`id_user`);

--
-- Indexes for table `kategori_beasiswa`
--
ALTER TABLE `kategori_beasiswa`
  ADD PRIMARY KEY (`id_kategori_beasiswa`),
  ADD KEY `tahun_ajaran_kategori_beasiswa` (`id_tahun_ajaran`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_komentar`),
  ADD KEY `mapel_komentar` (`id_mapel`),
  ADD KEY `user_komentar` (`id_user`);

--
-- Indexes for table `kriteria_beasiswa`
--
ALTER TABLE `kriteria_beasiswa`
  ADD PRIMARY KEY (`id_kriteria_beasiswa`),
  ADD KEY `kategori_beasiswa_kriteria_beasiswa` (`id_kategori_beasiswa`);

--
-- Indexes for table `kunci_jawaban`
--
ALTER TABLE `kunci_jawaban`
  ADD PRIMARY KEY (`id_kunci_jawaban`),
  ADD KEY `soal_kuncijawaban` (`id_soal`),
  ADD KEY `pilihanjawaban_kuncijawaban` (`id_pilihan_jawaban`);

--
-- Indexes for table `level_kelas`
--
ALTER TABLE `level_kelas`
  ADD PRIMARY KEY (`id_level_kelas`);

--
-- Indexes for table `level_user`
--
ALTER TABLE `level_user`
  ADD PRIMARY KEY (`id_level_user`),
  ADD KEY `user_level_user` (`id_user`),
  ADD KEY `type_user_leveluser` (`id_type`);

--
-- Indexes for table `log_rombel`
--
ALTER TABLE `log_rombel`
  ADD PRIMARY KEY (`id_log_rombel`),
  ADD KEY `user_log_rombel` (`id_user`),
  ADD KEY `tahun_ajaran_log_rombel` (`id_tahun_ajaran`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`),
  ADD KEY `jurusan_mapel` (`id_jurusan`),
  ADD KEY `level_kelas_mapel` (`id_level_kelas`);

--
-- Indexes for table `masa_pemilu`
--
ALTER TABLE `masa_pemilu`
  ADD PRIMARY KEY (`id_masa_pemilu`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `rombel_materi` (`id_rombel`),
  ADD KEY `user_materi` (`id_user`),
  ADD KEY `mapel_materi` (`id_mapel`);

--
-- Indexes for table `nilai_quis`
--
ALTER TABLE `nilai_quis`
  ADD PRIMARY KEY (`id_nilai_quis`),
  ADD KEY `user_nilaiquis` (`id_user`),
  ADD KEY `quis_nilaiquis` (`id_quis`);

--
-- Indexes for table `nilai_uas`
--
ALTER TABLE `nilai_uas`
  ADD PRIMARY KEY (`id_nilai_uas`),
  ADD KEY `user_nilaiuas` (`id_user`),
  ADD KEY `jadwaluas_nilaiuas` (`id_jadwal_uas`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`);

--
-- Indexes for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  ADD PRIMARY KEY (`id_pelanggaran`);

--
-- Indexes for table `pelanggaran_siswa`
--
ALTER TABLE `pelanggaran_siswa`
  ADD PRIMARY KEY (`id_pelanggaran_siswa`),
  ADD KEY `user_pelanggaransiswa` (`id_user`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `user_pembayaran` (`id_user`),
  ADD KEY `tagihan_pembayaran` (`id_tagihan`);

--
-- Indexes for table `pemilihan`
--
ALTER TABLE `pemilihan`
  ADD PRIMARY KEY (`id_pemilihan`),
  ADD KEY `kandidat_pemilihan` (`id_kandidat`),
  ADD KEY `masapemilu_pemilihan` (`id_masa_pemilu`),
  ADD KEY `user_pemilihan` (`id_user`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`label_pengaturan`);

--
-- Indexes for table `perizinan_siswa`
--
ALTER TABLE `perizinan_siswa`
  ADD PRIMARY KEY (`id_perizinan_siswa`),
  ADD KEY `user_pelanggaransiswa` (`id_user`);

--
-- Indexes for table `peserta_beasiswa`
--
ALTER TABLE `peserta_beasiswa`
  ADD PRIMARY KEY (`id_peserta_beasiswa`),
  ADD KEY `pesertabeasiswaiduser` (`id_user`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  ADD PRIMARY KEY (`id_pilihan_jawaban`),
  ADD KEY `soal_pilihanjawaban` (`id_soal`);

--
-- Indexes for table `pilih_biaya`
--
ALTER TABLE `pilih_biaya`
  ADD PRIMARY KEY (`id_pilih_biaya`),
  ADD KEY `biaya_pilih` (`id_biaya`),
  ADD KEY `tagihan_pilih` (`id_tagihan`);

--
-- Indexes for table `plot_ajar`
--
ALTER TABLE `plot_ajar`
  ADD PRIMARY KEY (`id_plot_ajar`),
  ADD KEY `rombel_plot_ajar` (`id_rombel`),
  ADD KEY `mapel_plot_ajar` (`id_mapel`),
  ADD KEY `user_plot_ajar` (`id_user`),
  ADD KEY `tahun_ajaran_plot_ajar` (`id_tahun_ajaran`);

--
-- Indexes for table `profile_gukar`
--
ALTER TABLE `profile_gukar`
  ADD PRIMARY KEY (`id_profile_gukar`),
  ADD KEY `user_profile_gukar` (`id_user`);

--
-- Indexes for table `profile_siswa`
--
ALTER TABLE `profile_siswa`
  ADD PRIMARY KEY (`id_profile_siswa`),
  ADD KEY `user_profile_siswa` (`id_user`),
  ADD KEY `rombel_profile_siswa` (`id_rombel`),
  ADD KEY `status_siswa_profile_siswa` (`id_status_siswa`),
  ADD KEY `jurusan_profile_siswa` (`id_jurusan`);

--
-- Indexes for table `quis`
--
ALTER TABLE `quis`
  ADD PRIMARY KEY (`id_quis`),
  ADD KEY `mapel_quis` (`id_mapel`);

--
-- Indexes for table `rombel`
--
ALTER TABLE `rombel`
  ADD PRIMARY KEY (`id_rombel`),
  ADD KEY `jurusan_rombel` (`id_jurusan`),
  ADD KEY `level_kelas_rombel` (`id_level_kelas`);

--
-- Indexes for table `sanksi`
--
ALTER TABLE `sanksi`
  ADD PRIMARY KEY (`id_sanksi`);

--
-- Indexes for table `slot_jam`
--
ALTER TABLE `slot_jam`
  ADD PRIMARY KEY (`id_slot_jam`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `user_soal` (`id_user`),
  ADD KEY `quis_soal` (`id_quis`);

--
-- Indexes for table `soal_uas`
--
ALTER TABLE `soal_uas`
  ADD PRIMARY KEY (`id_soal_uas`),
  ADD KEY `paket_soaluas` (`id_paket`);

--
-- Indexes for table `status_siswa`
--
ALTER TABLE `status_siswa`
  ADD PRIMARY KEY (`id_status_siswa`);

--
-- Indexes for table `surat_bk`
--
ALTER TABLE `surat_bk`
  ADD PRIMARY KEY (`id_surat_bk`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD PRIMARY KEY (`id_surat_keluar`),
  ADD KEY `jenissurat_suratkeluar` (`id_jenis_surat`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`id_surat_masuk`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id_tagihan`),
  ADD KEY `user_tagihan` (`id_user`),
  ADD KEY `tahun_ajaran_tagihan` (`id_tahun_ajaran`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_tahun_ajaran`);

--
-- Indexes for table `tbl_nilai_kriteria`
--
ALTER TABLE `tbl_nilai_kriteria`
  ADD PRIMARY KEY (`id_nilai_kriteria`),
  ADD KEY `kriteria_beasiswa_nilai_kriteria` (`id_kriteria_beasiswa`),
  ADD KEY `peserta_beasiswa_nilai_kriteria` (`id_user`);

--
-- Indexes for table `template_jenis_surat`
--
ALTER TABLE `template_jenis_surat`
  ADD PRIMARY KEY (`id_template_jenis_surat`),
  ADD KEY `id_jenis_surat` (`id_jenis_surat`);

--
-- Indexes for table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`id_type`);

--
-- Indexes for table `upload_file`
--
ALTER TABLE `upload_file`
  ADD PRIMARY KEY (`id_upload_file_berkas`),
  ADD KEY `user_uploadfileberkas` (`id_user`),
  ADD KEY `hakakses_uploadfileberkas` (`id_hak_akses_group`);

--
-- Indexes for table `upload_materi`
--
ALTER TABLE `upload_materi`
  ADD PRIMARY KEY (`id_upload_materi`),
  ADD KEY `mapel_uploadmateri` (`id_mapel`),
  ADD KEY `user_uploadmateri` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adisposisi`
--
ALTER TABLE `adisposisi`
  MODIFY `id_adisposisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `arsip_surat`
--
ALTER TABLE `arsip_surat`
  MODIFY `id_arsip_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `arsip_surat_detail`
--
ALTER TABLE `arsip_surat_detail`
  MODIFY `id_arsip_surat_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `biaya`
--
ALTER TABLE `biaya`
  MODIFY `id_biaya` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `disposisi`
--
ALTER TABLE `disposisi`
  MODIFY `id_disposisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `file_audio`
--
ALTER TABLE `file_audio`
  MODIFY `id_file_audio` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id_hak_akses` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hak_akses_group`
--
ALTER TABLE `hak_akses_group`
  MODIFY `id_hak_akses_group` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `jadwal_alarm`
--
ALTER TABLE `jadwal_alarm`
  MODIFY `id_jadwal_alarm` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_susulan_uas`
--
ALTER TABLE `jadwal_susulan_uas`
  MODIFY `id_jadwal_susulan_uas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_uas`
--
ALTER TABLE `jadwal_uas`
  MODIFY `id_jadwal_uas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `jawaban_siswa`
--
ALTER TABLE `jawaban_siswa`
  MODIFY `id_jawaban_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jawaban_uas`
--
ALTER TABLE `jawaban_uas`
  MODIFY `id_jawaban_uas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `jawaban_uas_siswa`
--
ALTER TABLE `jawaban_uas_siswa`
  MODIFY `id_jawaban_uas_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `jenis_file`
--
ALTER TABLE `jenis_file`
  MODIFY `id_jenis_file` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jenis_surat`
--
ALTER TABLE `jenis_surat`
  MODIFY `id_jenis_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kandidat`
--
ALTER TABLE `kandidat`
  MODIFY `id_kandidat` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_beasiswa`
--
ALTER TABLE `kategori_beasiswa`
  MODIFY `id_kategori_beasiswa` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_komentar` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kriteria_beasiswa`
--
ALTER TABLE `kriteria_beasiswa`
  MODIFY `id_kriteria_beasiswa` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `kunci_jawaban`
--
ALTER TABLE `kunci_jawaban`
  MODIFY `id_kunci_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `level_kelas`
--
ALTER TABLE `level_kelas`
  MODIFY `id_level_kelas` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `level_user`
--
ALTER TABLE `level_user`
  MODIFY `id_level_user` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `log_rombel`
--
ALTER TABLE `log_rombel`
  MODIFY `id_log_rombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `masa_pemilu`
--
ALTER TABLE `masa_pemilu`
  MODIFY `id_masa_pemilu` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id_materi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nilai_quis`
--
ALTER TABLE `nilai_quis`
  MODIFY `id_nilai_quis` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nilai_uas`
--
ALTER TABLE `nilai_uas`
  MODIFY `id_nilai_uas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pelanggaran`
--
ALTER TABLE `pelanggaran`
  MODIFY `id_pelanggaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pelanggaran_siswa`
--
ALTER TABLE `pelanggaran_siswa`
  MODIFY `id_pelanggaran_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pemilihan`
--
ALTER TABLE `pemilihan`
  MODIFY `id_pemilihan` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perizinan_siswa`
--
ALTER TABLE `perizinan_siswa`
  MODIFY `id_perizinan_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `peserta_beasiswa`
--
ALTER TABLE `peserta_beasiswa`
  MODIFY `id_peserta_beasiswa` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  MODIFY `id_pilihan_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pilih_biaya`
--
ALTER TABLE `pilih_biaya`
  MODIFY `id_pilih_biaya` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plot_ajar`
--
ALTER TABLE `plot_ajar`
  MODIFY `id_plot_ajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `profile_gukar`
--
ALTER TABLE `profile_gukar`
  MODIFY `id_profile_gukar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `profile_siswa`
--
ALTER TABLE `profile_siswa`
  MODIFY `id_profile_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `quis`
--
ALTER TABLE `quis`
  MODIFY `id_quis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rombel`
--
ALTER TABLE `rombel`
  MODIFY `id_rombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sanksi`
--
ALTER TABLE `sanksi`
  MODIFY `id_sanksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slot_jam`
--
ALTER TABLE `slot_jam`
  MODIFY `id_slot_jam` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `soal_uas`
--
ALTER TABLE `soal_uas`
  MODIFY `id_soal_uas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `status_siswa`
--
ALTER TABLE `status_siswa`
  MODIFY `id_status_siswa` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `surat_bk`
--
ALTER TABLE `surat_bk`
  MODIFY `id_surat_bk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  MODIFY `id_surat_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  MODIFY `id_surat_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id_tagihan` tinyint(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id_tahun_ajaran` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_nilai_kriteria`
--
ALTER TABLE `tbl_nilai_kriteria`
  MODIFY `id_nilai_kriteria` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `template_jenis_surat`
--
ALTER TABLE `template_jenis_surat`
  MODIFY `id_template_jenis_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `type_user`
--
ALTER TABLE `type_user`
  MODIFY `id_type` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `upload_file`
--
ALTER TABLE `upload_file`
  MODIFY `id_upload_file_berkas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `upload_materi`
--
ALTER TABLE `upload_materi`
  MODIFY `id_upload_materi` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=375;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `arsip_surat_detail`
--
ALTER TABLE `arsip_surat_detail`
  ADD CONSTRAINT `arsipsuratdetail_arsipsurat` FOREIGN KEY (`id_arsip_surat`) REFERENCES `arsip_surat` (`id_arsip_surat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `arsipsuratdetail_surakkeluar` FOREIGN KEY (`id_surat_keluar`) REFERENCES `surat_keluar` (`id_surat_keluar`) ON UPDATE CASCADE;

--
-- Constraints for table `disposisi`
--
ALTER TABLE `disposisi`
  ADD CONSTRAINT `adisposisi_disposisi` FOREIGN KEY (`id_adisposisi`) REFERENCES `adisposisi` (`id_adisposisi`) ON UPDATE CASCADE,
  ADD CONSTRAINT `suratmasuk_disposisi` FOREIGN KEY (`id_surat_masuk`) REFERENCES `surat_masuk` (`id_surat_masuk`) ON UPDATE CASCADE;

--
-- Constraints for table `hak_akses_group`
--
ALTER TABLE `hak_akses_group`
  ADD CONSTRAINT `hakakses_hakaksesgroup` FOREIGN KEY (`id_hak_akses`) REFERENCES `hak_akses` (`id_hak_akses`) ON UPDATE CASCADE;

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `plot_ajar_jadwal` FOREIGN KEY (`id_plot_ajar`) REFERENCES `plot_ajar` (`id_plot_ajar`) ON UPDATE CASCADE,
  ADD CONSTRAINT `slot_jam_jadwal` FOREIGN KEY (`id_slot_jam`) REFERENCES `slot_jam` (`id_slot_jam`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tahun_ajaran_jadwal` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_alarm`
--
ALTER TABLE `jadwal_alarm`
  ADD CONSTRAINT `fileaudio_jadwalalarm` FOREIGN KEY (`id_file_audio`) REFERENCES `file_audio` (`id_file_audio`) ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_susulan_uas`
--
ALTER TABLE `jadwal_susulan_uas`
  ADD CONSTRAINT `jadwaluas_jadwalsusulanuas` FOREIGN KEY (`id_jadwal_uas`) REFERENCES `jadwal_uas` (`id_jadwal_uas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_jadwalsusulanuas` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_uas`
--
ALTER TABLE `jadwal_uas`
  ADD CONSTRAINT `mapel_jadwaluas` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `paket_jadwaluas` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`) ON UPDATE CASCADE,
  ADD CONSTRAINT `rombel_jadwaluas` FOREIGN KEY (`id_rombel`) REFERENCES `rombel` (`id_rombel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tahunajaran_jadwaluas` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_jadwaluas` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `jawaban_siswa`
--
ALTER TABLE `jawaban_siswa`
  ADD CONSTRAINT `pilihanjawaban_jawabansiswa` FOREIGN KEY (`id_pilihan_jawaban`) REFERENCES `pilihan_jawaban` (`id_pilihan_jawaban`) ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_jawabansiswa` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id_soal`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_jawabansiswa` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `jawaban_uas`
--
ALTER TABLE `jawaban_uas`
  ADD CONSTRAINT `soal_jawabanuas` FOREIGN KEY (`id_soal_uas`) REFERENCES `soal_uas` (`id_soal_uas`) ON UPDATE CASCADE;

--
-- Constraints for table `jawaban_uas_siswa`
--
ALTER TABLE `jawaban_uas_siswa`
  ADD CONSTRAINT `jadwaluas_jawabanuassiswa` FOREIGN KEY (`id_jadwal_uas`) REFERENCES `jadwal_uas` (`id_jadwal_uas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jawabanuas_jawabanuassiswa` FOREIGN KEY (`id_jawaban_uas`) REFERENCES `jawaban_uas` (`id_jawaban_uas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_jawabanuassiswa` FOREIGN KEY (`id_soal_uas`) REFERENCES `soal_uas` (`id_soal_uas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_jawabanuassiswa` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `kandidat`
--
ALTER TABLE `kandidat`
  ADD CONSTRAINT `user_kandidat` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `kategori_beasiswa`
--
ALTER TABLE `kategori_beasiswa`
  ADD CONSTRAINT `tahun_ajaran_kategori_beasiswa` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE;

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `mapel_komentar` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_komentar` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `kriteria_beasiswa`
--
ALTER TABLE `kriteria_beasiswa`
  ADD CONSTRAINT `kategori_beasiswa_kriteria_beasiswa` FOREIGN KEY (`id_kategori_beasiswa`) REFERENCES `kategori_beasiswa` (`id_kategori_beasiswa`) ON UPDATE CASCADE;

--
-- Constraints for table `kunci_jawaban`
--
ALTER TABLE `kunci_jawaban`
  ADD CONSTRAINT `pilihanjawaban_kuncijawaban` FOREIGN KEY (`id_pilihan_jawaban`) REFERENCES `pilihan_jawaban` (`id_pilihan_jawaban`) ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_kuncijawaban` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id_soal`) ON UPDATE CASCADE;

--
-- Constraints for table `level_user`
--
ALTER TABLE `level_user`
  ADD CONSTRAINT `type_user_leveluser` FOREIGN KEY (`id_type`) REFERENCES `type_user` (`id_type`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_level_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `log_rombel`
--
ALTER TABLE `log_rombel`
  ADD CONSTRAINT `tahun_ajaran_log_rombel` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_log_rombel` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `mapel`
--
ALTER TABLE `mapel`
  ADD CONSTRAINT `jurusan_mapel` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `level_kelas_mapel` FOREIGN KEY (`id_level_kelas`) REFERENCES `level_kelas` (`id_level_kelas`) ON UPDATE CASCADE;

--
-- Constraints for table `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `mapel_materi` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `rombel_materi` FOREIGN KEY (`id_rombel`) REFERENCES `rombel` (`id_rombel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_materi` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `nilai_quis`
--
ALTER TABLE `nilai_quis`
  ADD CONSTRAINT `quis_nilaiquis` FOREIGN KEY (`id_quis`) REFERENCES `quis` (`id_quis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_nilaiquis` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `nilai_uas`
--
ALTER TABLE `nilai_uas`
  ADD CONSTRAINT `jadwaluas_nilaiuas` FOREIGN KEY (`id_jadwal_uas`) REFERENCES `jadwal_uas` (`id_jadwal_uas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_nilaiuas` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `pelanggaran_siswa`
--
ALTER TABLE `pelanggaran_siswa`
  ADD CONSTRAINT `user_pelanggaransiswa` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `tagihan_pembayaran` FOREIGN KEY (`id_tagihan`) REFERENCES `tagihan` (`id_tagihan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_pembayaran` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `pemilihan`
--
ALTER TABLE `pemilihan`
  ADD CONSTRAINT `kandidat_pemilihan` FOREIGN KEY (`id_kandidat`) REFERENCES `kandidat` (`id_kandidat`) ON UPDATE CASCADE,
  ADD CONSTRAINT `masapemilu_pemilihan` FOREIGN KEY (`id_masa_pemilu`) REFERENCES `masa_pemilu` (`id_masa_pemilu`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_pemilihan` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `perizinan_siswa`
--
ALTER TABLE `perizinan_siswa`
  ADD CONSTRAINT `user_perizinansiswa` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `peserta_beasiswa`
--
ALTER TABLE `peserta_beasiswa`
  ADD CONSTRAINT `pesertabeasiswaiduser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `pilihan_jawaban`
--
ALTER TABLE `pilihan_jawaban`
  ADD CONSTRAINT `soal_pilihanjawaban` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id_soal`) ON UPDATE CASCADE;

--
-- Constraints for table `pilih_biaya`
--
ALTER TABLE `pilih_biaya`
  ADD CONSTRAINT `biaya_pilih` FOREIGN KEY (`id_biaya`) REFERENCES `biaya` (`id_biaya`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tagihan_pilih` FOREIGN KEY (`id_tagihan`) REFERENCES `tagihan` (`id_tagihan`) ON UPDATE CASCADE;

--
-- Constraints for table `plot_ajar`
--
ALTER TABLE `plot_ajar`
  ADD CONSTRAINT `mapel_plot_ajar` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `rombel_plot_ajar` FOREIGN KEY (`id_rombel`) REFERENCES `rombel` (`id_rombel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tahun_ajaran_plot_ajar` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_plot_ajar` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `profile_gukar`
--
ALTER TABLE `profile_gukar`
  ADD CONSTRAINT `user_profile_gukar` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `profile_siswa`
--
ALTER TABLE `profile_siswa`
  ADD CONSTRAINT `jurusan_profile_siswa` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `rombel_profile_siswa` FOREIGN KEY (`id_rombel`) REFERENCES `rombel` (`id_rombel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `status_siswa_profile_siswa` FOREIGN KEY (`id_status_siswa`) REFERENCES `status_siswa` (`id_status_siswa`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_profile_siswa` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `quis`
--
ALTER TABLE `quis`
  ADD CONSTRAINT `mapel_quis` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE;

--
-- Constraints for table `rombel`
--
ALTER TABLE `rombel`
  ADD CONSTRAINT `jurusan_rombel` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `level_kelas_rombel` FOREIGN KEY (`id_level_kelas`) REFERENCES `level_kelas` (`id_level_kelas`) ON UPDATE CASCADE;

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `quis_soal` FOREIGN KEY (`id_quis`) REFERENCES `quis` (`id_quis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_soal` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `soal_uas`
--
ALTER TABLE `soal_uas`
  ADD CONSTRAINT `paket_soaluas` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`) ON UPDATE CASCADE;

--
-- Constraints for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD CONSTRAINT `jenissurat_suratkeluar` FOREIGN KEY (`id_jenis_surat`) REFERENCES `jenis_surat` (`id_jenis_surat`) ON UPDATE CASCADE;

--
-- Constraints for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD CONSTRAINT `tahun_ajaran_tagihan` FOREIGN KEY (`id_tahun_ajaran`) REFERENCES `tahun_ajaran` (`id_tahun_ajaran`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_tagihan` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_nilai_kriteria`
--
ALTER TABLE `tbl_nilai_kriteria`
  ADD CONSTRAINT `kriteria_beasiswa_nilai_kriteria` FOREIGN KEY (`id_kriteria_beasiswa`) REFERENCES `kriteria_beasiswa` (`id_kriteria_beasiswa`) ON UPDATE CASCADE,
  ADD CONSTRAINT `nilaibeasiswaiduser` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `template_jenis_surat`
--
ALTER TABLE `template_jenis_surat`
  ADD CONSTRAINT `templatesurat_jenissurat` FOREIGN KEY (`id_jenis_surat`) REFERENCES `jenis_surat` (`id_jenis_surat`) ON UPDATE CASCADE;

--
-- Constraints for table `upload_file`
--
ALTER TABLE `upload_file`
  ADD CONSTRAINT `hakaksesgroup_uploadfileberkas` FOREIGN KEY (`id_hak_akses_group`) REFERENCES `hak_akses_group` (`id_hak_akses_group`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_uploadfileberkas` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Constraints for table `upload_materi`
--
ALTER TABLE `upload_materi`
  ADD CONSTRAINT `mapel_uploadmateri` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON UPDATE CASCADE,
  ADD CONSTRAINT `user_uploadmateri` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
